METU EE493 Capstone Project - Robothings
==

This is our last year project for the Electrical Engineering BSc degree. 

Our symbolic company was named **Robothings Electronic Industries Inc.**

Group Members:
- Fatih YAZICI
- Mete Can KAYA
- Daniyal Ahmed BASHARAT
- Ahmet Barış GÖK
- Batuhan KALTALIOĞLU

Final project video: [https://youtu.be/6OKeh66o5t8](https://youtu.be/6OKeh66o5t8)
