#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 21:11:12 2019

@author: fatih
"""

socketio.emit("update-rough", [{"xs": [1,2,3], "ys": [4,5,6]}, {"xs": [2,3,4], "ys": [6,7,8]}])
    socketio.emit("update-laser", {"message": "circle", "dataGroups": [{"x": [1,2,3], "y": [4,5,6]}, {"x": [2,3,4], "y": [5,6,7]}]})
    socketio.emit("update-map", [[0, 0]])
    socketio.emit("update-objects", [
            {"type": "cylinder", "position": [600, 600], "radius": 25},
            {"type": "cylinder", "position": [-600, -600], "radius": 50},
            {"type": "triangle", "position": [-600, 600], "angle": 0},
            {"type": "square", "position": [600, -600], "angle": 0}
            ])
    socketio.emit("update-robot", {
            "location": [300, 500],
            "angle": 30,
            })