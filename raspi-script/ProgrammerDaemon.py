#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 18:09:05 2019

@author: fatih
"""

import pyinotify
import requests
import subprocess
import argparse
import logging

DEVICE_ENUMERATOR_URL = "http://localhost:8001"

watch_dir = "/home/pi/hex/"

watch_list = [
        "stepper_control.ino.standard.hex",
        "encoder_feedback_distance_measurement.ino.mega.hex"
        ]

watch_devs = {
        "stepper_control.ino.standard.hex": "uno",
        "encoder_feedback_distance_measurement.ino.mega.hex": "mega"
        }

watch_cmds = {
        "stepper_control.ino.standard.hex": "-Cavrdude.conf -v -V -patmega328p -carduino -P{} -b115200 -D -Uflash:w:stepper_control.ino.standard.hex:i",
        "encoder_feedback_distance_measurement.ino.mega.hex": "-C avrdude.conf -v -V -patmega2560 -cwiring -P{} /dev/ttyUSB0 -b 115200 -D -Uflash:w:encoder_feedback_distance_measurement.ino.mega.hex:i"
        }

class MyEventHandler(pyinotify.ProcessEvent):
#    def process_IN_ACCESS(self, event):
#        print("ACCESS event:", event.pathname)
#
#    def process_IN_ATTRIB(self, event):
#        print("ATTRIB event:", event.pathname)
#
#    def process_IN_CLOSE_NOWRITE(self, event):
#        print("CLOSE_NOWRITE event:", event.pathname)
#
#    def process_IN_CREATE(self, event):
#        print("CREATE event:", event.pathname)
#
#    def process_IN_DELETE(self, event):
#        print("DELETE event:", event.pathname)
#
#    def process_IN_MODIFY(self, event):
#        print("MODIFY event:", event.pathname)
#        
#    def process_IN_OPEN(self, event):
#        print("OPEN event:", event.pathname)
    
    def process_IN_CREATE(self, event):
        logging.debug("CREATE event: %s", event.name)
        if event.name in watch_list:
            watch_dev = watch_devs[event.name]
            watch_cmd = watch_cmds[event.name]
            
            # perform fresh scan
            # requests.get(DEVICE_ENUMERATOR_URL + "/scan")
            resp = requests.post(DEVICE_ENUMERATOR_URL + "/dev", data={"type": watch_dev.upper()}, timeout=1)
            device_path = resp.text
            logging.debug("device path is: %s", device_path)
            
            subprocess.call(["avrdude"] + watch_cmd.format(device_path).split(' '), cwd=watch_dir)


if __name__ == '__main__':
    # config
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    # watch manager
    wm = pyinotify.WatchManager()
    wm.add_watch(watch_dir, pyinotify.ALL_EVENTS)

    # event handler
    eh = MyEventHandler()

    # notifier
    notifier = pyinotify.Notifier(wm, eh)
    notifier.loop()
