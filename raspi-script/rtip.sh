#!/bin/sh

PASSCODE="XuABn3nm4fTJGxUY"
ADDR="https://user.ceng.metu.edu.tr/~e2167583/rtip.php"

RESPONSE=$(curl -s -X POST --data "id=$PASSCODE" "$ADDR")

if [ "$RESPONSE" = "OK" ]
then
  echo "RT-IP updated the client IP successfully."
else
  echo "RT-IP update failed. Server response: $RESPONSE"
fi

