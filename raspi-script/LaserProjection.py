#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 05:28:55 2019

@author: fatih
"""

import numpy as np
from sklearn import cluster, linear_model
import logging
from collections import namedtuple

CAM_FOCAL_LENGTH = 1280          # px (was 2571)
LASER_AXIS_POS = [0, 85, 160]   # in mm
LASER_PLANE_TILT = 90 - 34       # in deg
CAM_OFFSET_LOC = 40              # mm

def _run(args):
    if args.display:
        from mpl_toolkits import mplot3d
        import matplotlib.pyplot as plt
        
    pts = np.load(args.datapath)["pts"]

    if pts.shape[0] < 1:
        return None

    pts[:, 0] -= 760/2
    pts[:, 1] -= 520/2
    
    qs = np.hstack((pts, -CAM_FOCAL_LENGTH * np.ones((pts.shape[0], 1))))
    qs = np.dot(qs, np.diag([1, 1, 1]))
    
    #print(qs)
    
    pl = np.array(LASER_AXIS_POS)
    nl = np.array([0, np.tan(np.deg2rad(LASER_PLANE_TILT)), 1])
    
    plnl = np.dot(pl, nl)
    qsnl = np.dot(qs, nl)
    
    #print(plnl)
    
    ts = plnl / qsnl
    tts = np.vstack((ts, ts, ts)).T
    
    #print(tts)
    
    p3d = np.multiply(qs, tts)
    
    #print(p3d)
    
    xs, ys, zs = p3d.T
    
    if args.display:
        figure = plt.figure()
        ax3 = plt.axes(projection='3d')
        ax3.scatter(xs, ys, zs)
        ax3.set_title("3D Raw Point Cloud")
        ax3.set_xlabel("x (mm)")
        ax3.set_ylabel("y (mm)")
        ax3.set_zlabel("z (mm)")
        ax3.axis('equal')
    
    #plt.show()
    
    ############
    
    
    def reduce_cluster(p3d, eps):
        db = cluster.DBSCAN(eps=eps, min_samples=10).fit(p3d)
        labels = db.labels_
        core_samples_mask = np.zeros_like(labels, dtype=bool)
        core_samples_mask[db.core_sample_indices_] = True
        
        unique_labels = sorted(list(set(labels)))
        sizes = list(map(lambda x: (labels == x).sum(), unique_labels))
        max_index = np.argmax(sizes)
        max_label = unique_labels[max_index]
        max_points = p3d[core_samples_mask & (labels == max_label)]
        
        return max_points.T
    
    #figure = plt.figure()
    #ax3 = plt.axes(projection='3d')
    #ax3.scatter(xs, ys, zs)
    #ax3.set_title("3D Point Cloud (after outlier removal)")
    #ax3.set_xlabel("x (mm)")
    #ax3.set_ylabel("y (mm)")
    #ax3.set_zlabel("z (mm)")
    #ax3.axis('equal')
    
    #plt.show()
    
    ############
    
    def circle_fit(p3d):
        # circle fit is more restrictive, points should be densely packed together
        xs, ys, zs = reduce_cluster(p3d, 5)  
        
        if args.display:
            figure = plt.figure()
            ax3 = plt.axes(projection='3d')
            ax3.scatter(xs, ys, zs)
            ax3.set_title("3D Point Cloud CIRCLE (after outlier removal)")
            ax3.set_xlabel("x (mm)")
            ax3.set_ylabel("y (mm)")
            ax3.set_zlabel("z (mm)")
            ax3.axis('equal')
        
        x = np.vstack((xs, zs, np.ones_like(zs))).T
        y = xs * xs + zs * zs
        
        a, res, rk, s = np.linalg.lstsq(x, y, rcond=None)
        
        txc, tzc = a[0]/2, a[1]/2
        trc = np.sqrt(a[2] + txc*txc + tzc*tzc)
        logging.debug("Presumed radius: {}".format(trc))

        score = 1000000 / res    
        logging.debug("Circle matching score: {}".format(score))
        
        if trc < 70 and trc > 17 and score > 1:
            logging.debug("Seems like a circle!")
        else:
            return None
        
        xs, ys, zs = reduce_cluster(p3d, 20)
        
        x = np.vstack((xs, zs)).T
        y = xs * xs + zs * zs
        
        print(x.shape)
        ransac = linear_model.RANSACRegressor(loss='squared_loss', 
                                              residual_threshold=1000, min_samples=int(x.shape[0]*0.9), max_trials=1000)
        ransac.fit(x, y)
    #    print(ransac.__dict__)
        logging.debug(ransac.estimator_.__dict__)
        est = ransac.estimator_
        xc, zc = est.coef_[0]/2, est.coef_[1]/2
        rc = np.sqrt(est.intercept_ + xc*xc + zc*zc)
        inliers = ransac.inlier_mask_
        
        logging.debug("center x: {} \ncenter z: {} \nradius: {}".format(xc, zc, rc))
        
        if args.display:
            figure = plt.figure()
            ax = plt.axes()
            
            c1 = plt.Circle((xc, zc), radius=rc, color='g', fill=False, linestyle='--', linewidth=2)
            
            ax.scatter(xs[~inliers], zs[~inliers], marker="x", label="Outliers")
            ax.scatter(xs[inliers], zs[inliers], marker="o", label="Inliers")
            ax.add_artist(c1)
            ax.set_title("Circular Cross Section Matching")
            ax.set_xlabel("x (mm)")
            ax.set_ylabel("z (mm)")
            ax.legend()
            ax.axis('equal')
        
        return (xc, zc, rc, (xs[inliers].tolist(), zs[inliers].tolist(), xs[~inliers].tolist(), zs[~inliers].tolist()))
    
    def line_fit(p3d):
        # line fit is relaxed, points could be more sparsely distributed
        xs, ys, zs = reduce_cluster(p3d, 20)  

        if len(xs) < 10:
            return None

        if args.display:
            figure = plt.figure()
            ax3 = plt.axes(projection='3d')
            ax3.scatter(xs, ys, zs)
            ax3.set_title("3D Point Cloud LINE (after outlier removal)")
            ax3.set_xlabel("x (mm)")
            ax3.set_ylabel("y (mm)")
            ax3.set_zlabel("z (mm)")
            ax3.axis('equal')
        
        ransac_1 = linear_model.RANSACRegressor(loss='squared_loss', residual_threshold=0.5)
        ransac_1.fit(xs.reshape(-1, 1), zs)
        m_1, n_1 = ransac_1.estimator_.coef_, ransac_1.estimator_.intercept_
        line_1 = ransac_1.inlier_mask_
        xs_1, zs_1 = xs[line_1], zs[line_1]
        
        if sum(line_1) / len(line_1) < 0.9:
            ransac_2 = linear_model.RANSACRegressor(loss='squared_loss', residual_threshold=0.5)
            ransac_2.fit(xs.reshape(-1, 1)[~line_1], zs[~line_1])
            m_2, n_2 = ransac_2.estimator_.coef_, ransac_2.estimator_.intercept_
            line_2 = ransac_2.inlier_mask_
            
            xs_2, zs_2 = xs[~line_1][line_2], zs[~line_1][line_2]
            
            logging.debug("Line fit params: {}, {}, {}, {}".format(m_1, n_1, m_2, n_2))
            
            angle = 180 - np.rad2deg(np.arctan2(m_2-m_1, 1+m_2*m_1))
            if angle > 180:
                angle = 360 - angle
            
            logging.debug("Angle between edges: {} degrees".format(angle))
        else:
            xs_2 = None
            zs_2 = None
        
        if xs_2 is None or angle > 140:
            m = m_1
                
            edge_vec = np.float32([np.sign(m), np.abs(m)]) / np.hypot(1, m)
            cent_vec = np.float32([-m, 1]).reshape(2, 1) / np.hypot(1, m)
            
            #closest_idx = np.argmin(zs)
            #closest_pt = np.vstack((xs[closest_idx], zs[closest_idx]))
            
            logging.debug("Linear object breakpoint not visible, estimating from side length")
            n = xs.shape[0]
            max_dist = 0
            min_dist_idx = 0
            for i in range(n):
                for j in range(i + 1, n):
                    dist = np.hypot(xs[i] - xs[j], zs[i] - zs[j])
                    if dist > max_dist:
                        max_dist = dist
                        if zs[i] < zs[j]:
                            min_dist_idx = i
                        else:
                            min_dist_idx = j
            
            closest_pt = np.vstack((xs[min_dist_idx], zs[min_dist_idx]))
                
            logging.debug("Distance betweeen farthest points is {}".format(max_dist))
            logging.debug("Closest point: {}".format(closest_pt))
            logging.debug("Edge vector: {}".format(edge_vec))
            logging.debug("Center vector: {}".format(cent_vec))
            
            if max_dist > 70:
                # most likely triangle base
                mid_pt = closest_pt + edge_vec * 40
                obj_center = mid_pt + cent_vec * 23.09  # 40*sqrt(3)/3
                angle = 60
            else:
                # most likely square base
                mid_pt = closest_pt + edge_vec * 35
                obj_center = mid_pt + cent_vec * 35
                angle = 90
            
            logging.debug("mid point: {}".format(mid_pt))
            logging.debug("center pt: {}".format(obj_center))
            pt_vec = closest_pt - obj_center
            obj_orientation = np.rad2deg(np.arctan2(pt_vec[1], pt_vec[0]))
                    
        else:    
            # calculate intersection point
            x = (n_2 - n_1) / (m_1 - m_2)
            y = m_1 * x + n_1
            pta = np.float32([x, y])
            
            logging.debug("Intersect pt: {}".format(pta))
            
            v1 = np.float32([np.sign(m_1), np.abs(m_1)])
            v1 = v1 / np.hypot(1, m_1)
            
            v2 = np.float32([np.sign(m_2), np.abs(m_2)])
            v2 = v2 / np.hypot(1, m_2)
            
            edge_length = 70
            if angle > 75:
                edge_length = 80
                
            ptb = pta + v1 * edge_length
            ptc = pta + v2 * edge_length
            
            logging.debug("Edge pts: {} {}".format(ptb, ptc))
            
            if angle > 75:
                # square center
                obj_center = (ptb + ptc) / 2
            else:
                # triangle center
                obj_center = (pta + ptb + ptc) / 3
            
            pt_vec = pta - obj_center
            obj_orientation = np.rad2deg(np.arctan2(pt_vec[1], pt_vec[0]))
        
        if args.display:
            figure = plt.figure()
            #plt.scatter(xs, zs)
            plt.scatter(xs_1, zs_1, marker='x', label="Projected Pts 1")
            if xs_2 is not None:
                plt.scatter(xs_2, zs_2, marker='x', label="Projected Pts 2")
            
            plt.plot(xs_1, m_1*xs_1+n_1, label="Line Fit 1")
            if xs_2 is not None:
                plt.plot(xs_2, m_2*xs_2+n_2, label="Line Fit 2")
            plt.legend()
            plt.title("Linear Cross Section Matching")
            plt.xlabel("x (mm)")
            plt.ylabel("z (mm)")
            plt.axis('equal')
            plt.show()

        if xs_2 is None:
            xs_2 = np.array([])

        if zs_2 is None:
            zs_2 = np.array([])

        return (angle, obj_center, obj_orientation, (xs_1.tolist(), zs_1.tolist(), xs_2.tolist(), zs_2.tolist()))
    
    #############
    
    #figure = plt.figure()
    #plt.scatter(xs, zs)
    #plt.show()
    
    obj_type = "cylinder"
    obj_fit = circle_fit(p3d)
    
    if obj_fit is None:
        logging.debug("Not a circle. Lets check square or triangle base prism!")
        obj_type = "prism"
        obj_fit = line_fit(p3d)
        if obj_fit is None:
            logging.debug("Could not detect object type!")
            return None
    
    if args.display:
        plt.show()
    
    if obj_type == "cylinder":
        x, y, r, pts = obj_fit
        if r > 35:
            r = 50
        else:
            r = 25
        y -= CAM_OFFSET_LOC
        return { "type": "cylinder", "xoffset": x, "yoffset": y, "radius": r, "points": pts }
    else:
        angle, (x, y), theta, pts = obj_fit
        y -= CAM_OFFSET_LOC
        if angle > 75:
            return { "type": "square", "xoffset": x, "yoffset": y, "theta": float(theta), "points": pts }
        else:
            return { "type": "triangle", "xoffset": x, "yoffset": y, "theta": float(theta), "points": pts }

def run(datapath, display=False):
    Args_ = namedtuple("Args_", ["datapath", "display"])
    args = Args_(datapath, display)
    return _run(args)
    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--datapath", required=True)
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-d", "--display", action="store_true")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    print(_run(args))
    