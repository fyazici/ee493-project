#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 13:22:21 2019

@author: fatih
"""

from flask import Flask, abort, jsonify, json, request
import serial
import logging
from time import sleep
import glob
import argparse

DEVICE_ENUMERATOR_URL = "http://localhost:8001"

app = Flask(__name__)

class DeviceEnumeratorController:
    def __init__(self):
        logging.debug("Controller.initialize issued")
        self.devices = {}
        logging.debug("Controller initialized")
    
    def get_device(self, dev_type):
        if dev_type is None:
            return self.devices
        if dev_type in self.devices:
            return self.devices[dev_type]
        else:
            return None
    
    def enumerate_devices(self, device_masks=["/dev/ttyACM*", "/dev/ttyUSB*"]):
        for mask in device_masks:
            for file in glob.glob(mask):
                self.connect(file)
    
    def readdev(self):
        while True:
            resp_b = self.device.readline()
            logging.debug("Device responded: %s", resp_b)
            resp = resp_b.decode().strip()
            if resp[0] != "#":
                # not debug output
                return resp
        
    
    def connect(self, dev="/dev/ttyACM0", baudrate=1000000):
        logging.debug("Controller.connect issued")
        
        self.scanning = False
        self.device_path = dev
        try:
            self.device = serial.Serial(self.device_path, baudrate)
        except:
            self.connected = False
            return False
        
        self.device.setDTR(False)
        sleep(3)
        self.device.flushInput()
        #self.device.setDTR(True)
        
        
        logging.debug("Device opened")
        
        self.device.write("I\n".encode())
        logging.debug("Wait for device identity response")
        
        device_id = self.readdev()
        ack = self.readdev()
        if ack == "ACK":
            logging.debug("Device id: %s", device_id)
            self.devices[device_id] = dev
            self.connected = True
            return True
        
        self.connected = False
        return False
    
    def disconnect(self):
        if self.connected:
            logging.debug("Controller.disconnect issued")
            self.device.close()
        self.scanning = False
        self.connected = False
        return True

@app.route("/scan")
def enumerate_devices():
    ctl.enumerate_devices()
    return "OK"

@app.route("/dev", methods=["GET", "POST"])
def get_device_by_type(**kwargs):
    if request.method == "POST":
        name = request.values.get("type")
        dev = ctl.get_device(name)
        if not dev:
            abort(400)
        return dev
    else:
        return jsonify(ctl.get_device(None))

@app.route('/shutdown')
def shutdown():
    shutdown_server()
    return 'OK'
    
def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()
    
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbose", action="store_true")
    parser.add_argument("-n", "--no-scan", action="store_true")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    ctl = DeviceEnumeratorController()
    
    if not args.no_scan:
        ctl.enumerate_devices()
    
    app.run(host="0.0.0.0", port=8001)


    
