#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 18:26:46 2019

@author: fatih
"""

import subprocess
import time
import logging
from collections import namedtuple

def _run(args, lc):
    CAPTURE_COMMAND = "raspivid -v -n -w 1280 -h 720 -rot 180 -t {} -o {}".format(args.timeout, args.outfile).split()

    logging.debug("initializing laser controller connection")
    
    logging.debug("connected to laser controller")
    
    lc.laser_power(0)
    lc.laser_position(0)
    lc.laser_stop()
    
    logging.debug("starting raspivid capture")
    
    p = subprocess.Popen(CAPTURE_COMMAND)
    
    time.sleep(2)
    
    logging.debug("enabling laser")
    lc.laser_power(255)
    lc.laser_start()
    
    logging.debug("waiting for recording to finish")
    p.wait()
    
    logging.debug("stopping laser")
    lc.laser_power(0)
    lc.laser_stop()

def run(outfile, lc, timeout=10000):
    """
    outfile, timeout
    """
    Args_ = namedtuple("Args_", ["outfile", "timeout"])
    args = Args_(outfile, timeout)
    _run(args, lc)

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--outfile")
    parser.add_argument("-t", "--timeout", action="store", default=10000)
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    _run(args)
