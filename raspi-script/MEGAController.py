#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 23:34:43 2019

@author: metecan
"""

import requests
import serial
import time

DEVICE_ENUMERATOR_URL = "http://localhost:8001"

class MEGAController:
    def wait_ack(self):
        while True:
            r = self.device.readline()
            if r.startswith(b"#"):
                continue
            if r == b"ACK\r\n":
                break
    
    def send_wait(self, cmd):
        self.device.write(cmd)
        self.wait_ack()
    
    def connect(self):
        #requests.get(DEVICE_ENUMERATOR_URL + "/scan")
        devname = requests.post(DEVICE_ENUMERATOR_URL + "/dev", data={"type": "MEGA"})
        self.device = serial.Serial(devname.text, baudrate=1000000)
        time.sleep(3)
        #self.device.flush
        self.send_wait(b"S\n")
        time.sleep(1)
        self.send_wait(b"I\n")
        return True
    
    def disconnect(self):
        self.device.close()
    
    def movement(self, d):
        self.send_wait("M {}\n".format(d).encode())
        while True:
            r = self.device.readline()
            if r.startswith(b"#"):
                continue
            if r == b"MOVE_END\r\n":
                break
        return True
        
    def  rotation(self, r):
        self.send_wait("R {}\n".format(r).encode())
        while True:
            r = self.device.readline()
            if r.startswith(b"#"):
                continue
            if r == b"MOVE_END\r\n":
                break
        return True
    
    def odometry(self):
        self.send_wait(b"D\n")
        pos = None
        while True:
            r = self.device.readline()
            if r.startswith(b"#"):
                continue
            if r == b"END\r\n":
                break
            else:
                pos = tuple(map(float, r.decode("ASCII").split(",")))
        return pos
