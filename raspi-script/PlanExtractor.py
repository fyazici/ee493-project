#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 23:44:28 2019

@author: fatih
"""

from UNOController import UNOController
from MEGAController import MEGAController

import LidarProcess
import RoughmapCluster

import LaserCapture
import LaserGeometry
import LaserProjection

import logging
from datetime import datetime
import numpy as np
import cv2
import time
from scipy.spatial import ConvexHull

OBJECT_NEAR_THRESH = 600    # mm
OBJECT_INSPECT_OFFSET = 120 + 120   # mm
OBJECT_FAR_REACH_OFFSET = 300   # mm
ROBOT_WIDTH = 250   # mm
OBJECT_COLLISION_RADIUS = 150   # mm
OBJECT_TRAVERSE_DISTANCE = 300  # mm
SENSOR_VALID_DIST = 1000    # mm
TOF_PLATFORM_RETURN_TIME = 25   # secs
OBJECT_MATCH_DISTANCE = 150  # mm
SEEK_PATH_RANDOMIZED = False


def _mm():
    return datetime.now().strftime("%m-%d-%H-%M-%S")


def _m(fname, tag):
    filename, extension = fname.split(".")
    return filename + "(" + tag + ")." + extension


class Util:
    @staticmethod
    def rotation_matrix(theta):
        t = np.deg2rad(theta)
        c, s = np.cos(t), np.sin(t)
        return np.array(((c, -s), (s, c)))

    @staticmethod
    def is_point_in_circle(pt_x, pt_y, circle_x, circle_y, circle_r):
        return np.hypot(pt_x - circle_x, pt_y - circle_y) <= circle_r

    @staticmethod
    def is_point_in_rectangle(x, y, left, top, right, bottom):
        return (x >= left) and (x <= right) and (y <= top) and (y >= bottom)

    @staticmethod
    def merge_clusters(clusters):
        merged = []
        for cluster in clusters:
            for pt in cluster:
                merged.append([float(pt[0]), float(pt[1])])
        return merged

    @staticmethod
    def merge_object_clusters(clusters, ignored=()):
        merged = []
        for (i, cluster) in enumerate(clusters):
            if i not in ignored:
                for pt in cluster["points"]:
                    merged.append([float(pt[0]), float(pt[1])])
        return merged

    @staticmethod
    def collision_free_angles_strict(robot_x, robot_y, robot_theta, clusters, resolution=5):
        robot_rect = (0, ROBOT_WIDTH/2 + 10, OBJECT_FAR_REACH_OFFSET+ROBOT_WIDTH/2+50, -ROBOT_WIDTH/2 - 10)
        merged_pts = Util.merge_clusters(clusters)
        base_pts = np.matmul(np.array(merged_pts) - np.array((robot_x, robot_y)), Util.rotation_matrix(-robot_theta))
        step_rot = Util.rotation_matrix(-resolution)

        free_angles = [True] * (360 // resolution)
        for i in range(0, 360 // resolution):
            for pt in base_pts:
                if Util.is_point_in_rectangle(pt[0], pt[1], *robot_rect):
                    free_angles[i] = False
            base_pts = np.matmul(base_pts, step_rot)
        return free_angles

    @staticmethod
    def collision_free_object_approach(robot_x, robot_y, robot_theta, target_x, target_y, clusters, target_idx, resolution=5):
        logging.debug("robot {} {} {}".format(robot_x, robot_y, robot_theta))
        logging.debug("target {} {}".format(target_x, target_y))
        logging.debug("target idx: {}".format(target_idx))
        logging.debug("clusters: {}".format(clusters))

        target_distance = np.hypot(target_x - robot_x, target_y - robot_y)
        target_angle = np.rad2deg(np.arctan2(target_y - robot_y, target_x - robot_x))
        robot_rect = (0, ROBOT_WIDTH / 2 + 10, target_distance + ROBOT_WIDTH / 2, -ROBOT_WIDTH / 2 - 10)
        merged_pts = Util.merge_object_clusters(clusters, ignored=(target_idx,))

        if len(merged_pts) == 0:
            return True

        base_pts = np.matmul(np.array(merged_pts) - np.array((robot_x, robot_y)),
                             Util.rotation_matrix(-robot_theta + target_angle))

        logging.debug("rect {}".format(robot_rect))
        logging.debug("basept {}".format(base_pts))

        is_reachable = True
        for pt in base_pts:
            if Util.is_point_in_rectangle(pt[0], pt[1], *robot_rect):
                is_reachable = False
                break
        return is_reachable

    @staticmethod
    def collision_free_angles_relaxed(robot_x, robot_y, robot_theta, clusters, resolution=5):
        robot_rect = (0, ROBOT_WIDTH/2 + 30, OBJECT_FAR_REACH_OFFSET+ROBOT_WIDTH/2+50, -ROBOT_WIDTH/2 - 30)
        merged_pts = Util.merge_clusters(clusters)
        base_pts = np.matmul(np.array(merged_pts) - np.array((robot_x, robot_y)), Util.rotation_matrix(-robot_theta))
        step_rot = Util.rotation_matrix(-resolution)

        free_angles = [OBJECT_TRAVERSE_DISTANCE] * (360 // resolution)
        for i in range(0, 360 // resolution):
            for pt in base_pts:
                if Util.is_point_in_rectangle(pt[0], pt[1], *robot_rect):
                    free_angles[i] = min(free_angles[i], pt[0] - ROBOT_WIDTH/2 - 50)
            base_pts = np.matmul(base_pts, step_rot)
        free_angles = [{"angle": i * resolution, "dist": free_angles[i]} for i in range(0, 360 // resolution)]
        return sorted(free_angles, key=lambda x: x["dist"], reverse=True)

    @staticmethod
    def is_object_cluster(cluster):
        if len(cluster) > 1:
            arc_length = cv2.arcLength(np.float32(cluster), False)
            if arc_length < 300:
                return True
        return False

    @staticmethod
    def filter_clusters_by_distance(rx, ry, obj_clusters):
        objects_near = []
        for cluster in obj_clusters:
            dist = []
            for xy in cluster:
                dist.append({"pt": xy, "dist": np.hypot(xy[0] - rx, xy[1] - ry)})
            dist = sorted(dist, key=lambda x: x["dist"])
            pts = [x["pt"] for x in dist]
            if dist[0]["dist"] < OBJECT_NEAR_THRESH:
                objects_near.append({"points": pts, "nearest_pt": pts[0]})
        return objects_near

    @staticmethod
    def classify_clusters(clusters):
        obj_clusters = []
        wall_clusters = []
        for cluster in clusters:
            if Util.is_object_cluster(cluster):
                obj_clusters.append(cluster)
            else:
                wall_clusters.append(cluster)
        return obj_clusters, wall_clusters

    @staticmethod
    def movable_directions(free_angles, resolution=5):
        slices = []
        slice_start = None
        if free_angles[0]:
            for i in range(0, len(free_angles)):
                if free_angles[i]:
                    free_angles[i] = False
                    free_angles.append(True)
                else:
                    break

        for i in range(0, len(free_angles)):
            if (slice_start is None) and free_angles[i]:
                slice_start = i
            if (slice_start is not None) and (not free_angles[i]):
                if (i - slice_start) > 2:
                    slices.append((slice_start * resolution, i * resolution))
                slice_start = None
        if slice_start is not None:
            slices.append((slice_start * resolution, len(free_angles) * resolution))
        slices = sorted(slices, reverse=True, key=lambda x: abs(x[0]-x[1]))
        return slices


class NavigationController:
    def __init__(self, pusher):
        self.megactl = MEGAController()
        self.megactl.connect()
        self.robot_x = 0
        self.robot_y = 0
        self.robot_theta = 0
        self.pusher = pusher

    def update_pos(self):
        self.robot_x, self.robot_y, self.robot_theta = self.megactl.odometry()
        self.pusher.push_robot_data([self.robot_x, self.robot_y], self.robot_theta)
        return self.robot_x, self.robot_y, self.robot_theta

    def goto_pos(self, tx, ty, offset=0, multistage=True):
        logging.debug("goto pos")
        logging.debug("Rx: {}, Ry: {}, Rt: {}".format(self.robot_x, self.robot_y, self.robot_theta))
        logging.debug("Tx: {}, Ty: {}".format(tx, ty))

        if multistage:
            # move stage 1
            self.update_pos()
            logging.debug("Rx: {}, Ry: {}, Rt: {}".format(self.robot_x, self.robot_y, self.robot_theta))
            vx = tx - self.robot_x
            vy = ty - self.robot_y
            vlen = np.hypot(vx, vy) - offset
            vtheta = np.rad2deg(np.arctan2(vy, vx)) - self.robot_theta
            if vtheta > 180:
                vtheta -= 360
            elif vtheta < -180:
                vtheta += 360
            logging.debug("Vlen: {}, Vtheta: {}".format(vlen, vtheta))
            self.megactl.rotation(vtheta)
            self.megactl.movement(vlen / 3)

            # move stage 3
            self.update_pos()
            logging.debug("Rx: {}, Ry: {}, Rt: {}".format(self.robot_x, self.robot_y, self.robot_theta))
            vx = tx - self.robot_x
            vy = ty - self.robot_y
            vlen = np.hypot(vx, vy) - offset
            vtheta = np.rad2deg(np.arctan2(vy, vx)) - self.robot_theta
            if vtheta > 180:
                vtheta -= 360
            elif vtheta < -180:
                vtheta += 360
            logging.debug("Vlen: {}, Vtheta: {}".format(vlen, vtheta))
            self.megactl.rotation(vtheta)
            self.megactl.movement(vlen / 2)

        # move stage 3
        self.update_pos()
        logging.debug("Rx: {}, Ry: {}, Rt: {}".format(self.robot_x, self.robot_y, self.robot_theta))
        vx = tx - self.robot_x
        vy = ty - self.robot_y
        vlen = np.hypot(vx, vy) - offset
        vtheta = np.rad2deg(np.arctan2(vy, vx)) - self.robot_theta
        if vtheta > 180:
            vtheta -= 360
        elif vtheta < -180:
            vtheta += 360
        self.megactl.rotation(vtheta)
        self.megactl.movement(vlen)
        logging.debug("Vlen: {}, Vtheta: {}".format(vlen, vtheta))

        # cut the noise
        self.megactl.movement(0)
        self.update_pos()
        logging.debug("Rx: {}, Ry: {}, Rt: {}".format(self.robot_x, self.robot_y, self.robot_theta))


class PlanExtractor:
    def __init__(self, pusher, display=False):
        self.display = display
        self.pusher = pusher
        self.all_clusters = []
        self.all_objects = []
        self.all_walls = []
        self.covered_circles = []
        self.convexhull = None

        self.pusher.push_task("Initialize Map Controller")
        self.unoctl = UNOController()
        self.unoctl.connect()

        self.pusher.push_task("Initialize Nav Controller")
        self.navctl = NavigationController(pusher)

        self.scan_finish_timestamp = 0



    def rough_scan(self, origin_x=0, origin_y=0, origin_theta=0):
        tag = _mm()

        self.pusher.push_task("Rough Map Stage - ToF Scan")
        self.unoctl.scan(_m("rmap/RoughScanRaw.npz", tag))
        LidarProcess.run(_m("rmap/RoughScanRaw.npz", tag), _m("rmap/RoughScanProcessed.npz", tag))

        self.pusher.push_task("Rough Map Stage - Point Cloud Clustering")
        clusters = RoughmapCluster.run(_m("rmap/RoughScanProcessed.npz", tag), _m("rmap/RoughScanCluster.npz", tag), display=self.display)

        self.pusher.push_roughmap_data(clusters)

        # correct robot position
        cos_t = np.cos(np.deg2rad(origin_theta))
        sin_t = np.sin(np.deg2rad(origin_theta))

        valid_clusters = []
        for i in range(len(clusters)):
            new_cl = []
            for j in range(len(clusters[i])):
                x, y = clusters[i][j]
                if np.hypot(x, y) < SENSOR_VALID_DIST:
                    xp = cos_t * x - sin_t * y + origin_x
                    yp = sin_t * x + cos_t * y + origin_y
                    new_cl.append(np.float32([xp, yp]))
            if len(new_cl) > 0:
                valid_clusters.append(new_cl)

        return valid_clusters

    def fine_scan(self):
        tag = _mm()
        self.pusher.push_task("Fine Map Stage - Capture Laser Scan")
        LaserCapture.run(_m("fmap/FineScanRaw.h264", tag), self.unoctl)

        self.pusher.push_task("Fine Map Stage - Point Space Reprojection")
        LaserGeometry.run(_m("fmap/FineScanRaw.h264", tag), _m("fmap/FineScanGeom.npz", tag), display=False)
        fine_result = LaserProjection.run(_m("fmap/FineScanGeom.npz", tag), display=False)

        if fine_result is None:
            self.pusher.push_laser_data({"type": None, "points": [[], []]})
        else:
            self.pusher.push_laser_data({"type": fine_result["type"], "points": fine_result["points"]})

        return fine_result

    def inspect_near_objects(self, objects_near):
        moved_once = False
        for i in range(len(objects_near)):
            if len(self.all_objects) < 8:
                self.pusher.push_task("Fine Map Stage - Approach Object")
                obj = objects_near[i]

                # go to object
                near_pt = obj["nearest_pt"]
                robot_pos_x, robot_pos_y, robot_theta = self.navctl.update_pos()

                is_reachable = Util.collision_free_object_approach(
                    robot_pos_x, robot_pos_y, robot_theta, near_pt[0], near_pt[1], objects_near, i)

                if not is_reachable:
                    logging.debug("Object not reachable due to colliding path!")
                    continue

                self.navctl.goto_pos(near_pt[0], near_pt[1], offset=OBJECT_INSPECT_OFFSET)
                moved_once = True

                # do laser scan
                fine_result = self.fine_scan()
                robot_pos_x, robot_pos_y, robot_theta = self.navctl.update_pos()

                theta = robot_theta - 90
                cos_t = np.cos(theta * np.pi / 180)
                sin_t = np.sin(theta * np.pi / 180)

                if not (fine_result is None):
                    xoff = fine_result["xoffset"]
                    yoff = fine_result["yoffset"]
                    xoff2 = xoff * cos_t - yoff * sin_t
                    yoff2 = xoff * sin_t + yoff * cos_t

                    xpos, ypos = float(robot_pos_x + xoff2), float(robot_pos_y + yoff2)

                    if np.hypot(near_pt[0] - xpos, near_pt[1] - ypos) > OBJECT_MATCH_DISTANCE:
                        self.pusher.push_task("Object position inconsistent, ignoring result")
                        continue

                    if fine_result["type"] == "cylinder":
                        self.all_objects.append({
                            "type": fine_result["type"],
                            "position": [xpos, ypos],
                            "radius": float(fine_result["radius"])
                        })
                    else:
                        self.all_objects.append({
                            "type": fine_result["type"],
                            "position": [xpos, ypos],
                            "angle": float(fine_result["theta"])
                        })
                    self.pusher.push_objects_data(self.all_objects)
        return moved_once

    def is_object_known(self, closest_pt):
        for obj in self.all_objects:
            obj_center = obj["position"]
            if Util.is_point_in_circle(*closest_pt, *obj_center, OBJECT_COLLISION_RADIUS):
                return True
        return False

    def extract_plan_single(self):
        clusters, robot_scan_pos = self.discover_neighbourhood()
        return self.seek_next_neighbourhood(clusters, robot_scan_pos)

    def seek_next_neighbourhood(self, clusters, robot_scan_pos):
        # find possible exit path
        free_angles = Util.collision_free_angles_strict(*robot_scan_pos, clusters)
        logging.debug("Free angles: {}".format(free_angles))

        robot_x, robot_y, robot_theta = robot_scan_pos

        for i in range(0, len(free_angles)):
            if free_angles[i]:
                goto_x = robot_x + OBJECT_TRAVERSE_DISTANCE * np.cos(np.deg2rad(-i*5 - robot_theta))
                goto_y = robot_y + OBJECT_TRAVERSE_DISTANCE * np.sin(np.deg2rad(-i*5 - robot_theta))
                for (xc, yc) in self.covered_circles:
                    if Util.is_point_in_circle(goto_x, goto_y, xc, yc, OBJECT_TRAVERSE_DISTANCE-50):
                        free_angles[i] = False

        # find movable angles
        movable_angles = Util.movable_directions(free_angles)
        logging.debug("Movable angles: {}".format(movable_angles))
        logging.debug("Robot position: {}".format(self.navctl.update_pos()))

        if len(movable_angles) == 0:
            free_angles = Util.collision_free_angles_relaxed(*robot_scan_pos, clusters)
            free_angles_2 = []
            for fa in free_angles:
                goto_x = robot_x + fa["dist"] * np.cos(np.deg2rad(-fa["angle"] - robot_theta))
                goto_y = robot_y + fa["dist"] * np.sin(np.deg2rad(-fa["angle"] - robot_theta))
                is_valid = True
                for (xc, yc) in self.covered_circles:
                    if Util.is_point_in_circle(goto_x, goto_y, xc, yc, OBJECT_TRAVERSE_DISTANCE + 50):
                        is_valid = False
                        break
                if is_valid:
                    free_angles_2.append(fa)
            free_angles = free_angles_2

            logging.debug("Free angles: {}".format(free_angles))

            if len(free_angles) == 0:
                self.pusher.push_task("Trapped")
                return False

            if free_angles[0]["dist"] < OBJECT_COLLISION_RADIUS:
                self.pusher.push_task("Trapped")
                return False

            goto_angle = free_angles[0]["angle"]
            goto_dist = free_angles[0]["dist"]
        else:
            target_idx = 0  # go through the largest unvisited window
            if SEEK_PATH_RANDOMIZED:
                target_idx = np.random.randint(0, min(4, len(movable_angles)))
            goto_angle = (movable_angles[target_idx][0] + movable_angles[target_idx][1]) / 2
            goto_dist = OBJECT_TRAVERSE_DISTANCE

        # go to exit location
        # TODO go random with checking previous location
        robot_x, robot_y, robot_theta = robot_scan_pos
        goto_position = (
            robot_x + goto_dist * np.cos(np.deg2rad(-goto_angle - robot_theta)),
            robot_y + goto_dist * np.sin(np.deg2rad(-goto_angle - robot_theta))
        )

        logging.debug("going to position {}".format(goto_position))
        self.navctl.goto_pos(*goto_position)

        return True

    def discover_neighbourhood(self):
        # check if tof platform returned home
        platform_return_left = time.time() - self.scan_finish_timestamp
        if platform_return_left < TOF_PLATFORM_RETURN_TIME:
            time.sleep(TOF_PLATFORM_RETURN_TIME - platform_return_left)

        # initial rough scan
        self.pusher.push_task("Discover Neighbourhood")
        clusters = self.rough_scan(*self.navctl.update_pos())
        self.scan_finish_timestamp = time.time()

        robot_scan_pos = self.navctl.update_pos()
        self.covered_circles.append((robot_scan_pos[0], robot_scan_pos[1]))
        self.pusher.push_coverage(self.covered_circles, OBJECT_TRAVERSE_DISTANCE)

        # filter object clusters
        obj_clusters, wall_clusters = Util.classify_clusters(clusters)

        # display walls
        self.save_wall_clusters(wall_clusters)

        # separate near and far clusters
        objects_near = Util.filter_clusters_by_distance(robot_scan_pos[0], robot_scan_pos[1], obj_clusters)

        # display object close points
        self.save_object_close_points(objects_near)

        # push map update
        self.pusher.push_map_data(self.all_clusters)

        # select unvisited objects
        objects_unvisited = [obj for obj in objects_near if not self.is_object_known(obj["nearest_pt"])]
        logging.debug("Detected objects: {}".format(len(objects_near)))
        logging.debug("Unvisited object: {}".format(len(objects_unvisited)))

        # perform fine mapping on nearby objects
        moved_once = self.inspect_near_objects(objects_unvisited)

        # go back to origin
        self.pusher.push_task("Map Exploration - Return To Origin")
        self.navctl.goto_pos(robot_scan_pos[0], robot_scan_pos[1], multistage=moved_once)

        # return clusters and scan location for exit path search
        return clusters, robot_scan_pos

    def save_object_close_points(self, objects_near):
        for cluster in objects_near:
            count = min(4, len(cluster["points"]))
            for xy in cluster["points"][0:count]:
                self.all_clusters.append([float(xy[0]), float(xy[1])])

    def save_wall_clusters(self, wall_clusters):
        for cluster in wall_clusters:
            for xy in cluster:
                self.all_clusters.append([float(xy[0]), float(xy[1])])
                self.all_walls.append([float(xy[0]), float(xy[1])])
        self.create_convexhull()

    def create_convexhull(self):
        wall_xy = np.asarray(self.all_walls, dtype=np.int64)
        self.convexhull = ConvexHull(wall_xy)
        pts = wall_xy[self.convexhull.vertices, :].tolist()
        self.pusher.push_walls_hull(pts)

    def extract_plan(self):
        logging.basicConfig(level=logging.DEBUG)
        while True:
            r = self.extract_plan_single()
            if not r:
                break
        self.pusher.push_task("Mapping Finished")
