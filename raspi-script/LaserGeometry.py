#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 23:01:00 2019

@author: fatih
"""

import numpy as np
import cv2
import logging
import concurrent.futures
import threading
from collections import namedtuple

MAX_FRAMES_IN_MEM = 192
MAX_THREADS = 4

def _run(args):
    logging.debug("start capturing input and intermediate output")
    try:
        capture_name = int(args.capture)
        logging.info("Reading from camera #{}".format(capture_name))
    except ValueError:
        capture_name = args.capture
        logging.info("Reading from file named {}".format(capture_name))
    
    cap = cv2.VideoCapture(capture_name)
    
    #cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    #cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    
    w = np.uint(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = np.uint(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    logging.debug("width: " + str(w) + " height: " + str(h))
    aggregate_pts = []
    
    for i in range(0, 60):
        _ = cap.read()
        
    logging.debug("get background")
    _, bg = cap.read()
    #bg = cv2.cvtColor(bg, cv2.COLOR_RGB2BGR)
    bg = bg[50:520, 260:1020]
    
    if args.display:
        cv2.imshow("bg", bg)
        cv2.waitKey(0)
        
    #logging.debug("TODO: calibrate for laser dot brightness")
    
    def process_frame(frame_orig, args, exit_flag, mem_sem):
        aggregate_pts = []
        #logging.debug("subtract background and emphasize regions of interest")
        if args.display:
            frame = frame_orig.copy()
        else:
            frame = frame_orig
        
        frame_diff = cv2.absdiff(frame, bg)
        frame_diff = cv2.cvtColor(frame_diff, cv2.COLOR_BGR2GRAY)
        max_diff = np.max(frame_diff)
        _, frame_diff = cv2.threshold(frame_diff, max(30, max_diff-20), 255, cv2.THRESH_BINARY)
        frame_diff = cv2.merge((frame_diff, frame_diff, frame_diff))
        frame = np.uint8(frame * np.float32(frame_diff) / (np.max(frame_diff) + 1))
        
        #logging.debug("TODO: correct for camera and device orientation")
        #M = cv2.getRotationMatrix2D((1920/2,1080/2),2,1)
        #frame = cv2.warpAffine(frame, M, (1920, 1080))
        
        #logging.debug("filter laser-like regions based on colour")
        ll = (50, 10, 50)
        ul = (150, 255, 255)
        
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv[:, :, 0] = (hsv[:, :, 0] + 90) % 180
        nonred = cv2.inRange(hsv, ll, ul)
        frame = cv2.bitwise_and(frame, frame, nonred)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        _, frame = cv2.threshold(frame, 60, 255, cv2.THRESH_BINARY)
        
        #frame2 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #frame2 = cv2.adaptiveThreshold(frame2, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 51, -10)
        
        #logging.debug("select laser dot based on contour shape and size")
        #logging.debug("TODO: cv2 version 3.2 returns image also")
        contours, _ = cv2.findContours(frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = list(filter(lambda c: (cv2.contourArea(c) > 50) and (cv2.arcLength(c, True) < 500), contours))
        logging.debug("Contours found #{}".format(len(contours)))
        if len(contours) > 0:
            #logging.debug("find contour center")
            largest = max(contours, key=cv2.contourArea)
            m = cv2.moments(largest)
            cx, cy = m['m10'] / m['m00'], m['m01'] / m['m00']
            aggregate_pts.append((cx, cy))
            logging.info("Contour center: {} {}".format(cx, cy)) #hsv[np.uint(cx), np.uint(cy), :]))
            
            if args.display:
                cv2.drawContours(frame_orig, contours, -1, (0, 255, 0), 1)
                cv2.drawContours(frame_orig, [largest], -1, (0, 255, 0), 3)
                cv2.circle(frame_orig, (np.uint(cx), np.uint(cy)), 5, (255, 0, 0), 3)
        
        if args.display:    
                #frame_orig = cv2.resize(frame_orig, None, fx=0.5, fy=0.5)
                cv2.imshow("orig", frame_orig)
                cv2.imshow("frame", ~frame)
                k = cv2.waitKey(0) & 0xFF
                if k == ord('q'):
                    exit_flag.set()
                    return None
                elif k == ord('a'):
                    cap.set(cv2.CAP_PROP_POS_FRAMES, cap.get(cv2.CAP_PROP_POS_FRAMES) - 2)
        
        mem_sem.release()
        return aggregate_pts
    
    result_futures = []
    mempool_sem = threading.Semaphore(MAX_FRAMES_IN_MEM)
    exit_flag = threading.Event()
    
    with concurrent.futures.ThreadPoolExecutor(MAX_THREADS) as pool:
        while (not exit_flag.is_set()):
            #logging.debug("get frame")
            read, frame_orig = cap.read()
            if not read:
                break
            
            #frame_orig = cv2.cvtColor(frame_orig, cv2.COLOR_RGB2BGR)
            frame_part = frame_orig[50:520, 260:1020]
            
            mempool_sem.acquire()
            result_futures.append(
                pool.submit(
                    process_frame, 
                    frame_part, 
                    args, 
                    exit_flag, 
                    mempool_sem
               )
            )
    
    for future in concurrent.futures.as_completed(result_futures):
        aggregate_pts += future.result()
    
    if cap.isOpened():
        cap.release()
    
    logging.debug("save data points")
    np.savez(args.outfile, pts=aggregate_pts)
    if args.display:
        cv2.destroyAllWindows()

def run(capture, outfile, display=False):
    Args_ = namedtuple("Args_", ["capture", "outfile", "display"])
    args = Args_(capture, outfile, display)
    _run(args)

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--capture", required=True, action="store")
    parser.add_argument("-d", "--display", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=False, action="store_true")
    parser.add_argument("-o", "--outfile")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    _run(args)
