#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu May 16 06:56:54 2019

@author: fatih
"""

from flask import Flask, jsonify
from flask_socketio import SocketIO
from PlanExtractor import PlanExtractor
import RPi.GPIO as GPIO

class CollectorProxy:
    def __init__(self, socketio):
        self._socketio = socketio

    def push_task(self, message):
        self._socketio.emit("update-task", message)

    def push_roughmap_data(self, clusters):
        payload = []
        for cluster in clusters:
            xs = []
            ys = []
            for (x, y) in cluster:
                xs.append(x)
                ys.append(y)
            payload.append({"xs": xs, "ys": ys})
        self._socketio.emit("update-rough", payload)

    def push_laser_data(self, laser_data):
        payload = {}
        payload["dataGroups"] = []
        payload["dataGroups"].append({"xs": laser_data["points"][0], "ys": laser_data["points"][1]})
        if len(laser_data["points"]) == 4:
            payload["dataGroups"].append({"xs": laser_data["points"][2], "ys": laser_data["points"][3]})

        if laser_data["type"] == "cylinder":
            payload["message"] = "Object is a cylinder"
        elif laser_data["type"] == "triangle":
            payload["message"] = "Object is a triangle base prism"
        elif laser_data["type"] == "square":
            payload["message"] = "Object is a square base prism"
        else:
            payload["message"] = "Object type could not be determined!"

        self._socketio.emit("update-laser", payload)

    def push_map_data(self, raw_points):
        self._socketio.emit("update-map", raw_points)

    def push_objects_data(self, objects):
        self._socketio.emit("update-objects", objects)

    def push_robot_data(self, location, angle):
        self._socketio.emit("update-robot", {"location": location, "angle": angle})

    def push_coverage(self, covered_circles, radius):
        self._socketio.emit("update-coverage", {"circles": covered_circles, "radius": radius})

    def push_walls_hull(self, pts):
        self._socketio.emit("update-hull", pts)


def start_scan():
    pusher = CollectorProxy(socketio)
    pe = PlanExtractor(pusher)
    pe.extract_plan()


def gpio_callback(channel):
    start_scan()


app = Flask(__name__)
socketio = SocketIO(app)

GPIO.setmode(GPIO.BCM)
GPIO.setup(4, GPIO.IN, pull_up_down=GPIO.PUD_UP)
GPIO.add_event_detect(4, GPIO.FALLING, callback=gpio_callback)


@socketio.on("scan")
def handle_scan():
    start_scan()


if __name__ == "__main__":
    socketio.run(app, host="0.0.0.0", port=5000)