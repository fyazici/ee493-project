#ifndef RT_STEPPER_HPP_
#define RT_STEPPER_HPP_

template<int P1, int P2, int P3, int P4, bool HIGH_>
struct RT_Stepper {
private:
  int step_count_;
  int step_target_;
  bool enabled_;
  bool override_;
  int step_delay_;
  unsigned long schedule_next_;

  void __attribute__((always_inline)) apply_step(int step_number) {
    switch (step_number & 0x07) {
      case 0x00:
        digitalWrite(P1, HIGH_);
        digitalWrite(P2, !HIGH_);
        digitalWrite(P3, !HIGH_);
        digitalWrite(P4, !HIGH_);
        break;
      case 0x01:
        digitalWrite(P1, HIGH_);
        digitalWrite(P2, HIGH_);
        digitalWrite(P3, !HIGH_);
        digitalWrite(P4, !HIGH_);
        break;
      case 0x02:
        digitalWrite(P1, !HIGH_);
        digitalWrite(P2, HIGH_);
        digitalWrite(P3, !HIGH_);
        digitalWrite(P4, !HIGH_);
        break;
      case 0x03:
        digitalWrite(P1, !HIGH_);
        digitalWrite(P2, HIGH_);
        digitalWrite(P3, HIGH_);
        digitalWrite(P4, !HIGH_);
        break;
      case 0x04:
        digitalWrite(P1, !HIGH_);
        digitalWrite(P2, !HIGH_);
        digitalWrite(P3, HIGH_);
        digitalWrite(P4, !HIGH_);
        break;
      case 0x05:
        digitalWrite(P1, !HIGH_);
        digitalWrite(P2, !HIGH_);
        digitalWrite(P3, HIGH_);
        digitalWrite(P4, HIGH_);
        break;
      case 0x06:
        digitalWrite(P1, !HIGH_);
        digitalWrite(P2, !HIGH_);
        digitalWrite(P3, !HIGH_);
        digitalWrite(P4, HIGH_);
        break;
      case 0x07:
        digitalWrite(P1, HIGH_);
        digitalWrite(P2, !HIGH_);
        digitalWrite(P3, !HIGH_);
        digitalWrite(P4, HIGH_);
        break;
      default:
        break;
    }
  }
  
public:
  void begin() {    
    step_count_ = 0;
    step_target_ = 0;
    enabled_ = false;
    override_ = false;
    step_delay_ = 10;   // 100Hz
    schedule_next_ = 0;
    pin_setup();
    stall();
  }

  void pin_setup() {
    pinMode(P1, OUTPUT);
    pinMode(P2, OUTPUT);
    pinMode(P3, OUTPUT);
    pinMode(P4, OUTPUT);
  }

  void stall() {
    disable();
    digitalWrite(P1, !HIGH_);
    digitalWrite(P2, !HIGH_);
    digitalWrite(P3, !HIGH_);
    digitalWrite(P4, !HIGH_);
  }

  void brake() {
    disable();
    digitalWrite(P1, HIGH_);
    digitalWrite(P2, HIGH_);
    digitalWrite(P3, HIGH_);
    digitalWrite(P4, HIGH_);
  }

  void soft_break() { 
    disable();
    step_target_ = step_count_; 
    override_ = false;
    enable();
  }

  int get_step_count() const { return step_count_; }
  void set_step_count(int step_count) { step_count_ = step_count; }

  int get_step_target() const { return step_target_; }
  void set_step_target(int step_target, bool enabled = true) { 
    step_target_ = step_target; 
    enabled_ = enabled;
  }
  
  int get_step_delay() const { return step_delay_; }
  void set_step_delay(int step_delay) { step_delay_ = step_delay; }
  
  bool is_enabled() const { return enabled_; }
  void enable() { enabled_ = true; }
  void disable() { enabled_ = false; }

  bool is_busy() { return override_ || (step_target_ != step_count_); }

  int steps_left() { return (step_target_ - step_count_); }

  void set_step_target_blocking(int step_target) {
    set_step_target(step_target);
    while (is_busy()) update();
  }

  void override_forward() {
    step_target_ = 1;
    override_ = true;
  }

  void override_backward() {
    step_target_ = 0;
    override_ = true;
  }

  template<typename F>
  void override_forward_until(F&& f) {
    step_target_ = 1;
    override_ = true;
    enable();
    while (f());
    disable();
    override_ = false;
    step_target_ = step_count_;
    enable();
  }

  template<typename F>
  void override_backward_until(F&& f) {
    step_target_ = 0;
    override_ = true;
    enable();
    while (f());
    disable();
    override_ = false;
    step_target_ = step_count_;
    enable();
  }

  void update(unsigned long t) {
    if (enabled_) {
      if (t > schedule_next_) {
        schedule_next_ = t + step_delay_;
        if (override_) {
          if (step_target_ > 0) {
            apply_step(++step_count_);
          } else {
            apply_step(--step_count_);
          }
        } else {
          if (step_target_ > step_count_) {
            apply_step(++step_count_);
          } else if (step_target_ < step_count_) {
            apply_step(--step_count_);
          }
        }
      }
    }
  }
};

#endif
