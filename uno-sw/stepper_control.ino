#include "RT_Stepper.hpp"
#include "RT_CNY70.hpp"
#include "RT_Util.hpp"
#include "RT_Comms.hpp"
#include <Servo.h>

#include <ComponentObject.h>
#include <RangeSensor.h>
#include <SparkFun_VL53L1X.h>
#include <vl53l1x_class.h>
#include <vl53l1_error_codes.h>
#include <Wire.h>
#include "SparkFun_VL53L1X.h"

//#define RT_DEBUG
#include "RT_Debug.hpp"

/*
 * LIDAR library object
 */
SFEVL53L1X distanceSensor1;
SFEVL53L1X distanceSensor2;
const int XSHUT_1 = 7;
const int SENSORS_VCC = 6;

/*
 * Calibration arm object
 */
Servo calibArm;

/*
 * Stepper motor controller object
 */

RT_Stepper<8, 9, 10, 11, true> stepper;

/*
 * Optical Encoder Controller object
 */

RT_CNY70<2> sensor;
RT_IMPLEMENT_CNY70_INT(2, sensor);

/*
 * Sensor and motor calibration controller
 */

struct {
  int lidar_offset = 0;
  int target_pulse_count = 0;
  
  void platform_goto_angle(int angle) {
    /*
    target_pulse_count = angle / 10;
    if (sensor.get_pulse_count() > target_pulse_count) {
      sensor.set_backward();
      stepper.override_backward();
    } else if (sensor.get_pulse_count() < target_pulse_count) {
      sensor.set_forward();
      stepper.override_forward();
    }*/
    // no calibration - no feedback
    stepper.set_step_target(angle * (long)4096 / 360);
  }

  bool wait_settle() {
    /*
    debug_logln(" target: ", target_pulse_count, " steps: ", stepper.get_step_count(), " pulse: ", sensor.get_pulse_count());
    if (sensor.get_pulse_count() > target_pulse_count) {
      sensor.set_backward();
      stepper.override_backward();
    } else if (sensor.get_pulse_count() < target_pulse_count) {
      sensor.set_forward();
      stepper.override_forward();
    } else {
      stepper.soft_break();
      return true;
    }
    */
    return (!stepper.is_busy());
  }
} calib;

/*
 * Calibration task
 */

struct {
  const int CALIB_ARM_H = 33;
  const int CALIB_ARM_V = 135;
  const int CALIB_ARM_DISTANCE = 75;
  const int CALIB_ARM_RANGE = 120;
  const int CALIB_ARM_CENTER_STEPS = 150;

  unsigned long schedule_next = 0;
  average_helper<int> distance_avg;
  enum class State : int {
    Seeking,
    Minimizing,
    Averaging
  } state;

  void setup() {
    calibArm.attach(5);
    calibArm.write(CALIB_ARM_H);
  }

  void begin() {
    calibArm.write(CALIB_ARM_V); // raise the calibration arm
    delay(50);
    schedule_next = millis() + 100;
    distance_avg.reset();
    stepper.override_backward();
    state = State::Seeking;
  }

  bool update(unsigned long t) {
    switch (state) {
      case State::Seeking:
      {
        auto dist = distanceSensor1.getDistance();
        debug_logln(" distance: ", dist);
        if (dist < CALIB_ARM_RANGE) { /* we have seen the calibration arm */
          stepper.soft_break();
          auto steps = stepper.get_step_count();
          stepper.set_step_target(steps - CALIB_ARM_CENTER_STEPS);
          state = State::Minimizing;
        }
        break;
      }
      case State::Minimizing:
      {
        if (!stepper.is_busy()) {
          stepper.set_step_count(0);
          stepper.soft_break();
//          sensor.set_pulse_count(0);
          state = State::Averaging;
        }
        break;
      }
      case State::Averaging:
      {
        distanceSensor1.clearInterrupt();
        while(!distanceSensor1.checkForDataReady());
        
        auto dist = distanceSensor1.getDistance();
        if (distanceSensor1.getRangeStatus() != 2) {  /* timeout check */
          distance_avg.update(dist);
          debug_logln(" distance2: ", dist);
        } else {
          debug_logln(" timeout ");
        }
        if (distance_avg.count > 50) {
          calib.lidar_offset = CALIB_ARM_DISTANCE - distance_avg.average();
          stepper.soft_break();
          return true;
        }
        break;
      }
    }
    return false;
  }

  void end() {
    calibArm.write(CALIB_ARM_H);
    debug_logln(" LIDAR_OFFSET=", calib.lidar_offset);
    respond("END");
  }
  
} calib_task;

/*
 * Scan Task
 */

struct {
  int angle;
  static const int ANGLE_MAX = 180;
  static const int ANGLE_STEP = 1;

  int repetition;
  static const int REPETITION_MAX = 2;

  bool return_home;

  average_helper<unsigned long> distance_avg_1, distance_avg_2;

  void begin() {
    angle = 0;
    repetition = 0;
    distance_avg_1.reset();
    distance_avg_2.reset();
    calib.platform_goto_angle(0);
    return_home = false;
  }

  bool update(unsigned long /* unused */) {
    if (!calib.wait_settle()) {
      return false; // not finished
    } else {
      if (return_home) {
        return true;
      } else if (repetition < REPETITION_MAX) {
        // take measurement

        distanceSensor1.clearInterrupt();
        distanceSensor2.clearInterrupt();

        while (!distanceSensor1.checkForDataReady());
        
        auto meas = distanceSensor1.getDistance();
        if (distanceSensor1.getRangeStatus() != 2)  /* timeout check */
        {
          distance_avg_1.update(24 * meas / 25 + 60);
        }

        while (!distanceSensor2.checkForDataReady());
        
        meas = distanceSensor2.getDistance();
        if (distanceSensor2.getRangeStatus() != 2)  /* timeout check */
        {
          distance_avg_2.update(24 * meas / 25 + 60);
        }
        
        repetition++;
      } else {
        // print measurements
        int32_t d1 = (distance_avg_1.count == 0) ? (-1) : (distance_avg_1.average());
        int32_t d2 = (distance_avg_2.count == 0) ? (-1) : (distance_avg_2.average());
        
        respond(angle, ", ", d1, ", ", angle + 180, ", ", d2);
        
        // reset avg
        distance_avg_1.reset();
        distance_avg_2.reset();
        
        repetition = 0;
        angle += ANGLE_STEP;
        if (angle > ANGLE_MAX) {
          calib.platform_goto_angle(0);
          return_home = true;
          return true;
        }
        calib.platform_goto_angle(angle);
      }
      return false;
    }
  }

  void end() {
    respond("END");
    //stepper.stall();
  }
  
} scan_task;

/*
 * Fake scan task
 */

struct {
  unsigned long schedule_next;
  int counter;

  void begin() {
    schedule_next = 0;
    counter = 0;
  }

  bool update(unsigned long t) {
    if (t > schedule_next) {
      respond(counter, ",", counter);
      counter++;
      
      if (counter > 360) {
        return true;  // finished
      }

      schedule_next = t + 10;  // 10ms between measurements
    }

    return false; // not finished
  }
  
  void end() {
    respond("END");
  }
  
} fake_scan_task;

/*
 * Continuous Measurement Task
 */
struct {

  int repetition;
  static const int REPETITION_MAX = 5;
  average_helper<unsigned long> distance_avg_1, distance_avg_2;
  
  void begin() {
    calib.platform_goto_angle(0);
    distance_avg_1.reset();
    distance_avg_2.reset();
  }

  bool update(unsigned long t) {
    if (!calib.wait_settle()) {
      return false;
    } else {
      if (repetition < REPETITION_MAX) {
        // take measurement
        distanceSensor1.clearInterrupt();
        distanceSensor2.clearInterrupt();

        while (!distanceSensor1.checkForDataReady());
        auto meas = distanceSensor1.getDistance();
        if (distanceSensor1.getRangeStatus() != 2)
        {
          // distance_avg_1.update(meas + calib.lidar_offset);
          distance_avg_1.update(24 * meas / 25 + 60);
        }

        while (!distanceSensor2.checkForDataReady());
        meas = distanceSensor2.getDistance();
        if (distanceSensor2.getRangeStatus() != 2)
        {
          distance_avg_2.update(24 * meas / 25 + 60);
        }
        
        repetition++;
      } else {
        // print measurements
        respond("DISTANCE: ", 
            (distance_avg_1.count == 0) ? (-1) : (int32_t) distance_avg_1.average(), " mm ", 
            (distance_avg_2.count == 0) ? (-1) : (int32_t) distance_avg_2.average(), " mm");
        
        // reset avg
        distance_avg_1.reset();
        distance_avg_2.reset();
        
        repetition = 0;
      }
      return false;
    }
  }

  void end() {
    // empty
  }
  
} cont_meas_task;

/*
 * Fine mapping laser sweep task
 */
struct {
  const int LASER_ARM_PIN = 4;
  const int LASER_PWR_PIN = 3;
  const int ANGLE_MIN = 50;
  const int ANGLE_MAX = 130;
  const int ANGLE_STEP = 1;
  const unsigned long STEP_TIME = 100;
  
  Servo laser_arm;
  bool running = false;
  int direction = 1;
  TimepointScheduler mvmt_timer;
  
  void setup() {
    laser_arm.attach(LASER_ARM_PIN);
    //pinMode(LASER_PWR_PIN, OUTPUT);
    mvmt_timer.start(millis(), STEP_TIME);
  }

  void start() { running = true; }
  void stop() { running = false; }

  bool update(unsigned long t)
  {
    if (running && mvmt_timer.is_due(t))
    {
      mvmt_timer.start(t, STEP_TIME);

      auto current = laser_arm.read();
      debug_logln("laser current: ", current);
      if (current > ANGLE_MAX)
      {
        direction = -ANGLE_STEP;
      }
      else if (current < ANGLE_MIN)
      {
        direction = ANGLE_STEP;
      }

      laser_arm.write(current + direction);
    }
    return false;
  }

  void set_angle(float angle)
  {
    angle = constrain(angle, ANGLE_MIN, ANGLE_MAX);
    laser_arm.write(angle);
  }

  void set_pwm(int pwm)
  {
    analogWrite(LASER_PWR_PIN, pwm);
  }
  
} laser_sweep;

/*
 * OS Boot
 */

void setup() {
  //sensor.begin();
  //RT_ATTACH_CNY70_INT(2);
  //sensor.set_pulse_count(0);

  { /* lidar initialization */

    pinMode(SENSORS_VCC, OUTPUT);
    digitalWrite(SENSORS_VCC, LOW);
    delay(100);
    digitalWrite(SENSORS_VCC, HIGH);
    delay(100);
    
    pinMode(XSHUT_1, OUTPUT);
    digitalWrite(XSHUT_1, LOW);
    delay(100);

    Wire.begin();
    
    Serial.begin(1000000);

    if (distanceSensor1.begin() == false)
    {
      debug_logln("Sensor 1 online");
    }   

    distanceSensor1.setI2CAddress(70);
    
    pinMode(XSHUT_1, INPUT);
    delay(100);
    
    if (distanceSensor2.begin() == false)
    {
      debug_logln("Sensor 2 online");
    }

    //distanceSensor1.setROI(4, 4);
    //distanceSensor1.setDistanceModeShort();
    //distanceSensor1.setTimingBudgetInMs(50);

    distanceSensor1.setROI(4, 4);
    distanceSensor1.setDistanceModeLong();
    distanceSensor1.setTimingBudgetInMs(50);
    
    distanceSensor2.setROI(4, 4);
    distanceSensor2.setDistanceModeLong();
    distanceSensor2.setTimingBudgetInMs(50);
    
    distanceSensor1.startRanging();
    distanceSensor2.startRanging();
    
  }

  calib_task.setup();
  laser_sweep.setup();
  
  stepper.begin();
  stepper.enable();
  stepper.set_step_delay(10);

  debug_logln(" READY");

  //sensor.set_pulse_count(0);
}

/*
 * State machine states
 */
enum class State {
  WaitCmd,
  ParseCmd,
  Scanning,
  FakeScanning,
  Calibrating,
  ContMeasuring
} state = State::WaitCmd;

/*
 * Command line buffer
 */
char cmd_buffer[32] = {0};
int cmd_index = 0;

/*
 * OS Event Loop
 */
void loop() {
  unsigned long t = millis(); // current time
  
  // hw job 1 update stepper position
  stepper.update(t);

  // hw job 2 update cny70 counter
  //sensor.update(t);

  // hw job 3 update laser sweep position
  laser_sweep.update(t);

  // main state machine
  bool task_finished = false;
  switch (state) {
    case State::WaitCmd:
      if (Serial.available()) {
        int c = Serial.read();
        if (c != '\n') {
          cmd_buffer[cmd_index++] = c;
        } else {
          cmd_buffer[cmd_index] = 0;
          cmd_index = 0;
          state = State::ParseCmd;
        }
      }
      break;
    case State::ParseCmd:
      switch (cmd_buffer[0]) {
        case 'I':
          respond("UNO");
          respond("ACK");
          state = State::WaitCmd;
          break;
        case 'S':
          respond("ACK");
          state = State::Scanning;
          scan_task.begin();
          break;
        case 'F':
          respond("ACK");
          stepper.override_forward();
          state = State::WaitCmd;
          break;
        case 'Q':
          respond("ACK");
          // fake scan
          state = State::FakeScanning;
          fake_scan_task.begin();
          break;
        case 'C':
          respond("ACK");
          state = State::Calibrating;
          calib_task.begin();
          break;
        case 'M':
          respond("ACK");
          state = State::ContMeasuring;
          cont_meas_task.begin();
          break;
        case 'L':
          debug_logln("laser ", cmd_buffer[1]);
          if (cmd_buffer[1] == 'I')
          {
            respond("ACK");
            laser_sweep.start();
          }
          else if (cmd_buffer[1] == 'O')
          {
            respond("ACK");
            laser_sweep.stop();
          }
          else if (cmd_buffer[1] == 'P')
          {
            respond("ACK");
            float target_angle = atof(cmd_buffer + 3);
            laser_sweep.set_angle(target_angle);
          }
          else if (cmd_buffer[1] == 'B')
          {
            respond("ACK");
            int target_pwm = atoi(cmd_buffer + 3);
            laser_sweep.set_pwm(target_pwm);
          }
          else
          {
            respond("NACK");
          }
          state = State::WaitCmd;
          break;
          // add further commands
        default:
          respond("NACK");
          state = State::WaitCmd;
          break;
      }
      break;
    case State::Scanning:
      task_finished = scan_task.update(t);
      if (task_finished) {
        scan_task.end();
        state = State::WaitCmd;
      }
      break;
    case State::FakeScanning:
      task_finished = fake_scan_task.update(t);
      if (task_finished) {
        fake_scan_task.end();
        state = State::WaitCmd;
      }
      break;
    case State::Calibrating:
      task_finished = calib_task.update(t);
      if (task_finished) {
        calib_task.end();
        state = State::WaitCmd;
      }
      break;
    case State::ContMeasuring:
      task_finished = cont_meas_task.update(t);
      if (task_finished) {
        cont_meas_task.end();
        state = State::WaitCmd;
      }
  }
}
