#ifndef RT_CNY70_HPP_
#define RT_CNY70_HPP_

#define RT_ATTACH_CNY70_INT(pin) \
attachInterrupt(digitalPinToInterrupt(pin), \
  &RT_CNY70_InterruptHandler##pin,\
  RISING)

#define RT_IMPLEMENT_CNY70_INT(pin, inst) \
void RT_CNY70_InterruptHandler##pin(void) { \
  inst.set_pending(); \
}

template<int SensorPin>
struct RT_CNY70 {
    
  void begin() {
    pending_update_ = true;
    debounce_ = false;
    pulse_count_ = 0;
    pulse_dir_ = PulseDirection::Forward;
    pinMode(SensorPin, INPUT_PULLUP);
  }

  int get_pulse_count() const { return pulse_count_; }
  void set_pulse_count(int pulse_count) { pulse_count_ = pulse_count; }

  int get_pulse_dir() const { return (int) pulse_dir_; }
  void set_forward() { pulse_dir_ = PulseDirection::Forward; }
  void set_backward() { pulse_dir_ = PulseDirection::Backward; }

  void set_pending() {
    pending_update_ = true;
  }

  void update(unsigned long t) {
    if (debounce_) {
      if (t > (debounce_time_ + 50)) {
        if (digitalRead(SensorPin) == HIGH) {
          pulse_count_ += (int) pulse_dir_;
        } else {
          Serial.println("glitch");
        }
        debounce_ = false;
      }
    } else {
      if (pending_update_ && (digitalRead(SensorPin) == HIGH)) {
        debounce_ = true;
        debounce_time_ = t;
      }
      pending_update_ = false;
    }
  }

private:
  enum class PulseDirection : int {
    Forward = 1,
    Backward = -1
  };

  bool debounce_;
  unsigned long debounce_time_;
  volatile bool pending_update_;
  int pulse_count_;
  PulseDirection pulse_dir_;
};

#endif
