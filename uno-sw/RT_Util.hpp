#ifndef RT_UTIL_HPP_
#define RT_UTIL_HPP_

template<typename T>
struct average_helper {
  T sum;
  size_t count;
  void reset() { sum = 0; count = 0; }
  void update(T value) { sum += value; count++; }
  T average() { return (sum / count); }
};

struct TimepointScheduler {
  unsigned long schedule_start;
  unsigned long schedule_next;

  void start(unsigned long t, unsigned long delta) { schedule_start = t; schedule_next = t + delta; }

  bool is_due(unsigned long t) { return (schedule_next < t); }
  unsigned long elapsed(unsigned long t) { return (t - schedule_start); }
};

#endif
