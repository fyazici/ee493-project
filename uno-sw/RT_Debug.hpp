#ifndef RT_DEBUG_HPP_
#define RT_DEBUG_HPP_

/* define RT_DEBUG */

namespace detail {

template<typename T>
void debug_log_impl(T&& t) {
#ifdef RT_DEBUG
    Serial.print(t);
#endif
}

template<typename T, typename ... Ts>
void debug_log_impl(T&& t1, Ts&& ... ts) {
#ifdef RT_DEBUG
    Serial.print(t1);
    debug_log_impl(static_cast<Ts&&>(ts)...);
#endif
}
}

template<typename ... Ts>
void debug_log(Ts && ... ts) {
#ifdef RT_DEBUG
    Serial.print("# ");
    Serial.print(millis());
    detail::debug_log_impl(static_cast<Ts&&>(ts)...);
#endif
}

template<typename ... Ts>
void debug_logln(Ts && ... ts) {
#ifdef RT_DEBUG
    debug_log(static_cast<Ts&&>(ts)...);
    Serial.println();
#endif
}

#endif
