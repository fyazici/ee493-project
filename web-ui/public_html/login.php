<?php
require("functions/users.php");

session_start();
if (isset($_SESSION['user_id'])) {
    header('Location: index.php');
    exit();
}

$username = null;
$password = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (!empty($_POST["username"]) && !empty($_POST["password"])) {
        $username = $_POST["username"];
        $password = $_POST["password"];
        
        if (check_user($username, $password)) {
            $_SESSION['user_id'] = $username;
	    header("Location: index.php");
	    exit();
        } else {
	    header("Location: login.php");
	    exit();
        }
    } else {
        header("Location: login.php");
        exit();
    }
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <link rel="stylesheet" href="dist/bootstrap.min.css">
        
        <link href="assets/login.css" rel="stylesheet">

        <title>Login | Robothings</title>
        
        <link rel="icon" type="image/png" href="assets/img/favicon.png">
    </head>
    <body class="text-center">
        <form class="form-login" method="POST">
            <img class="mb-4" src="assets/img/logo.png" alt="" width=300>
            <h1 class="h3 mb-3 font-weight-normal">Please log in</h1>
            <label for="username" class="sr-only">Username:</label>
            <input id="username" name="username" type="username" class="form-control" placeholder="Username" required autofocus>
            <label for="password" class="sr-only">Password:</label>
            <input id="password" name="password" type="password" class="form-control" placeholder="Password" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit" >Log in</button>
        </form>
        
        <script src="dist/popper.min.js"></script>
        <script src="dist/angular.min.js"></script>
        <script src="dist/ui-bootstrap-tpls-2.5.0.min.js"></script>
    </body>
</html>
