<?php
require("authenticate.php");
require("functions/session.php");
?>

<!doctype html>
<html lang="en" ng-app="rtWebUI">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Robothings Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="dist/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="assets/common.css" rel="stylesheet">
    <link href="assets/dashboard.css" rel="stylesheet">
    
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
</head>

<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-nowrap p-0 shadow">
        <a class="navbar-brand col-md-1 col-sm-4" href="#">Robothings</a>
        <ul class="navbar-nav">
            <li class="nav-item text-nowrap" style="color: white">
                Welcome,
                <?php get_login_user(); ?>!
            </li>
        </ul>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="logout.php">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-1 bg-light sidebar">
                <div class="py-2 sticky-top flex-grow-1">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <!-- TODO: other views list here -->
                            <a class="nav-link active" href="#">
                                <span data-feather="home"></span>
                                Device Status <span class="sr-only">(current)</span>
                            </a>
                            <a class="nav-link" href="plan.php">
                                <span data-feather="map"></span>
                                Plan Extraction
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-11 ml-sm-auto col-lg-11 px-4" ng-controller="DeviceStatusController as deviceStatus">
                <!-- Device Status page header -->
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Device Status</h1>
                    <div>
                        <button class="btn btn-sm btn-outline-secondary" ng-click="deviceStatus.refresh()">
                            <span class="status-refreshing-{{deviceStatus.refreshing}}" data-feather="refresh-cw"></span>
                            Refresh
                        </button>
                    </div>
                </div>
                
                <!-- Device Status page content -->    
                <div class="table-responsive">
                    <table class="table table-bordered table-striped status-refreshing-table-{{deviceStatus.refreshing}}">
                        <tr>
                            <td style="width: 200px">Device ID:</td>
                            <td><span ng-bind="deviceStatus.raspiid"></span></td>
                        </tr>
                        <tr>
                            <td>Device Status:</td>
                            <td><span class="status-online-{{deviceStatus.online}}" ng-bind="deviceStatus.status"></span></td>
                        </tr>
                        <tr>
                            <td>Device Address:</td>
                            <td><span style="font-family: monospace" ng-bind="deviceStatus.ipaddress"></span></td>
                        </tr>
                        <tr>
                            <td>Last Boot Updated:</td>
                            <td><span ng-bind="deviceStatus.lastupdate"></span></td>
                        </tr>
                    </table>
                </div>
                
            </main>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script>
        window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')

    </script>
    <script src="dist/popper.min.js"></script>
    <script src="dist/angular.min.js"></script>
    <script src="dist/ui-bootstrap-tpls-2.5.0.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
        feather.replace()
    </script>
    
    <!-- Dashboard Scripts -->
    <script src="js/dashboard.js"></script>
</body>

</html>
