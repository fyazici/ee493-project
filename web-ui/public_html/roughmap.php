<?php
require("authenticate.php");
require("functions/session.php");
?>

<!doctype html>
<html lang="en" ng-app="rtWebUI">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Robothings Dashboard</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="dist/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="assets/common.css" rel="stylesheet">

    <link rel="icon" type="image/png" href="assets/img/favicon.png">
</head>

<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-nowrap p-0 shadow">
        <a class="navbar-brand col-md-2 col-sm-4" href="#">Robothings</a>
        <ul class="navbar-nav">
            <li class="nav-item text-nowrap" style="color: white">
                Welcome,
                <?php get_login_user(); ?>!
            </li>
        </ul>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="logout.php">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-2 bg-light sidebar">
                <div class="py-2 sticky-top flex-grow-1">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <!-- TODO: other views list here -->
                            <a class="nav-link" href="index.php">
                                <span data-feather="home"></span>
                                Device Status
                            </a>
                            <a class="nav-link" href="plan.php">
                                <span data-feather="map"></span>
                                Plan Extraction
                            </a>
                            <a class="nav-link active" href="#">
                                <span data-feather="map"></span>
                                Rough Mapping <span class="sr-only">(current)</span>
                            </a>
                            
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4" ng-controller="RoughMappingController as mapController">
                <!-- Rough Mapping page header -->
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Rough Mapping</h1>
                </div>

                <!-- Rough Mapping page content -->
                <div>
                    <div class="btn-group">
                        <button class="btn btn-sm btn-outline-secondary" ng-click="mapController.refresh()">
                            <span data-feather="refresh-cw"></span>
                            Refresh
                        </button>
                        <button class="btn btn-sm btn-outline-secondary" ng-click="mapController.connect()" ng-disabled="mapController.connected">
                            <span data-feather="play"></span>
                            Connect
                        </button>
                        <button class="btn btn-sm btn-outline-secondary" ng-disabled="(!mapController.connected)||(mapController.scanning)" ng-click="mapController.scan()">
                            <span data-feather="settings"></span>
                            Scan
                        </button>
                        <button class="btn btn-sm btn-outline-secondary" ng-disabled="!mapController.connected" ng-click="mapController.disconnect()">
                            <span data-feather="x"></span>
                            Disconnect
                        </button>
                    </div>
                    <div class="d-flex float-right">
                        <label class="col-form-label text-nowrap px-2">Refresh</label>
                        <select class="form-control form-control-sm" ng-model="mapController.refreshInterval">
                            <option value="100">every 100 ms</option>
                            <option value="500">every 500 ms</option>
                            <option value="1000">every second</option>
                        </select>
                    </div>
                </div>

                <div class="table-responsive" style="padding: 30px 0;">
                    <table class="table table-bordered">
                        <tr>
                            <td style="width: 200px;">Status:</td>
                            <td><span ng-bind="mapController.status"></span></td>
                        </tr>
                    </table>
                </div>

                <div id="roughMapDiv"></div>
                
                <div clas="text">
                    <h2 class="h2">Raw Sensor Data</h2>
                    <span ng-bind="mapController.xy" style="font-family: monospace"></span>
                </div>
            </main>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/popper.min.js"></script>
    <script src="dist/angular.min.js"></script>
    <script src="dist/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>

    <!-- Icons -->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <script>
        feather.replace()

    </script>

    <!-- Dashboard Scripts -->
    <script src="js/dashboard.js"></script>
</body>

</html>
