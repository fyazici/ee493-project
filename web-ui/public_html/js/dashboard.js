module = angular.module('rtWebUI', []);

module.controller('DeviceStatusController', function ($http) {
    var self = this;
    self.raspiid;
    self.online;
    self.status;
    self.ipaddress;
    self.lastupdate;
    self.refreshing = false;

    self.refresh = function () {
        self.refreshing = true;
        $http({
            url: "raspberry.php",
            method: "POST",
            data: {
                "action": "refresh"
            }
        }).then(function (response) {
            d = response.data;
            self.raspiid = d.raspiid;
            self.online = d.online;
            self.status = d.online ? "Online" : "Offline";
            self.ipaddress = d.ipaddress;
            self.lastupdate = d.lastupdate;
            self.refreshing = false;
        }, function (error) {
            self.raspiid = "-";
            self.online = false;
            self.status = "(refresh failed)";
            self.ipaddress = "-";
            self.lastupdate = "-";
            self.refreshing = false;
        });
    };

    self.refresh();
});

module.controller('RoughMappingController', function ($http, $interval) {
    var self = this;
    self.connected = false;
    self.scanning = false;
    self.status = "Not connected";
    self.mapData = [[]];
    self.refreshInterval = "500";
    DATASOURCE_HOST = "http://127.0.0.1:5000";

    self.refresh = function () {
        self.scanning = false;
        self.connected = false;
        self.status = "Refreshing...";
        self.mapData = [];
        $http({
            url: DATASOURCE_HOST + "/status",
        }).then(function (response) {
            d = response.data;
            self.connected = d.connected;
            self.scanning = d.scanning;
            self.status = d.status;
        }, function (error) {
            self.connected = false;
            self.scanning = false;
            self.status = "(refresh failed)"
        });

        self.replot();
    };

    self.connect = function () {
        if (!self.connected) {
            self.scanning = false;
            self.connected = false;
            self.status = "Connecting...";
            self.mapData = [];
            $http({
                url: DATASOURCE_HOST + "/connect",
            }).then(function (response) {
                d = response.data;
                self.connected = d.connected;
                self.scanning = d.scanning;
                self.status = d.status;
            }, function (error) {
                self.connected = false;
                self.scanning = false;
                self.status = "(connect failed)";
            });
        }
    };

    self.disconnect = function () {
        if (self.connected) {
            self.scanning = false;
            self.connected = false;
            self.status = "Disconnecting...";
            self.mapData = [];
            $http({
                url: DATASOURCE_HOST + "/disconnect",
            }).then(function (response) {
                d = response.data;
                self.connected = d.connected;
                self.scanning = d.scanning;
                self.status = d.status;
            }, function (error) {
                self.connected = false;
                self.scanning = false;
                self.status = "(disconnect failed)";
            });
        }
    };

    self.scan = function () {
        if (self.connected && !self.scanning) {
            self.scanning = true;
            self.status = "Sending scan command...";
            $http({
                url: DATASOURCE_HOST + "/scan",
            }).then(function (response) {
                d = response.data;
                self.connected = d.connected;
                self.scanning = d.scanning;
                self.status = d.status;
                self.mapData = []
                self.scanUpdatePms = $interval(self.scanUpdateFn, parseInt(self.refreshInterval));
            }, function (error) {
                self.scanning = false;
                self.status = "(scan failed)";
            });
        }
    };

    self.scanUpdateFn = function () {
        if (self.connected && self.scanning) {
            $http({
                url: DATASOURCE_HOST + "/data",
            }).then(function (response) {
                var d = response.data;
                self.mapData = self.mapData.concat(d.mapData);
                self.replot();
            }, function (error) {
                $interval.cancel(self.scanUpdatePms);
                self.scanning = false;
                self.status = "Scan finished";
                self.replot();
            });
        }
    };

    function transpose(original) {
        var copy = [];
        for (var i = 0; i < original.length; ++i) {
            for (var j = 0; j < original[i].length; ++j) {
                // skip undefined values to preserve sparse array
                if (original[i][j] === undefined) continue;
                // create row if it doesn't exist yet
                if (copy[j] === undefined) copy[j] = [];
                // swap the x and y coords for the copy
                copy[j][i] = original[i][j];
            }
        }
        return copy;
    }

    self.xy = transpose(self.mapData);

    self.data = [{
        r: self.xy[1],
        theta: self.xy[0],
        mode: 'markers',
        name: 'Figure',
        marker: {
            color: 'rgb(0, 0, 255)',
            width: 1
        },
        type: 'scatterpolar'
    }, {
        r: [],
        theta: [],
        mode: 'lines',
        name: 'Cursor',
        line: {
            color: 'rgb(0, 255, 0)',
            width: 3
        },
        type: 'scatterpolar'
    }];


    PLOT_LAYOUT_RADIUS_MAX = 1000;    
    self.layout = {
        title: 'Rough Map',
        orientation: -90,
        polar: {
            angularaxis: {
                range: [0, 360]
            },
            radialaxis: {
                range: [0, PLOT_LAYOUT_RADIUS_MAX]
            }
        },
        autosize: true,
    };

    self.replot = function () {
        self.xy = transpose(self.mapData);
        
        self.data[0].r = self.xy[1];
        self.data[0].theta = self.xy[0];
        
        if (self.scanning && self.mapData.length > 0) {
            var cursorAngle = self.mapData[self.mapData.length-1][0];
            self.data[1].r = [0, PLOT_LAYOUT_RADIUS_MAX];
            self.data[1].theta = [cursorAngle, cursorAngle];
        } else {
            self.data[1].r = [];
            self.data[1].theta = [];
        }
        
        Plotly.react('roughMapDiv', self.data, self.layout, {responsive: true});
    };

    Plotly.plot('roughMapDiv', self.data, self.layout, {responsive: true});
});

/*
window.onresize = function() {
    Plotly.Plots.resize('myDiv');
};
*/