module = angular.module('rtWebUI', []);

// const RASPI_ADDRESS = "http://192.168.137.49:5000";
const RASPI_ADDRESS = "http://10.42.0.229:5000";
const MAP_LEFT = -1500;
const MAP_RIGHT = 1500;
const MAP_TOP = -1500;
const MAP_BOTTOM = 1500;
const TRI_ILEN = 80 / Math.sqrt(3);
const SQR_ILEN = 70 / Math.sqrt(2);
const ROBOT_COLLISION_RADIUS = 300;

module.controller('PlanController', function ($scope, $http) {
    var self = this;
    self.connected = false;
    self.tasksList = [" -- "];
    self.mapData = null;
    self.mapObjects = null;
    self.laserMsg = "-";
    self.cursorPosXY = "-";
    self.cursorPosRT = "-";
    self.robot = null;
    self.coverCircles = {circles: [], radius: 0};
    
    var win = $(window);
    var canvas = $("#main-map-canvas");
    var ctx = canvas[0].getContext('2d');
    function resize() {
        canvas[0].width = win.height() * 0.8 * 16 / 9;
        canvas[0].height = win.height() * 0.8;
        self.updateMapCanvas();
    }
    
    self.socket = io(RASPI_ADDRESS, {
        autoConnect: false
    });
    
    self.socket.on("connect", () => {
        $scope.$apply(() => {
            self.connected = true;
            /* self.tasksList.unshift("Connected"); */
        });
    });
    
    self.socket.on("disconnect", () => {
        $scope.$apply(() => {
            self.connected = false;
            /* self.tasksList.unshift("Disconnected"); */
        });
    });
    
    self.connect = () => { 
        self.socket.connect(); 
    }
    
    self.disconnect = () => { 
        self.connected = false;
        //self.currentTask = "Disconnected";
        self.socket.disconnect(); 
    }
    
    self.scan = () => {
        self.tasksList.unshift("Initiate Scan");
        self.socket.emit("scan");
    }
    
    self.socket.on("update-task", (task) => {
        $scope.$apply(() => {
            self.tasksList.unshift(task);
        });
    })
    
    self.socket.on("update-map", (data) => {
        $scope.$apply(() => {
            self.mapData = data;
            self.updateMapCanvas();
        });
    });
    
    self.socket.on("update-objects", (objects) => {
       $scope.$apply(() => {
            self.mapObjects = objects;
            self.updateMapCanvas();
       });
    });
    
    self.socket.on("update-robot", (robotData) => {
       $scope.$apply(() => {
           self.robot = robotData;
           self.updateMapCanvas();
       });
    });
    
    self.socket.on("update-rough", (roughData) => {
        $scope.$apply(() => {
            self.updateRoughPlot(roughData);
        });
    });
    
    self.socket.on("update-laser", (laserData) => {
        $scope.$apply(() => {
            self.updateLaserPlot(laserData); 
            self.laserMsg = laserData.message;
        });
    });
    
    self.socket.on("update-coverage", (coverage) => {
        $scope.$apply(() => {
            self.coverCircles = coverage;
            self.updateMapCanvas();
        });
    });
    
    self.socket.on("update-hull", (pts) => {
        $scope.$apply(() => {
            self.hullPts = pts;
            self.updateMapCanvas();
        });
    });
    
    self.pointToCanvas = (pt) => {
        ch = canvas.height();
        cw = canvas.width();
        w = MAP_RIGHT - MAP_LEFT;
        h = MAP_BOTTOM - MAP_TOP;
        xn = pt[0] * ch / h + cw/2;
        yn = (MAP_BOTTOM - pt[1]) * ch / h;
        return {
            x: xn, y: yn
        };
    }
    
    self.canvasToPoint = (pt) => {
        ch = canvas.height();
        cw = canvas.width();
        w = MAP_RIGHT - MAP_LEFT;
        h = MAP_BOTTOM - MAP_TOP;
        
        x = (pt.x - cw/2) * h / ch;
        y = MAP_BOTTOM - pt.y * h / ch;
        return {
            x: x,
            y: y
        };
    }
    
    self.lengthToCanvas = (l) => {
        ch = canvas.height();
        h = MAP_BOTTOM - MAP_TOP;
        return l * ch / h;
    }
    
    self.drawGrid = () => {
        org = self.pointToCanvas([0, 0]);
        delta = self.lengthToCanvas(500);
        
        // draw coordinate axes
        ctx.save();
        ctx.setLineDash([5, 5]);
        ctx.lineWidth = 1;
        ctx.moveTo(org.x, 0);
        ctx.lineTo(org.x, canvas.height());
        ctx.moveTo(0, org.y);
        ctx.lineTo(canvas.width(), org.y);
        ctx.stroke();
        ctx.restore();
        
        // draw aux grid
        ctx.save();
        ctx.strokeStyle = "rgb(200, 200, 200)";
        ctx.setLineDash([8, 2, 2, 2]);
        ctx.lineWidth = 1;
        var i;
        for (i = delta; i <= MAP_RIGHT; i += delta) {
            ctx.beginPath();
            ctx.moveTo(org.x + i, 0);
            ctx.lineTo(org.x + i, canvas.height());
            ctx.stroke();
            
            ctx.beginPath();
            ctx.moveTo(org.x - i, 0);
            ctx.lineTo(org.x - i, canvas.height());
            ctx.stroke();
        }
        for (i = delta; i <= MAP_BOTTOM; i += delta) {
            ctx.beginPath();
            ctx.moveTo(0, org.y + i);
            ctx.lineTo(canvas.width(), org.y + i);
            ctx.stroke();
            
            ctx.beginPath();
            ctx.moveTo(0, org.y - i);
            ctx.lineTo(canvas.width(), org.y - i);
            ctx.stroke();
        }
        ctx.restore();
    }
    
    self.drawObject = (obj) => {
        ctx.save();
        ctx.strokeStyle = "rgb(0, 0, 0)";
        ctx.lineWidth = 2;
        ctx.beginPath();
        
        var anchor = self.pointToCanvas(obj.position);
        ctx.font = "16px Monospace";
        ctx.fillText("(" + Math.round(obj.position[0]) + ", " + Math.round(obj.position[1]) + ")", anchor.x, anchor.y + 30);
        
        if (obj.type == "cylinder") 
        {
            var pt = self.pointToCanvas(obj.position);
            var rd = self.lengthToCanvas(obj.radius);
            ctx.arc(pt.x, pt.y, rd, 0, 2*Math.PI);
            ctx.stroke();
        } 
        else if (obj.type == "triangle") 
        {
            var x = obj.position[0];
            var y = obj.position[1];
            var t = obj.angle;

            var xa = x + TRI_ILEN * Math.cos(t * Math.PI / 180);
            var ya = y + TRI_ILEN * Math.sin(t * Math.PI / 180);
            var pt = self.pointToCanvas([xa, ya]);
            ctx.moveTo(pt.x, pt.y);

            var xb = x + TRI_ILEN * Math.cos((t + 120) * Math.PI / 180);
            var yb = y + TRI_ILEN * Math.sin((t + 120) * Math.PI / 180);
            pt = self.pointToCanvas([xb, yb]);
            ctx.lineTo(pt.x, pt.y);

            var xc = x + TRI_ILEN * Math.cos((t + 240) * Math.PI / 180);
            var yc = y + TRI_ILEN * Math.sin((t + 240) * Math.PI / 180);
            pt = self.pointToCanvas([xc, yc]);
            ctx.lineTo(pt.x, pt.y);

            pt = self.pointToCanvas([xa, ya]);
            ctx.lineTo(pt.x, pt.y);

            ctx.stroke();
        }
        else if (obj.type == "square") 
        {
            var x = obj.position[0];
            var y = obj.position[1];
            var t = obj.angle;

            var xa = x + SQR_ILEN * Math.cos(t * Math.PI / 180);
            var ya = y + SQR_ILEN * Math.sin(t * Math.PI / 180);
            var pt = self.pointToCanvas([xa, ya]);
            ctx.moveTo(pt.x, pt.y);

            var xb = x + SQR_ILEN * Math.cos((t + 90) * Math.PI / 180);
            var yb = y + SQR_ILEN * Math.sin((t + 90) * Math.PI / 180);
            pt = self.pointToCanvas([xb, yb]);
            ctx.lineTo(pt.x, pt.y);

            var xc = x + SQR_ILEN * Math.cos((t + 180) * Math.PI / 180);
            var yc = y + SQR_ILEN * Math.sin((t + 180) * Math.PI / 180);
            pt = self.pointToCanvas([xc, yc]);
            ctx.lineTo(pt.x, pt.y);

            var xd = x + SQR_ILEN * Math.cos((t + 270) * Math.PI / 180);
            var yd = y + SQR_ILEN * Math.sin((t + 270) * Math.PI / 180);
            pt = self.pointToCanvas([xd, yd]);
            ctx.lineTo(pt.x, pt.y);

            pt = self.pointToCanvas([xa, ya]);
            ctx.lineTo(pt.x, pt.y);

            ctx.stroke();
        }
        ctx.restore();
    };
    
    self.drawRobot = () => {
        ctx.save();
        
        ctx.fillStyle = "rgba(255, 0, 0, 0.5)";
        ctx.lineWidth = 2;
        
        var pt = self.pointToCanvas(self.robot.location);
        var theta = -self.robot.angle;
        
        ctx.beginPath();
        ctx.arc(pt.x, pt.y, self.lengthToCanvas(120), (theta + 10)*Math.PI/180, (theta - 10)*Math.PI/180);
        
        ctx.arc(pt.x, pt.y, self.lengthToCanvas(150), (theta - 1)*Math.PI/180, (theta + 1)*Math.PI/180);
        
        ctx.closePath();
        ctx.fill();
        
        var move_thresh_radius = self.lengthToCanvas(ROBOT_COLLISION_RADIUS);
        ctx.beginPath();
        ctx.strokeStyle = "rgb(0, 255, 0)";
        ctx.arc(pt.x, pt.y, move_thresh_radius, 0, 2*Math.PI);
        ctx.stroke();
        
        ctx.restore();
    }
    
    self.drawCoverage = () => {
        ctx.save();
        
        ctx.fillStyle = "rgba(0, 255, 0, 0.2)";
        
        var rad = self.lengthToCanvas(self.coverCircles["radius"]);
        
        self.coverCircles.circles.forEach((c) => {
            ctx.beginPath();
            pt = self.pointToCanvas(c);
            ctx.arc(pt.x, pt.y, rad, 0, 2*Math.PI);
            ctx.fill();
        });
        
        ctx.restore();
    }
    
    self.drawDataPoints = () => {
        ctx.save();
        ctx.fillStyle = "blue";
        self.mapData.forEach((pt) => {
            cpt = self.pointToCanvas(pt);
            ctx.beginPath();
            ctx.arc(cpt.x, cpt.y, 1, 0, 2*Math.PI);
            ctx.fill();
        });
        ctx.restore();
    }
    
    self.drawMapObjects = () => {
        self.mapObjects.forEach((obj) => self.drawObject(obj));
    }
    
    self.drawHull = () => {
        ctx.save();
        
        ctx.strokeStyle = "rgb(0,0,0)";
        ctx.lineWidth = 3;
        
        ctx.beginPath();
        pt = self.pointToCanvas(self.hullPts[0]);
        ctx.moveTo(pt.x, pt.y);
        
        var i;
        for (i = 1; i < self.hullPts.length; i++) {
            pt = self.pointToCanvas(self.hullPts[i]);
            ctx.lineTo(pt.x, pt.y);
        }
        
        pt = self.pointToCanvas(self.hullPts[0]);
        ctx.lineTo(pt.x, pt.y);
        
        ctx.stroke();
        ctx.restore();
    }
    
    self.updateMapCanvas = () => {
        ctx.clearRect(0, 0, canvas.width(), canvas.height());
        
        self.drawGrid();
        
        if (self.mapData != null) {
            self.drawDataPoints();
        }
        
        if (self.hullPts != null) {
            self.drawHull();
        }
        
        if (self.mapObjects != null) {
            self.drawMapObjects();
        }
        
        if (self.robot != null) {
            self.drawRobot();
        }
        
        if (self.coverCircles != null) {
            self.drawCoverage();
        }
    }
    
    self.updateRoughPlot = (roughData) => {
        var traces = [];
        
        roughData.forEach((cluster) => {
            traces.push({
                x: cluster.xs,
                y: cluster.ys,
                type: 'scatter',
                mode: 'markers'
            });
        });

        Plotly.newPlot('rough-plot', traces);
    }
    
    self.updateLaserPlot = (laserData) => {
        var traces = [];
        
        laserData.dataGroups.forEach((xy) => {
            traces.push({
                x: xy.xs,
                y: xy.ys,
                type: 'scatter',
                mode: 'markers'
            });
        })

        Plotly.newPlot('laser-plot', traces);
    }
    
    self.cart2pol = (pt) => {
        return {
            r: Math.hypot(pt.x, pt.y),
            t: Math.atan2(pt.y, pt.x)
        };
    }
    
    canvas.mousemove((e) => {
        $scope.$apply(() => {
            x = e.pageX - canvas.offset().left;
            y = e.pageY - canvas.offset().top;
            pt = self.canvasToPoint({x: x, y: y});
            self.cursorPosXY = "(" + Math.round(pt.x) + ", " + Math.round(pt.y) + ")";
            
            pt2 = self.cart2pol(pt);
            self.cursorPosRT = "(" + Math.round(pt2.r) + ", " + Math.round(pt2.t*180/Math.PI) + ")";
        });
    });
    
    win.resize(resize);
    win.resize();
    
    self.updateMapCanvas();
});