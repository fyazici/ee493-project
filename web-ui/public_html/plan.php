<?php
require("authenticate.php");
require("functions/session.php");
?>

<!doctype html>
<html lang="en" ng-app="rtWebUI">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Robothings Plan Extraction</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="dist/bootstrap.min.css">

    <!-- Custom styles for this template -->
    <link href="assets/common.css" rel="stylesheet">
    <link href="assets/plan.css" rel="stylesheet">
    
    <link rel="icon" type="image/png" href="assets/img/favicon.png">
</head>

<body>
    <nav class="navbar navbar-dark fixed-top bg-dark flex-nowrap p-0 shadow">
        <a class="navbar-brand col-md-1 col-sm-4" href="#">Robothings</a>
        <ul class="navbar-nav">
            <li class="nav-item text-nowrap" style="color: white">
                Welcome,
                <?php get_login_user(); ?>!
            </li>
        </ul>
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="logout.php">Sign out</a>
            </li>
        </ul>
    </nav>

    <div class="container-fluid">
        <div class="row">
            <nav class="col-md-1 bg-light sidebar">
                <div class="py-2 sticky-top flex-grow-1">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <!-- TODO: other views list here -->
                            <a class="nav-link" href="index.php">
                                <span data-feather="home"></span>
                                Device Status
                            </a>
                            <a class="nav-link active" href="#">
                                <span data-feather="map"></span>
                                Plan Extraction <span class="sr-only">(current)</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>

            <main role="main" class="col-md-11 ml-sm-auto col-lg-11 px-4" ng-controller="PlanController as plan">
                <!-- Plan Extraction page header -->
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    <h1 class="h2">Plan Extraction</h1>
                </div>
                
                <!-- Device Status page content -->   
                
                <div class="btn-group" style="margin: 10px">
                    <button class="btn btn-sm btn-outline-secondary" ng-click="plan.connect()" ng-disabled="plan.connected">
                        <span data-feather="play"></span>
                        Connect
                    </button>
                    <button class="btn btn-sm btn-outline-secondary" ng-disabled="!plan.connected" ng-click="plan.scan()">
                        <span data-feather="settings"></span>
                        Scan
                    </button>
                    <button class="btn btn-sm btn-outline-secondary" ng-disabled="!plan.connected" ng-click="plan.disconnect()">
                        <span data-feather="x"></span>
                        Disconnect
                    </button>
                </div>
                
                <!-- Map Information -->
                <div style="margin: auto">
                    <div class="row">
                        <div class="col">
                            <div class="plan-canvas-container">
                                <h4>Map View</h4>
                                <div>
                                <canvas id="main-map-canvas">Map Canvas</canvas>
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="plan-panel-container">
                                <h4>Scan Status</h4>
                                <div class="table-responsive">
                                <table class="table table-bordered table-striped">
                                    <col width="120">
                                    <col width="*">
                                    <tr>
                                        <td># of objects:</td>
                                        <td><span ng-bind="plan.mapObjects.length"></span></td>
                                    </tr>
                                    <tr>
                                        <td>Robot Location:</td>
                                        <td><span ng-bind="plan.robot.location"></span></td>
                                    </tr>
                                    <tr>
                                        <td>XY Location:</td>
                                        <td><span ng-bind="plan.cursorPosXY"></span></td>
                                    </tr>
                                    <tr>
                                        <td>R&Theta; Location:</td>
                                        <td><span ng-bind="plan.cursorPosRT"></span></td>
                                    </tr>
                                </table>
                                    
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <td>Current Task:</td>
                                    </thead>
                                    <tr>
                                        <td><span id="current-task-box" ng-bind="plan.tasksList[0]"></span></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <ul id="all-tasks-list">
                                                <li ng-repeat="task in plan.tasksList track by $index">{{task}}</li>
                                            </ul>
                                        </td>
                                    </tr>
                                </table>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="col-6">
                            <div class="plan-auxplot-container">
                                <h4>Rough Scan Plot</h4><br/>
                                <div id="rough-plot"></div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="plan-auxplot-container">
                                <h4>Laser Scan Plot</h4><br/>
                                <div id="laser-plot"></div>
                                Result: <span ng-bind="plan.laserMsg"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="dist/jquery-3.3.1.slim.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="../../assets/js/vendor/jquery-slim.min.js"><\/script>')
    </script>
    <script src="dist/popper.min.js"></script>
    <script src="dist/angular.min.js"></script>
    <script src="dist/ui-bootstrap-tpls-2.5.0.min.js"></script>
    <script src="dist/socket.io.js"></script>
    <script src="dist/plotly-latest.min.js"></script>

    <!-- Icons -->
    <script src="dist/feather.min.js"></script>
    <script>
        feather.replace()
    </script>
    
    <!-- Dashboard Scripts -->
    <script src="js/plan.js"></script>
</body>

</html>
