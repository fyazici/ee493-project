<?php

$users_login = [
    'fatih' => '$2y$10$Om5oCecLkd7aE3QSmb8o3uCRQl1NGGBSTuYI5pQjMgIA3JQwHIJHS',
    'mete' => '$2y$10$NlzJnR5r9noYPqMFkzAVXeU8rY3rO9/P6WuIxmOJYOd3k6VmudCdm',

];

function check_user($username, $password)
{    
    global $users_login;
    if (!empty($users_login[$username])) {
        $user_hash = $users_login[$username];
        return password_verify($username . $password, $user_hash);
    }
    return false;
}

?>
