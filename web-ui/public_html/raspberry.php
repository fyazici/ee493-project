<?php
require('authenticate.php');

error_reporting(0);

function raspi_refresh() {
    $ipfile = "functions/raspberry-ip.txt";

    if (!file_exists($ipfile))
    {
        return json_encode([
            "raspiid" => "Raspberry Pi 3",
            "online" => false,
            "ipaddress" => "x.x.x.x",
            "lastupdate" => "(not connected yet)",
        ], true);
    }
    
    $device = json_decode(file_get_contents($ipfile), true);
    
    if (empty($device['from'])) {
        return json_encode([
            "raspiid" => "Raspberry Pi 3",
            "online" => false,
            "ipaddress" => "x.x.x.x",
            "lastupdate" => "(not connected yet)",
        ], true);
    }
    
    $addr = $device['from'];
    $fp = fsockopen($addr, '22', $errno, $errstr, 3);
    
    if ($fp) {
        return json_encode([
            "raspiid" => "Raspberry Pi 3",
            "online" => true,
            "ipaddress" => $addr,
            "lastupdate" => date("r", $device['when']),
        ], true);
    } else {
        return json_encode([
            "raspiid" => "Raspberry Pi 3",
            "online" => false,
            "ipaddress" => $addr,
            "lastupdate" => date("r", $device['when']),
        ], true);
    }
}

$params_json = file_get_contents("php://input");
$params = json_decode($params_json, true);

if (!empty($params['action'])) {
    /* handle requested action */
    if ($params['action'] == 'refresh') {
        echo raspi_refresh();
    }
}

?>
