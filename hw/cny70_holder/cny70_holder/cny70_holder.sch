EESchema Schematic File Version 4
LIBS:cny70_holder-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Sensor_Proximity:CNY70 U1
U 1 1 5BF895F7
P 6600 4350
F 0 "U1" H 6600 4667 50  0000 C CNN
F 1 "CNY70" H 6600 4576 50  0000 C CNN
F 2 "OptoDevice:Vishay_CNY70" H 6600 4150 50  0001 C CNN
F 3 "https://www.vishay.com/docs/83751/cny70.pdf" H 6600 4450 50  0001 C CNN
	1    6600 4350
	1    0    0    -1  
$EndComp
$Comp
L Device:R R1
U 1 1 5BF896B3
P 5950 4250
F 0 "R1" V 5743 4250 50  0000 C CNN
F 1 "150" V 5834 4250 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 4250 50  0001 C CNN
F 3 "~" H 5950 4250 50  0001 C CNN
	1    5950 4250
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5BF8976B
P 7000 4550
F 0 "#PWR0102" H 7000 4300 50  0001 C CNN
F 1 "GND" H 7005 4377 50  0000 C CNN
F 2 "" H 7000 4550 50  0001 C CNN
F 3 "" H 7000 4550 50  0001 C CNN
	1    7000 4550
	1    0    0    -1  
$EndComp
Text Label 4700 3100 0    50   ~ 0
SENS1
Text Label 7100 4250 0    50   ~ 0
SENS2
Text Label 4700 3000 0    50   ~ 0
VCC
Text Label 5700 4250 2    50   ~ 0
VCC
Wire Wire Line
	5700 4250 5800 4250
Wire Wire Line
	6100 4250 6300 4250
Wire Wire Line
	6300 4450 6200 4450
Wire Wire Line
	6200 4450 6200 4550
Wire Wire Line
	7000 4550 7000 4450
Wire Wire Line
	7000 4450 6900 4450
Wire Wire Line
	7100 4250 6900 4250
Wire Wire Line
	4400 3100 4700 3100
$Comp
L power:GND #PWR0101
U 1 1 5BF89743
P 6200 4550
F 0 "#PWR0101" H 6200 4300 50  0001 C CNN
F 1 "GND" H 6205 4377 50  0000 C CNN
F 2 "" H 6200 4550 50  0001 C CNN
F 3 "" H 6200 4550 50  0001 C CNN
	1    6200 4550
	1    0    0    -1  
$EndComp
Wire Wire Line
	4400 3000 4700 3000
$Comp
L Connector_Generic:Conn_01x04 J1
U 1 1 5BFAB063
P 4200 3100
F 0 "J1" H 4120 3417 50  0000 C CNN
F 1 "Conn_01x04" H 4120 3326 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x04_P2.54mm_Horizontal" H 4200 3100 50  0001 C CNN
F 3 "~" H 4200 3100 50  0001 C CNN
	1    4200 3100
	-1   0    0    -1  
$EndComp
Text Label 4700 3200 0    50   ~ 0
SENS2
Wire Wire Line
	4700 3200 4400 3200
$Comp
L power:GND #PWR0103
U 1 1 5BFAB2CD
P 4500 3400
F 0 "#PWR0103" H 4500 3150 50  0001 C CNN
F 1 "GND" H 4505 3227 50  0000 C CNN
F 2 "" H 4500 3400 50  0001 C CNN
F 3 "" H 4500 3400 50  0001 C CNN
	1    4500 3400
	1    0    0    -1  
$EndComp
Wire Wire Line
	4500 3400 4500 3300
Wire Wire Line
	4500 3300 4400 3300
$Comp
L Sensor_Proximity:CNY70 U2
U 1 1 5BFAB992
P 6600 3100
F 0 "U2" H 6600 3417 50  0000 C CNN
F 1 "CNY70" H 6600 3326 50  0000 C CNN
F 2 "OptoDevice:Vishay_CNY70" H 6600 2900 50  0001 C CNN
F 3 "https://www.vishay.com/docs/83751/cny70.pdf" H 6600 3200 50  0001 C CNN
	1    6600 3100
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5BFAB998
P 5950 3000
F 0 "R2" V 5743 3000 50  0000 C CNN
F 1 "150" V 5834 3000 50  0000 C CNN
F 2 "Resistor_THT:R_Axial_DIN0207_L6.3mm_D2.5mm_P10.16mm_Horizontal" V 5880 3000 50  0001 C CNN
F 3 "~" H 5950 3000 50  0001 C CNN
	1    5950 3000
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5BFAB99E
P 7000 3300
F 0 "#PWR0104" H 7000 3050 50  0001 C CNN
F 1 "GND" H 7005 3127 50  0000 C CNN
F 2 "" H 7000 3300 50  0001 C CNN
F 3 "" H 7000 3300 50  0001 C CNN
	1    7000 3300
	1    0    0    -1  
$EndComp
Text Label 7100 3000 0    50   ~ 0
SENS1
Text Label 5700 3000 2    50   ~ 0
VCC
Wire Wire Line
	5700 3000 5800 3000
Wire Wire Line
	6100 3000 6300 3000
Wire Wire Line
	6300 3200 6200 3200
Wire Wire Line
	6200 3200 6200 3300
Wire Wire Line
	7000 3300 7000 3200
Wire Wire Line
	7000 3200 6900 3200
Wire Wire Line
	7100 3000 6900 3000
$Comp
L power:GND #PWR0105
U 1 1 5BFAB9AD
P 6200 3300
F 0 "#PWR0105" H 6200 3050 50  0001 C CNN
F 1 "GND" H 6205 3127 50  0000 C CNN
F 2 "" H 6200 3300 50  0001 C CNN
F 3 "" H 6200 3300 50  0001 C CNN
	1    6200 3300
	1    0    0    -1  
$EndComp
$EndSCHEMATC
