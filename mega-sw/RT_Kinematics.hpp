#ifndef RT_KINEMATICS_HPP_
#define RT_KINEMATICS_HPP_

#include <math.h>

namespace kmx
{

template<typename T>
struct Integrator
{ 
  void write(T x) { x_ = x; }
  
  void update(T dx) { x_ += dx; }
  
  T read() { return x_; }

  operator T() { return x_; }

private:
  T x_ = 0;
};

template<typename T>
struct VecRL
{
  T right = 0;
  T left = 0;
};

template<typename T>
struct VecST
{
  T s = 0;
  T theta = 0;

  VecST(T s, T theta) : s{s}, theta{theta} {}
};

template<typename T>
VecST<T> vec_rl_to_st(T r_wheel, T d_wheel, VecRL<T> rl)
{
  return {
    r_wheel * (rl.right + rl.left) / 2,
    r_wheel * (rl.right - rl.left) / d_wheel
  };
}

template<typename T>
VecRL<T> vec_rl_to_st(T r_wheel, T d_wheel, VecST<T> st)
{
  float a = st.s / r_wheel;
  float b = st.theta * d_wheel / r_wheel / 2;
  return { a + b, a - b };
}

template<typename T>
struct CartesianOdometry
{
  void write_x(T x) { x_.write(x); }
  void write_y(T y) { y_.write(y); }
  void write_theta(T theta) { theta_.write(theta); }
  
  T read_x() { return x_; }
  T read_y() { return y_; }
  T read_theta() { return theta_; }

  void update(VecST<T>&& delta)
  {
    x_.update(cos(theta_) * delta.s);
    y_.update(sin(theta_) * delta.s);
    theta_.update(delta.theta);
    if (theta_ > M_PI) theta_.write(theta_ - 2*M_PI);
    else if (theta_ < -M_PI) theta_.write(theta_ + 2*M_PI);
  }
  
  T calculate_error_s(T target_x, T target_y)
  {
    return hypot(target_x - x_, target_y - y_);
  }

  T calculate_error_theta(T target_theta)
  {
    return target_theta - theta_;
  }
  
private:
  Integrator<T> x_, y_, theta_;
};

}


#endif
