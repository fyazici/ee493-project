#ifndef RT_UTIL_HPP_
#define RT_UTIL_HPP_

template<typename T>
struct average_helper {
  T sum;
  size_t count;
  void reset() { sum = 0; count = 0; }
  void update(T value) { sum += value; count++; }
  T average() { return (sum / count); }
};

struct TimepointScheduler {
  unsigned long schedule_start;
  unsigned long schedule_next;

  void start(unsigned long t, unsigned long delta) { schedule_start = t; schedule_next = t + delta; }

  bool is_due(unsigned long t) { return (schedule_next < t); }
  unsigned long elapsed(unsigned long t) { return (t - schedule_start); }
};

struct PIDController {
  double kp, ki, kd;
  double output_max, output_min;
  double prev_error = 0.0;
  double integ_error = 0.0;

  PIDController(double _kp, double _ki, double _kd, double _omax, double _omin)
    : kp{_kp}, ki{_ki}, kd{_kd}, output_max{_omax}, output_min{_omin} {}

  void reset()
  {
    prev_error = 0.0;
    integ_error = 0.0;
  }
  
  double run(double input, double target, double dt)
  {
    double error = target - input;
    integ_error = (error + prev_error) / 2 * dt;
    double derror = (error - prev_error) / dt;
    prev_error = error;
    double pid = kp * error + ki * integ_error + kd * derror;
    return constrain(pid, output_min, output_max);
  }
};

#endif
