#ifndef RT_COMMS_HPP_
#define RT_COMMS_HPP_

namespace detail {

template<typename T>
void respond_impl(T t) {
  Serial.print(t);
}

template<typename T, typename ... Ts>
void respond_impl(T t, Ts ... ts) {
  respond_impl(t);
  respond_impl(ts...);
}
}

template<typename ... Ts>
void respond(Ts ... ts) {
  detail::respond_impl(ts...);
  Serial.println();
}

template<typename ... Ts>
void respond_bare(Ts ... ts) {
    detail::respond_impl(ts...);
}

#endif
