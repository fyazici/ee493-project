#include <L3G.h>
#include <math.h>
#include <Wire.h>
#include <Encoder.h>
#include "RT_Comms.hpp"
#include "RT_Util.hpp"
#include "RT_Kinematics.hpp"

//#define RT_DEBUG
#include "RT_Debug.hpp"

void (*software_reset)(void) = 0;

/*
 * Constants
 */

const float ERROR_THRESHOLD = 0.5;      // deg
const int MOTOR_ERROR_THRESHOLD = 10;    // mm
const float MVMT_VEL_MAX = 20;          // mm/s
const int ENCODER_STEPS_PER_REV = 823*4; // steps/rev
const int ENCODER_WHEEL_DIAM = 65;   // mm
const int ENCODER_AXLE_WIDTH = 175;     // mm
const int MOTOR_DEFAULT_REACH_TOL = 1000;  // ms

/*
 * Encoder Task
 */
 
struct TEncoderTask {

  Encoder ctr_right {19, 18};
  Encoder ctr_left {3, 2};

  int32_t prev_right = 0;
  int32_t prev_left = 0;

  void setup();

  float get_dist_left_mm() {
    return ctr_left.read() * M_PI * ENCODER_WHEEL_DIAM / ENCODER_STEPS_PER_REV;
  }

  float get_dist_right_mm() {
    return ctr_right.read() * M_PI * ENCODER_WHEEL_DIAM / ENCODER_STEPS_PER_REV;
  }

  float get_delta_tran_mm() {
    return ((ctr_right.read() - prev_right) + (ctr_left.read() - prev_left)) * M_PI * ENCODER_WHEEL_DIAM / ENCODER_STEPS_PER_REV / 2;
  }

  float get_delta_rot_rad() {
    return ((ctr_right.read() - prev_right) - (ctr_left.read() - prev_left)) * M_PI * ENCODER_WHEEL_DIAM / ENCODER_STEPS_PER_REV / ENCODER_AXLE_WIDTH;
  }

  float get_rot_target_diff(float deg)
  {
    // 2a * M_PI * WHLDIA / STPerRev / AxleWidth = rad
    return deg / 360.0f * M_PI * ENCODER_AXLE_WIDTH;
  }

  void update_prev()
  {
    prev_right = ctr_right.read();
    prev_left = ctr_left.read();
  }

  
  
  void begin() {
    // empty
  }
  
  bool update(unsigned long t) {
    // empty
    //debug_logln(" ctr_w: ", ctr_w, " ctr_b: ", ctr_b);
    return false;
  }

  void end() {
    // empty
  }
  
} encoder_task;

void TEncoderTask::setup() {
  ctr_right.write(0);
  ctr_left.write(0);
}

/*
 * Motor drive task
 */

/* TODO: implement this task */
struct TMotorTask {

  const int MRP = 7; // black motor
  const int MRN = 6;
  const int MLP = 4; //white motor 
  const int MLN = 5;
  
  const int MRV = 8;
  const int MLV = 9;
  const int MAX_THRUST = 75;
  const int BASE_THRUST = 150;

  double rot_cmd, tran_cmd;

  int reach_counter = 0;
  int reach_counter_2 = 0;

  TimepointScheduler pidUpdate;

  double leftReading=0, leftTarget=0, leftTarget2=0, leftCommand=0;
  PIDController pidLeft {10, 40.0, 0.35, 105.0, -110.0};

  double rightReading=0, rightTarget=0, rightTarget2=0, rightCommand=0;
  PIDController pidRight{10, 38.0, 0.35, 100.0, -96.0};

  void setup() {
    pinMode(MLP, OUTPUT);
    pinMode(MLN, OUTPUT);
    pinMode(MRP, OUTPUT);
    pinMode(MRN, OUTPUT);
    pinMode(MRV, OUTPUT);
    pinMode(MLV, OUTPUT);
    analogWrite(MRV, 0);
    analogWrite(MLV, 0);
    pidUpdate.start(micros(), 0);
  }

  void begin() {
    // empty
    leftReading=0; leftTarget=0; leftTarget2=0; leftCommand=0;
    rightReading=0; rightTarget=0; rightTarget2=0; rightCommand=0;
    pidLeft.reset(); pidRight.reset();
  }

  void set_rotational_cmd(float cmd) {
    rot_cmd = cmd;
  }

  void set_translational_cmd(float cmd) {
    tran_cmd = cmd;
  }

  void set_left_target(double target) {
    leftTarget = target;
    if (leftTarget > (leftTarget2 + 10)) leftTarget2 += 10;
    if (leftTarget < (leftTarget2 - 10)) leftTarget2 -= 10;
  }

  void set_right_target(double target) {
    rightTarget = target;
    if (rightTarget > (rightTarget2 + 10)) rightTarget2 += 10;
    if (rightTarget < (rightTarget2 - 10)) rightTarget2 -= 10;
  }

  void set_reach_counter(int cnt = MOTOR_DEFAULT_REACH_TOL)
  {
    reach_counter = cnt;
    reach_counter_2 = cnt * 10;
  }

  int get_reach_status()
  {
    return (reach_counter > 0) && (reach_counter_2 > 0);
  }

  void stall() {
    digitalWrite(MLP, LOW);
    digitalWrite(MLN, LOW);
    digitalWrite(MRP, LOW);
    digitalWrite(MRN, LOW);
    analogWrite(MLV, 0);
    analogWrite(MRV, 0);
  }

  template<typename T1, typename T2>
  T1 clamp(T1 v, T2 mi, T2 ma) {
    if (v > ma) return ma;
    if (v < mi) return mi;
    return v;
  }
  
  bool update(unsigned long t) {
    if (pidUpdate.is_due(t))
    {
      double dt = pidUpdate.elapsed(t) * 1.0e-6;
      pidUpdate.start(t, 1000);
      
      if (reach_counter > 0 && reach_counter_2 > 0)
      {
        reach_counter_2--;
        if ((abs(leftReading - leftTarget) < MOTOR_ERROR_THRESHOLD) &&
            (abs(rightReading - rightTarget) < MOTOR_ERROR_THRESHOLD))
        {
          reach_counter--;
        }
      }
      
      leftReading = encoder_task.get_dist_left_mm();
      leftCommand = pidLeft.run(leftReading, leftTarget2, dt);

      if (abs(leftReading - leftTarget2) < 5)
      {
        leftTarget2 += min(10, max(-10,  leftTarget - leftTarget2));
      }
      
      if (leftCommand > 30) {
        digitalWrite(MLP, HIGH);
        digitalWrite(MLN, LOW);
        analogWrite(MLV, max(10, uint8_t{abs(leftCommand)}));
      } else if (leftCommand < -10) {
        digitalWrite(MLP, LOW);
        digitalWrite(MLN, HIGH);
        analogWrite(MLV, max(10, uint8_t{abs(leftCommand)}));
      } else {
        digitalWrite(MLP, LOW);
        digitalWrite(MLN, LOW);
        analogWrite(MLV, 10);
      }
      

      rightReading = encoder_task.get_dist_right_mm();
      rightCommand = pidRight.run(rightReading, rightTarget2, dt);

      if (abs(rightReading - rightTarget2) < 5)
      {
        rightTarget2 += min(10, max(-10,  rightTarget - rightTarget2));
      }
      
      if (rightCommand > 30) {
        digitalWrite(MRP, HIGH);
        digitalWrite(MRN, LOW);
        analogWrite(MRV, max(10, uint8_t{abs(rightCommand)}));
      } else if (rightCommand < -10) {
        digitalWrite(MRP, LOW);
        digitalWrite(MRN, HIGH);
        analogWrite(MRV, max(10, uint8_t{abs(rightCommand)}));
      } else {
        digitalWrite(MRP, LOW);
        digitalWrite(MRN, LOW);
        analogWrite(MRV, 10);
      }

      //debug_logln(" lr: ", leftReading, " lt1: ", leftTarget, " lt2: ", leftTarget2);
      debug_logln("read left: ", leftReading, " command left: ", leftCommand, " read right: ", rightReading, " command right: ", rightCommand);
    }
  }

  void end() {
    // empty
  }
  
} motor_task;

struct TOdometryTask {

  using value_type = double;

  kmx::CartesianOdometry<value_type> odom;
  TimepointScheduler odom_update_timer;

  value_type target_x, target_y, target_theta;

  void setup()
  {
    odom.write_x(0);
    odom.write_y(0);
    odom.write_theta(0);

    odom_update_timer.start(micros(), 10000);
  }

  void begin()
  {
  }

  bool update(unsigned long t)
  {
    if (odom_update_timer.is_due(t))
    {
      odom_update_timer.start(t, 10000);
      
      auto ds = encoder_task.get_delta_tran_mm();
      auto dtheta = encoder_task.get_delta_rot_rad();
      //debug_logln("ENCO: ", ds, "\t", dtheta);
      encoder_task.update_prev();
      odom.update({ds, dtheta});
      debug_logln("ODOM: ", odom.read_x(), "\t", odom.read_y(), "\t", odom.read_theta() * 180 / M_PI);
    }

    return false;
  }

  void set_target_distance(value_type distance)
  {
    target_x = odom.read_x() + distance * cos(odom.read_theta());
    target_y = odom.read_y() + distance * sin(odom.read_theta());
    target_theta = odom.read_theta();
    debug_logln("target: ", target_x, " ", target_y);
  }

  void set_target_rotation(value_type rotation)
  {
    target_x = odom.read_x();
    target_y = odom.read_y();
    target_theta = odom.read_theta() + rotation;
  }

  value_type get_current_x()
  {
    return odom.read_x();
  }

  value_type get_current_y()
  {
    return odom.read_y();
  }

  value_type get_current_theta()
  {
    return odom.read_theta();
  }

  value_type get_error_tran()
  {
    debug_logln("target: ", odom.read_x(), " ", odom.read_y());
    auto dx = target_x - odom.read_x();
    auto dy = target_y - odom.read_y();
    auto tx = cos(odom.read_theta());
    auto ty = sin(odom.read_theta());
    return dx*tx + dy*ty;
    
    //return (target_x - odom.read_x());
    //return odom.calculate_error_s(target_x, target_y);
  }

  value_type get_error_rot()
  {
    return odom.calculate_error_theta(target_theta);
  }

  void end()
  {
    // empty
  }
  
} odometry_task;

/*
 * OS Setup
 */
void setup() {
  Serial.begin(1000000);
  
  encoder_task.setup();
  motor_task.setup();
  //rotation_task.setup();
  //translation_task.setup();
  odometry_task.setup();

  debug_logln(" READY");
}

/*
 * State Machine
 */
enum class State {
  WaitCmd,
  ParseCmd,
  Rotating,
  Moving
} state = State::WaitCmd;

/*
 * Command line buffer
 */
char cmd_buffer[32] = {0};
int cmd_index = 0;

/*
 * OS Event Loop
 */
void loop() {
  unsigned long t = micros(); // current time

  /*
   * Update odometry task
   */
  odometry_task.update(t);
  motor_task.update(t);
  

  // main state machine
  bool task_finished = false;
  switch (state) {
    case State::WaitCmd:
      if (Serial.available()) {
        int c = Serial.read();
        if (c != '\n') {
          cmd_buffer[cmd_index++] = c;
        } else {
          cmd_buffer[cmd_index] = 0;
          cmd_index = 0;
          state = State::ParseCmd;
        }
      }
      break;
    case State::ParseCmd:
      switch (cmd_buffer[0]) {
        case 'I':
          respond("MEGA");
          respond("ACK");
          state = State::WaitCmd;
          break;
        case 'R': // set target angle
        { // format: 'R 123.45' set angle to 123.45 degrees from starting angle
          float angle_target = atof(cmd_buffer + 2);
          /* OLD VERSION LOGIC *
          rotation_task.begin(angle_target);
          motor_task.begin();
          */
          float cur_left = encoder_task.get_dist_left_mm();
          float cur_right = encoder_task.get_dist_right_mm();
          float dist_right = encoder_task.get_rot_target_diff(angle_target);
          motor_task.set_left_target(cur_left - dist_right);
          motor_task.set_right_target(cur_right + dist_right);
          motor_task.set_reach_counter();
          respond("ACK");
          state = State::Rotating;
          break;
        }
        case 'M': // set target distance, keep current heading
        { // format: 'M 100' move 100mm keeping current heading
          float dist_target = atof(cmd_buffer + 2);
          /* OLD VERSION LOGIC *
          // gyro already tries to keep the angle
          rotation_task.begin();
          translation_task.begin(dist_target);
          motor_task.begin();
          */
          float cur_left = encoder_task.get_dist_left_mm();
          float cur_right = encoder_task.get_dist_right_mm();
          motor_task.set_left_target(cur_left + dist_target);
          motor_task.set_right_target(cur_right + dist_target);
          motor_task.set_reach_counter();
          respond("ACK");
          state = State::Moving;
          break;
        }
        case 'D':
        {
          respond("ACK");
          respond(odometry_task.get_current_x(), ", ", odometry_task.get_current_y(), ", ", 180.0 * odometry_task.get_current_theta() / M_PI);
          respond("END");
          state = State::WaitCmd;
          break;
        }
        case 'S':
        {
          respond("ACK");
          delay(100);
          software_reset();
          break;
        }
        
        default:
          respond("NACK");
          state = State::WaitCmd;
          break;
      }
      break;
    case State::Rotating:
      /* OLD CODE LOGIC *
      task_finished = rotation_task.update(t);
      
      if (task_finished) {
        rotation_task.end();
        motor_task.end();
        state = State::WaitCmd;
      } else {
        motor_task.update(t);
      }
      break;
      */
      if (!motor_task.get_reach_status())
      {
        respond("MOVE_END");
        state = State::WaitCmd;
      }
      break;
    case State::Moving:
      /* OLD CODE LOGIC *
      rotation_task.update(t);
      task_finished = translation_task.update(t);
      motor_task.update(t);
      if (task_finished) {
        rotation_task.end();
        translation_task.end();
        motor_task.end();
        state = State::WaitCmd;
      }
      */
      if (!motor_task.get_reach_status())
      {
        respond("MOVE_END");
        state = State::WaitCmd;
      }
      break;
    default:
      break;
  }
}
