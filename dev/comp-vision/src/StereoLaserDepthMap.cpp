#include <iostream>
#include <opencv2/opencv.hpp>


namespace
{
constexpr auto filename = "/home/fatih/Documents/METU/ee4-1/ee493/stuff/laser/geom/video.mp4";
}

int main([[maybe_unused]] int argc, [[maybe_unused]] char **argv) 
{
    cv::VideoCapture cam1 {filename};

    if ( ! ( cam1.isOpened() ) ) {
        std::cerr << "Opening cameras failed." << std::endl;
        return -1;
    }

    cv::Mat gray1(480 ,640, CV_8UC1);
    while (std::tolower(cv::waitKey(1)) != 'q')
    {
        cv::Mat frame1, frame2;
        if (!cam1.grab())
            break;
        if (!cam1.retrieve(frame1))
            break;

        cv::Mat ll = ( cv::Mat_<uint8_t> ( 1, 3 ) << 0, 0, 200 );
        cv::Mat ul = ( cv::Mat_<uint8_t> ( 1, 3 ) << 0, 0, 255 );
        cv::Mat nonred;
        cv::Mat hsv;

        cv::cvtColor ( frame1, hsv, cv::COLOR_BGR2HSV );
        cv::inRange ( hsv, ll, ul, nonred );
        cv::bitwise_and(frame1, cv::Scalar::all(255), frame2, nonred);
        cv::cvtColor ( frame2, frame2, cv::COLOR_BGR2GRAY);
        
        std::vector<std::vector<cv::Point>> contours;
        cv::findContours(frame2, contours, cv::RetrievalModes::RETR_EXTERNAL, cv::ContourApproximationModes::CHAIN_APPROX_SIMPLE);
        std::clog << contours.size() << "\n";
        if (contours.size())
        {
            auto M = cv::moments(contours[0]);
            auto cx = M.m10 / M.m00;
            auto cy = M.m01 / M.m00;
            std::clog << cx << "," << cy << "\n";
            cv::circle(gray1, cv::Point(cx, cy), 2, cv::Scalar(255), 2, cv::LineTypes::LINE_AA);
        }
        
        cv::imshow("frame2", frame2);
    }
    
    cv::imshow("gray1", gray1);
    cv::waitKey(0);
    
    
    /*
    frame2 = gray1;
    
    auto kernel = cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(11, 11), cv::Point(5, 5));
    cv::dilate(frame2, frame2, kernel, cv::Point(-1, -1), 3);
    cv::erode(frame2, frame2, kernel, cv::Point(-1, -1), 3);
    
    cv::imshow("frame2", frame2);
    cv::waitKey(0);

    
    cv::imshow("frame1", frame1);
    cv::waitKey(0);
    */
    
    cv::rectangle(gray1, cv::Point(0, 0), cv::Point(640, 480), cv::Scalar(255), 6);
    cv::bitwise_not(gray1, gray1);
    cv::imwrite("laser_scan_aggregate.png", gray1);

    return 0;
}
