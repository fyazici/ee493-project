#include <iostream>
#include <array>
#include <vector>
#include <string>
#include <opencv2/opencv.hpp>


int main ([[maybe_unused]] int argc, [[maybe_unused]] char **argv)
{
    cv::VideoCapture cam1 {1}, cam2 {2};

    if ( ! ( cam1.isOpened() && cam2.isOpened() ) ) {
        std::cerr << "Opening cameras failed." << std::endl;
        return -1;
    }

    auto clamp = [] ( float ll, float ul, float x ) {
        return std::min ( ul, std::max ( ll, x ) );
    };
    auto clamp240 = [clamp] ( float x ) {
        return clamp ( 0, 240, x );
    };
    auto pbrisk = cv::BRISK::create();
    cv::Mat frame1, frame2, gray1, gray2;
    std::vector<cv::KeyPoint> kp1, kp2;
    cv::Mat desc1, desc2;

    while ( std::tolower ( cv::waitKey ( 33 ) ) != 'q' ) {
        cam1.grab();
        cam2.grab();

        cam1.retrieve ( frame1 );
        cam2.retrieve ( frame2 );

        cv::cvtColor(frame1, gray1, cv::COLOR_BGR2GRAY);
        cv::cvtColor(frame2, gray2, cv::COLOR_BGR2GRAY);

        auto brisk = pbrisk.get();
        brisk->detectAndCompute ( gray1, cv::noArray(), kp1, desc1 );
        brisk->detectAndCompute ( gray2, cv::noArray(), kp2, desc2 );

        cv::BFMatcher matcher ( cv::NORM_HAMMING );
        std::vector<cv::DMatch> matches;
        matcher.match ( desc1, desc2, matches );

        cv::Mat depth_map = cv::Mat::zeros ( gray1.size(), CV_32FC3 );

        for ( auto &match: matches ) {
            auto pt1 = kp1[match.queryIdx].pt;
            auto pt2 = kp2[match.trainIdx].pt;
            auto delta = pt1 - pt2;
            if ( std::abs ( delta.x ) < 10 ) {
                auto pixel_distance = std::abs ( delta.y );
                auto distance = 1.0f/pixel_distance;
                auto &value = depth_map.at<std::array<float, 3>> ( pt1.y, pt1.x );
                float fg = 0.040, bg = 0.010;
                value[0] = clamp240 (
                               static_cast<float> ( 240* ( distance-bg ) / ( fg-bg ) )
                           );
                value[1] = value[2] = 1.0f;
            }
        }

        auto kernel = cv::Mat::ones ( 11, 11, CV_32FC1 );
        cv::dilate ( depth_map, depth_map, kernel );
        cv::GaussianBlur ( depth_map, depth_map, cv::Size {21,21}, 5.0 );

        cv::cvtColor ( depth_map, depth_map, cv::COLOR_HSV2BGR );
        cv::imshow ( "Camera 1", frame1 );
        cv::imshow ( "Camera 2", frame2 );
        cv::imshow ( "Depth Map", depth_map );

        matches.erase ( std::remove_if ( matches.begin(), matches.end(), [&kp1, &kp2] ( const cv::DMatch &dm ) {
            auto p1 = kp1[dm.queryIdx].pt;
            auto p2 = kp2[dm.trainIdx].pt;
            auto delta = p1-p2;
            return ( abs ( delta.x ) > 10 );
        } ), matches.end() );

        cv::Mat all_matches;
        cv::drawMatches ( frame1, kp1, frame2, kp2, matches, all_matches, cv::Scalar::all ( -1 ), cv::Scalar::all ( -1 ), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS );
        cv::imshow ( "BRISK All Matches", all_matches );

    }


    cam1.release();
    cam2.release();

    return 0;
}
