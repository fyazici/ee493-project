#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov 11 19:06:28 2018

@author: fatih
"""

import asyncio
import serial
import logging
from matplotlib import pyplot as plt
import numpy as np

async def produce(queue):
    try:
        with serial.Serial("/dev/ttyUSB0", baudrate=2000000) as ser:
            line = ""
            while True:
                line += ser.read_until().decode("ascii")
                startpos = line.find("START")
                
                if startpos == -1: 
                    continue
                
                endpos = line.find("END")
                
                if endpos == -1: 
                    continue
                
                packet = line[(startpos+7):(endpos-1)]
                logging.info("[PROD] packet produced. line:" + str(len(line)))
                line = line[(endpos+5):]
                await queue.put(packet)    
    except serial.SerialException:
        await queue.put(None)

async def consume(queue):
    plt.figure()
    ln, = plt.plot([])
    plt.ion()
    plt.show()
    plt.xlim((0, 5000))
    plt.ylim((-24, 1048))
    while True:
        try:
            logging.info("[CONS] get call")
            item = await queue.get()
            if item is None:
                break
            data = []
            for i in range(0, len(item), 3):
                data.append(int(item[i:(i+3)], 16))
            np.savetxt("servo_step_data.txt", np.array(data, dtype=np.uint16))
            plt.pause(0.001)
            ln.set_xdata(range(len(data)))
            ln.set_ydata(data)
            plt.draw()
        except Exception:
            pass

if __name__ == "__main__":
    logging.getLogger().setLevel(logging.DEBUG)
    logging.debug("test")
    loop = asyncio.get_event_loop()
    queue = asyncio.Queue(maxsize=1, loop=loop)
    producer_coro = produce(queue)
    consumer_coro = consume(queue)
    loop.run_until_complete(asyncio.gather(producer_coro, consumer_coro))
    loop.close()
