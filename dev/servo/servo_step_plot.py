#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 12 02:00:06 2018

@author: fatih
"""

import numpy as np
from matplotlib import pyplot as plt

data = np.loadtxt("servo_step_data.txt", dtype=np.uint16)
t=0.1196*np.arange(len(data))

a=425
b=748
d=b-a
c=d*0.05
e=b+c
f=b-c
ul=np.ones_like(data)*e
ll=np.ones_like(data)*f

plt.plot(t, data, "-")
plt.plot(t, ul, "--")
plt.plot(t, ll, "--")
plt.legend(("45-135 Step Response", "$\pm$ 5%"))
plt.title("Step Response of Unloaded SG90 Servo Motor")
plt.xlabel("time (ms)")
plt.ylabel("ADC Reading")
plt.show()
