#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 17:41:18 2019

@author: fatih
"""

import asyncio
import aioconsole
import serial_asyncio
import argparse
import signal
import requests

DEVICE_ENUMERATOR_URL = "http://localhost:8001"

async def send(w, ee):
    while True:
        try:
            msg = await aioconsole.ainput(">> ", use_stderr=True)
            w.write((msg + '\n').encode())
        except asyncio.CancelledError:
            break
        except EOFError:
            ee.set()
            break

async def recv(r):
    while True:
        try:
            msg = await r.readline()
            await aioconsole.aprint(msg.decode())
        except asyncio.CancelledError:
            break

async def main(loop):
    parser = argparse.ArgumentParser()
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-p", "--path", required=False)
    group.add_argument("-t", "--type", required=False)
    parser.add_argument("-b", "--baudrate", default=1000000, required=False)
    args = parser.parse_args()
    device_path, device_type, baudrate = args.path, args.type, args.baudrate
    
    # perform fresh scan
    if device_path is None:      
        # perform fresh scan
        # requests.get(DEVICE_ENUMERATOR_URL + "/scan")
        resp = requests.post(DEVICE_ENUMERATOR_URL + "/dev", data={"type": device_type.upper()}, timeout=1)
        device_path = resp.text
    
    if not device_path:
        print("Could not connect!")
        return
    
    reader, writer = await serial_asyncio.open_serial_connection(url=device_path, baudrate=baudrate)
    
    exit_event = asyncio.Event()
    loop.add_signal_handler(signal.SIGINT, exit_event.set)
    
    send_task = asyncio.ensure_future(send(writer, exit_event))
    recv_task = asyncio.ensure_future(recv(reader))
    
    await exit_event.wait()
    await aioconsole.aprint("exiting...")
    
    send_task.cancel()
    recv_task.cancel()
    await asyncio.gather(send_task, recv_task)
    
loop = asyncio.get_event_loop()
loop.run_until_complete(main(loop))
loop.close()