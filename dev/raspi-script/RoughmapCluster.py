#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 02:01:59 2019

@author: fatih
"""

import numpy as np
import argparse
from sklearn.cluster import DBSCAN

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--datafile")
parser.add_argument("-o", "--outfile")
parser.add_argument("-d", "--display", action="store_true", required=False, default=False)
args = parser.parse_args()

if args.display:
    import matplotlib.pyplot as plt

data = np.load(args.datafile)
theta = data["theta"]
rad = data["rad"]

if args.display:
    plt.polar(np.deg2rad(theta), rad, 'x')
    plt.legend(["sensor data"])
    plt.title("Polar Plot of Sensor Readings")
    plt.show()

e = np.vstack((theta, rad))
    
#d = np.load("data/lidar_493.npz")["arr_0"]
#e = d[:, d[1] != -1]
#e = e[:, e[0].argsort()]

xs = e[1] * np.cos(np.deg2rad(e[0])).T
ys = e[1] * np.sin(np.deg2rad(e[0])).T

minx, maxx = np.min(xs), np.max(xs)
miny, maxy = np.min(ys), np.max(ys)

w = np.uint32(2 * max([-minx, maxx]))
h = np.uint32(2 * max([-miny, maxy]))

print(minx, maxx, miny, maxy)
print(w, h)

#####################

pts = np.vstack((xs, ys)).T
print(pts.shape)
db = DBSCAN(eps=50, min_samples=3).fit(pts)

print(db.core_sample_indices_)
print(db.labels_)

core_sample_mask = np.zeros_like(db.labels_, dtype=bool)
core_sample_mask[db.core_sample_indices_] = True
labels = db.labels_
unique_labels = set(labels)

clusters = []

for k in unique_labels:
    class_member_mask = (labels == k)
    xy = pts[class_member_mask & core_sample_mask] 
    clusters.append(xy)
    if args.display:
        plt.scatter(xy[:, 0], xy[:, 1])

if args.display:
    plt.title("Segmentation Distance Map")
    plt.legend(["Wall", "Cylinder 5cm", "Wall", "Triangle", "Wall", "Square", "Wall", "Cylinder 10cm"], loc=4)
    plt.show()

np.savez(args.outfile, clusters=clusters)















