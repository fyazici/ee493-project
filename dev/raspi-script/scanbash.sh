#!/bin/bash
echo LidarProvider
python3 LidarProvider.py -v -o rmap/scan.npz
echo LidarProcess
python3 LidarProcess.py -s rmap/scan.npz -o rmap/scan_init.npz
echo RoughmapCluster
python3 RoughmapCluster.py -s rmap/scan_init.npz -o rmap/scan_map.npz -d
echo travel_algorithm
python3 travel_algorithm.py -v -s rmap/scan_map.npz