#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 23:27:18 2019

@author: metecan
"""

import numpy as np
import argparse
from scipy import spatial
from navigation_control import NavigationController
from operator import itemgetter
import logging
import subprocess

import matplotlib.pyplot as plt
import cv2



length_of_line = np.hypot
#def length_of_line(x,y):
#    distance = math.sqrt(x**2+y**2)
#    return distance


#Takes distance arg , make movement update the robot coordianates
length_of_line = np.hypot
def cart2pol(xy):
    x, y = xy
    dist = np.hypot(x, y)
    angle = np.rad2deg(np.arctan2(y, x))
    return (dist, angle)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)
#def length_of_line(x,y):
#    distance = math.sqrt(x**2+y**2)
#    return distance
class NavigationSimulator():
    def __init__(self):
        self.px = 0
        self.py = 0
        self.theta = 0
        
    def movement(self,dist):
        self.px , self.py = pol2cart(dist, self.theta)
        
    def rotation(self,angle):
        self.theta = angle + self.theta
        
    def add_error(self):
        self.px = self.px * 1.02
        self.py = self.py * 1.02
        self.theta = self.theta * 1.02 
        
    def odometry(self):
        self.add_error()
        return self.px, self.py, self.theta


#class node():
#    def __init__(self):
        
#class ScanTree():
 #   def __init__(self):
#Takes distance arg , make movement update the robot coordianates
class RobotControl():
    def __init__(self):
        self.robot_status = {
            "position": np.float32([0,0]),
            "angle": 0,
        }
        
        self.robot_destination = {
            "distance":0,
            "position":np.float32([0,0]),
            "angle":0
        }
        
        self.start_pos = np.float32([0,0])
        self.travel_log = []
        
    def update_robot_status(self):
        px, py, ptheta = NC.odometry()
        self.robot_status["position"] = np.float32([px, py])
        self.robot_status["angle"] = ptheta
        
    def update_travel_log(self):
        self.travel_log.append(dict(self.robot_status))
        
    def movement(self,dist):
        logging.debug("movement to {}".format(dist))
        NC.movement(dist)
        self.update_robot_status()
        logging.debug(self.robot_status)
        self.update_travel_log()
        
    def rotation(self,theta):
        logging.debug("Rotation to {}".format(theta))
        NC.rotation(theta - self.robot_status["angle"])
        self.update_robot_status()
        self.update_travel_log()
        
    def set_destination(self, pos):
        self.robot_destination["position"] = pos
        
    def go_to_destination(self, offset=0):
        logging.debug("Moving to position")
        self.update_robot_status()
        
        dist, angle = cart2pol(self.robot_destination["position"] - self.robot_status["position"])
        
        self.rotation(angle)
        self.movement(dist - offset)
        #self.correcting_position()
        
    def correcting_position(self):
        logging.debug("Position Correction")
        self.print_robot_destination()
        self.print_robot_status()
        dist, angle = cart2pol(self.robot_destination["position"] - self.robot_status["position"])
        self.rotation(angle)
        self.movement(dist)
        
    def return_start_position(self):
        logging.debug("Robot return start position x:{}, y:{}".format(0,0))
        
        dist, angle = cart2pol(self.robot_destination["position"] - self.robot_status["position"])
        
        self.set_destination([0, 0])
        self.go_to_destination()
        
    def print_robot_status(self):
        logging.debug(self.robot_status)
        
    def print_travel_log(self):
        logging.debug(self.travel_log)
        
    def print_robot_destination(self):
        logging.debug(self.robot_destination)
        

class ScanMap():
    def __init__(self):
        self.closest_points = []
        
    def store_map(self,path):
        data = np.load(path)
        self.clusters = data["clusters"]
        
    def find_shortest_point(self):
        for xy in self.clusters:
            if (len(xy) > 0 and len(xy) < 20):
                print(len(xy))
                closest_point = { "coordinates": None, "distance": 20000 }
                for p in xy:
                    distance = np.linalg.norm(p - RC.robot_status["position"])
                    if closest_point["distance"] > distance:
                        closest_point["distance"] = distance
                        closest_point["coordinates"] = p

                self.closest_points.append(closest_point)
        print(self.closest_points)
        self.closest_points = sorted(self.closest_points, key=itemgetter('distance'))
        print(self.closest_points)
    
    def plot_points(self):
        for xy in SM.closest_points:
            plt.plot(xy['coordinates'][0], xy['coordinates'][1],'.')
            
class Travel_Algoritm():
    def decode_map(self):
        SM.store_map(args.datafile)
        SM.find_shortest_point()
        SM.plot_points()
        
    def travel_map(self):
        for closest_point in SM.closest_points:
            dist, angle = cart2pol(closest_point['coordinates'] - RC.robot_status["position"])
            RC.set_destination(closest_point['coordinates'])
            RC.go_to_destination(offset=100+120)
            input("block")
            RC.return_start_position()   
            
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--datafile")
parser.add_argument("-v", "--verbose", action="store_true")
args = parser.parse_args()

if args.verbose:
    logging.basicConfig(level=logging.DEBUG)

NC = NavigationController()
NC.connect()
logging.debug("Mega connected")
logging.basicConfig(level=logging.DEBUG)

RC = RobotControl() 
SM = ScanMap()
TA = Travel_Algoritm()
TA.decode_map()
TA.travel_map()
