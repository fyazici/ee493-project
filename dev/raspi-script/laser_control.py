#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar 24 18:41:28 2019

@author: fatih
"""

import requests
import serial
import time

DEVICE_ENUMERATOR_URL = "http://localhost:8001"

class LaserController:
    def wait_ack(self):
        while True:
            r = self.device.readline()
            if r.startswith(b"#"):
                continue
            if r == b"ACK\r\n":
                break
    
    def send_wait(self, cmd):
        self.device.write(cmd)
        self.wait_ack()
    
    def connect(self):
        #requests.get(DEVICE_ENUMERATOR_URL + "/scan")
        devname = requests.post(DEVICE_ENUMERATOR_URL + "/dev", data={"type": "UNO"})
        self.device = serial.Serial(devname.text, baudrate=1000000)
        #self.device.flush()
        time.sleep(3)
        self.send_wait(b"I\n")
        return True
    
    def disconnect(self):
        self.laser_stop()
        self.laser_power(0)
        self.device.close()
    
    def laser_power(self, pwm):
        self.send_wait("LB {}\n".format(pwm).encode())
        
    def laser_start(self):
        self.send_wait(b"LI\n")
        
    def laser_stop(self):
        self.send_wait(b"LO\n")
    
    def laser_position(self, pos):
        self.send_wait("LP {}\n".format(pos).encode())
    
    def laser_center(self):
        self.send_wait(b"LP 90\n")