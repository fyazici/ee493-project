#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 02:01:59 2019

@author: fatih
"""

import numpy as np
import cv2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("datafile")
datapath = parser.parse_args().datafile


def showd(img):
    h, w, _ = img.shape
    m = max(h, w)
    h = h * 1000 / m
    w = w * 1000 / m
    img = cv2.flip(img, -1)
    img2 = cv2.resize(img, (np.uint(w), np.uint(h)))
    cv2.imshow("img", img2)
    cv2.waitKey(0)

data = np.load(datapath)
theta = data["theta"]
rad = data["rad"]
e = np.vstack((theta, rad))
    
#d = np.load("data/lidar_493.npz")["arr_0"]
#e = d[:, d[1] != -1]
#e = e[:, e[0].argsort()]

xs = e[1] * np.cos(np.deg2rad(e[0])).T
ys = e[1] * np.sin(np.deg2rad(e[0])).T

minx, maxx = np.min(xs), np.max(xs)
miny, maxy = np.min(ys), np.max(ys)

w = np.uint32(2 * max([-minx, maxx]))
h = np.uint32(2 * max([-miny, maxy]))

ys = ys + h/2
xs = xs + w/2

print(minx, maxx, miny, maxy)
print(w, h)

idx = []
idxs = []

for i in range(1, len(xs)):
    p1y = ys[i-1]
    p1x = xs[i-1]
    p2y = ys[i]
    p2x = xs[i]
    
    if np.hypot(p1x-p2x, p1y-p2y) < 100:
        idx.append(i)
    else:
        idxs.append(idx[:])
        idx = []

print(idxs)

img = np.zeros((h + 1, w + 1, 3), dtype=np.uint8)

for (x, y) in zip(xs, ys):
    cv2.circle(img, (np.int32(x), np.int32(y)), 5, (0, 0, 255), 3)

showd(img)

for idx in idxs:
    cv2.polylines(img, np.int32([np.column_stack((xs[idx], ys[idx]))]), False, (255, 0, 0), 3)

showd(img)

lines = cv2.HoughLinesP(img[:, :, 0], 10, np.pi/180, 100, None, 200, 50)
if not (lines is None):
    print(lines.shape)    
    for (x1, y1, x2, y2) in lines[:, 0]:
        cv2.line(img, (x1, y1), (x2, y2), (0, 255, 0), 50)

showd(img)

#img[np.uint32(ys + h/2), np.uint32(xs + w/2), 2] = 255

#kernel1 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (21, 21))
#kernel2 = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (11, 11))
#img = cv2.morphologyEx(img, cv2.MORPH_DILATE, kernel1)
#img = cv2.morphologyEx(img, cv2.MORPH_ERODE, kernel2)

circles = cv2.HoughCircles(img[:, :, 0], 
                           cv2.HOUGH_GRADIENT, 1, 200, 
                           None, 
                           100, 10, 
                           20, 200)

if not (circles is None):
    print(circles)
    for (x, y, r) in circles[0]:
        cv2.circle(img, (x, y), r, (0, 255, 0), 2)

showd(img)


#showd(img)

cv2.destroyAllWindows()