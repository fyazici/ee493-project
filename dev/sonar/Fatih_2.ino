#include <Servo.h>

Servo servoMotor;

void setup() {
  Serial.begin(2000000);
  // put your setup code here, to run once:
  servoMotor.attach(6);
  delay(2000);
  pinMode(11, OUTPUT);
  pinMode(10, INPUT);
  digitalWrite(11, LOW);
}

void measure_distance() {
   // wait for servo to settle
  digitalWrite(11, HIGH);
  delayMicroseconds(10);
  digitalWrite(11, LOW);
  int meas = pulseIn(10, HIGH)/58;
  //Serial.print("Distance: ");
  //if (meas < 100)
  Serial.println(meas);	// print always
  //Serial.println(" cm");
}

void loop() {
  // put your main code here, to run repeatedly:
  for (int i=30; i < 150; i += 5) {
     servoMotor.write(i);
     delay(200);
     for (int j=0;j<5;++j)
     {
	Serial.print(i);
	Serial.print(" ");
        measure_distance();
     }
  }

  for (int i=150; i > 30; i -= 5) {
     servoMotor.write(i);
     delay(200);
     for (int j=0;j<5;++j)
     {
	Serial.print(i);
	Serial.print(" ");
      	measure_distance();
     }
  }

}
