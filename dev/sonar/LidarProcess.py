#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 21:52:22 2019

@author: metecan
"""

import numpy as np
import argparse
from collections import defaultdict
import itertools

parser = argparse.ArgumentParser()
parser.add_argument("-s", "--scanfile")
parser.add_argument("-o", "--outfile")
parser.add_argument("-d", "--display", action="store_true", required=False, default=False)
args = parser.parse_args()

if args.display:
    import matplotlib.pyplot as plt
    #print("Import lib")
data = np.load(args.scanfile)

sens1, sens2 = data["sens1"], data["sens2"]

#sens1 = sens1[:, sens1[1] != -1]    # no timeout
#sens2 = sens2[:, sens2[1] != -1]    # no timeout

sens1[:, 0] = (-sens1[:, 0]) % 360
sens2[:, 0] = (-sens2[:, 0]) % 360

sens = defaultdict(list)

for (t, r) in itertools.chain(sens1, sens2):
    if r != -1:
        sens[int(t)].append(r + 30) # lidar pos to robot center

if args.display:
    print(sens)

sens_avg = { t : np.mean(rs) for (t, rs) in sens.items() if len(rs) > 0 }

theta = np.fromiter(sens_avg.keys(), dtype=np.float)
rad = np.fromiter(sens_avg.values(), dtype=np.float)

if args.display:
    #print(theta)
    #print(rad)
    plt.polar(np.deg2rad(theta), rad, '.')
    plt.show()

np.savez(args.outfile, theta=theta, rad=rad)



#sens1 = sens1[:, np.argsort(sens1[0])]
#sens2 = sens2[:, np.argsort(sens2[0])]
#
##print(sens1, sens2)
#
#no_data_cols = [True] * len(sens1[0])
#
#for i in range(len(sens1[0])):
#    if sens1[1, i] == -1:
#        if sens2[1, i] == -1:
#            no_data_cols[i] = False
#        else:
#            sens1[1, i] = sens2[1, i]
#    if sens2[1, i] == -1:
#        if sens1[1, i] == -1:
#            no_data_cols[i] = False
#        else:
#            sens2[1, i] = sens1[1, i]
#
##print(no_data_cols)
#
#sens1 = sens1[:, no_data_cols]
#sens2 = sens2[:, no_data_cols]
#
##print(sens1, sens2)
#
#sens1[1] = sens1[1] + 24
#sens2[1] = sens2[1] + 24
#
#if args.display:
#    plt.polar(np.deg2rad(180 - sens1[0]), sens1[1], 'x', np.deg2rad(180 - sens2[0]), sens2[1], '.')
#    plt.show()
#
#def sigmoid(x):  
#    return np.exp(-np.logaddexp(0, -x))
#
#def mapfn(x, k=15, m=50):
#    return sigmoid(x/k - m)
#
##print(sens1, sens2)
#
#avgs = (sens1[1] + sens2[1]) / 2
#alpha = mapfn(avgs)
##print(alpha)
#beta = 1 - alpha
#meas = alpha*sens2[1] + beta*sens1[1]
#
#if args.display:
#    plt.polar(np.deg2rad(180 - sens1[0]), meas, '.')
#    plt.show()
#
#measd = {}
#for i in range(len(sens1[0])):
#    if sens1[0, i] in measd:
#        measd[sens1[0, i]].append(meas[i])
#    else:
#        measd[sens1[0, i]] = [meas[i]]
#
##print(measd)
#
#measd_avg = {}
#for k in measd.keys():
#    measd_avg[k] = np.mean(measd[k])
#    
##print(measd_avg)
#
#theta = np.fromiter(measd_avg.keys(), dtype=np.float)
#rad = np.fromiter(measd_avg.values(), dtype=np.float)
##print(theta, rad)
#
#np.savez(args.outfile, theta=theta, rad=rad)
#
#if args.display:
#    plt.polar(np.deg2rad(180 - theta), rad, '.')
#    plt.show()
