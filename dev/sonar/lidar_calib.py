#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 00:08:11 2019

@author: metecan
"""

import numpy as np
import matplotlib.pyplot as plt


data = np.loadtxt("lidar/lidar.txt", delimiter=',', skiprows=1)

real, meas = data.T
real = real * 10    # cm to mm

#plt.scatter(real, meas), plt.show()

error = meas - real
#plt.scatter(real, error), plt.show()

ps = np.polyfit(meas, real, 5)
print(ps)

p = np.poly1d(ps)
estimate = p(meas)
diff = estimate - real
print("mean diff: ", np.mean(diff), " var diff: ", np.var(diff))

plt.scatter(real, error)
plt.scatter(real, estimate - real)
plt.title("Error (measured - real) vs Absolute Distance Graph for LIDAR")
plt.ylabel("Error (mm)")
plt.xlabel("Distance (mm)")
plt.legend(["Sensor Data", "3rd order poly. fit"])
plt.show()