#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  7 22:49:53 2018

@author: fatih
"""

from flask import Flask, Response, abort, jsonify, json
import serial
import logging
import threading
from time import sleep
import queue

app = Flask(__name__)

class DataLinkController:
    def __init__(self):
        logging.debug("Controller.initialize issued")
        self.initialized = True
        self.connected = False
        self.device = None
        self.scanning = False
        self.scan_queue = queue.Queue()
        logging.debug("Controller initialized")
    
    def readdev(self):
        while True:
            resp_b = self.device.readline()
            logging.debug("Device responded: %s", resp_b)
            resp = resp_b.decode().strip()
            if resp[0] != "#":
                # not debug output
                return resp
        
    
    def connect(self, dev="/dev/ttyACM0", baudrate=1000000):
        logging.debug("Controller.connect issued")
        if not self.initialized:
            return False
        
        self.scanning = False
        self.device_path = dev
        try:
            self.device = serial.Serial(self.device_path, baudrate)
        except:
            self.connected = False
            return False
        
        self.device.setDTR(False)
        sleep(3)
        self.device.flushInput()
        #self.device.setDTR(True)
        
        if not self.device.is_open:
            self.connected = False
            return False
        
        logging.debug("Device opened")
        
        self.device.write("I\n".encode())
        logging.debug("Wait for device identity response")
        resp = self.readdev()
        if resp == "ACK":
            self.connected = True
            return True
        
        self.connected = False
        return False
    
    def get_status_response(self):
        logging.debug("Controller.get_status issued")
        status_text = ""
        if self.scanning:
            status_text = "Scanning..."
        elif self.connected:
            status_text = "Connected to scanning data provider"
        elif self.initialized:
            status_text = "Adapter initialized - Not connected"
        else:
            status_text = "Adapter not initialized"
        return {
                "status": status_text,
                "connected": self.connected,
                "scanning": self.scanning
            }
        
    def disconnect(self):
        if self.connected:
            logging.debug("Controller.disconnect issued")
            self.device.close()
        self.scanning = False
        self.connected = False
        return True

    def scan_start(self):
        logging.debug("Controller.scan_start issued")
        if self.scanning or not self.connected:
            return False
                
        self.device.write("C\n".encode())
        resp = self.readdev()
        if resp == "ACK":
            resp = self.readdev()
            if resp != "END":
                return False
        
        self.device.write("S\n".encode())
        logging.debug("Wait for scan start command response")
        resp = self.readdev()
        if resp == "ACK":
            self.scanning = True
            self.scan_queue = queue.Queue()
            self.scan_thread = threading.Thread(target=self.scan_thread_target)
            self.scan_thread.start()
            return True
        
        self.scanning = False
        return False

    def scan_thread_target(self):
        logging.debug("Controller.scan_thread_target issued")
        #if not self.scanning:
        #    logging.debug("Controller.scan_thread_target early finish")
        #    return None
        
        #self.device.flushOutput()
        while True:
            logging.debug("Controller.scan_thread_target ask device data")
            resp = self.readdev()
            if resp == "END":
                self.scanning = False
                logging.debug("Controller.scan_thread_target ended")
                break
            self.scan_queue.put(tuple(map(float, resp.split(","))))

    def scan_get_partial_results(self):
        logging.debug("Controller.scan_get_partial_results issued")
        if not self.scan_queue.empty():
            results = []
            while not self.scan_queue.empty():
                results.append(self.scan_queue.get())
                self.scan_queue.task_done()
            return (True, results)
        elif self.scanning:
            logging.debug("Controller.scan_get_partial_results early finish")
            return (True, [])
        else:
            return (False, [])


@app.route("/connect")
def dev_connect():
    if not ctl.connect():
        abort(400)
    resp = jsonify(ctl.get_status_response())
    resp.headers.add('Access-Control-Allow-Origin', '*')
    return resp

@app.route("/disconnect")
def dev_disconnect():
    if not ctl.disconnect():
        abort(400)
    resp = jsonify(ctl.get_status_response())
    resp.headers.add('Access-Control-Allow-Origin', '*')
    return resp

@app.route("/status")
def dev_status():
    resp = jsonify(ctl.get_status_response())
    resp.headers.add('Access-Control-Allow-Origin', '*')
    return resp

@app.route("/scan")
def dev_scan():
    if not ctl.scan_start():
        abort(400)
    resp = jsonify(ctl.get_status_response())
    resp.headers.add('Access-Control-Allow-Origin', '*')
    return resp

@app.route("/data")
def dev_scan_data():
    r, data = ctl.scan_get_partial_results()
    if not r:
        abort(400)
    resp = ctl.get_status_response()
    resp["mapData"] = data
    resp = jsonify(resp)
    resp.headers.add('Access-Control-Allow-Origin', '*')
    return resp
    
if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    ctl = DataLinkController()
    app.run()


    
