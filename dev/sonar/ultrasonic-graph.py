#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 31 09:41:03 2018

@author: fatih
"""

import numpy as np
import matplotlib.pyplot as plt

def polar_plot(angle_deg, dist_cm, angle_ll=None, angle_ul=None, dist_ll=None, dist_ul=None, fmt='.'):
    angle_rad = np.deg2rad(angle_deg)
    plt.polar(angle_rad, dist_cm, fmt)
    plt.xlabel("distance (mm)") # should be ylabel but this has better placement
    ax = plt.gca()
    ax.xaxis.set_label_coords(0.75, 0.15)
    if not (angle_ll is None or angle_ul is None):
        plt.xlim((np.deg2rad(angle_ll), np.deg2rad(angle_ul)))
    if not (dist_ll is None or dist_ul is None):
        plt.ylim((dist_ll, dist_ul))
    plt.show()

#np.set_printoptions(threshold=np.nan)

import argparse
args = argparse.ArgumentParser()
args.add_argument("data", type=str)
args = args.parse_args()

data = np.loadtxt(args.data).T
angle_raw = data[0]
dist_raw = data[1]
plt.plot(angle_raw, dist_raw, '.')
plt.xlabel("angle (deg)")
plt.ylabel("distance (mm)")
plt.show()

polar_plot(angle_raw, dist_raw, 0, 180, 0, 1000)

angles = np.unique(angle_raw)
distmap_raw = dict()
for b in angles:
    distmap_raw[b] = []

for (x, y) in zip(angle_raw, dist_raw):
    distmap_raw[x].append(y)

distances = []
for x in angles:
    distances.append(np.median(distmap_raw[x]))

#dist = scipy.signal.medfilt(dist, kernel_size=11)
plt.plot(angles, distances)
plt.xlabel("angle (deg)")
plt.ylabel("distance (mm)")
plt.show()

polar_plot(angles, distances, 0, 180, 0, 500, '.-')
