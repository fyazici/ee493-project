#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 02:01:59 2019

@author: fatih
"""

import numpy as np
import logging
from sklearn.cluster import DBSCAN
from collections import namedtuple

def _run(args):
    if args.display:
        import matplotlib.pyplot as plt
    
    data = np.load(args.datafile)
    theta = data["theta"]
    rad = data["rad"]
    
    if args.display:
        plt.polar(np.deg2rad(theta), rad, 'x')
        plt.show()
    
    e = np.vstack((theta, rad))
        
    #d = np.load("data/lidar_493.npz")["arr_0"]
    #e = d[:, d[1] != -1]
    #e = e[:, e[0].argsort()]
    
    xs = e[1] * np.cos(np.deg2rad(e[0])).T
    ys = e[1] * np.sin(np.deg2rad(e[0])).T
    
    minx, maxx = np.min(xs), np.max(xs)
    miny, maxy = np.min(ys), np.max(ys)
    
    w = np.uint32(2 * max([-minx, maxx]))
    h = np.uint32(2 * max([-miny, maxy]))
    
    logging.debug(f"Limits: {minx}, {maxx}, {miny}, {maxy}")
    logging.debug(f"Size: {w}, {h}")
    
    #####################
    
    pts = np.vstack((xs, ys)).T
    #print(pts.shape)
    db = DBSCAN(eps=40, min_samples=3).fit(pts)
    
    #print(db.core_sample_indices_)
    #print(db.labels_)
    
    core_sample_mask = np.zeros_like(db.labels_, dtype=bool)
    core_sample_mask[db.core_sample_indices_] = True
    labels = db.labels_
    unique_labels = set(labels)
    
    clusters = []
    
    for k in unique_labels:
        class_member_mask = (labels == k)
        xy = pts[class_member_mask & core_sample_mask] 
        clusters.append(xy)
        if args.display:
            plt.scatter(xy[:, 0], xy[:, 1])
    
    if args.display:
        plt.show()
    
    np.savez(args.outfile, clusters=clusters)
    return clusters
    
def run(datafile, outfile, display=False):
    Args_ = namedtuple("Args_", ["datafile", "outfile", "display"])
    args = Args_(datafile, outfile, display)
    return _run(args)
    
if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--datafile")
    parser.add_argument("-o", "--outfile")
    parser.add_argument("-d", "--display", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    _run(args)

    