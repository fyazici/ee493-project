#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Mar  2 23:01:00 2019

@author: fatih
"""

import numpy as np
import cv2
import logging
import concurrent.futures
import threading
from collections import namedtuple

MAX_FRAMES_IN_MEM = 192
MAX_THREADS = 4

def _run(args, logger):
    logger.debug("start capturing input and intermediate output")
    try:
        capture_name = int(args.capture)
        logger.info("Reading from camera #{}".format(capture_name))
    except ValueError:
        capture_name = args.capture
        logger.info("Reading from file named {}".format(capture_name))
    
    cap = cv2.VideoCapture(capture_name)
    
    #cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1920)
    #cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1080)
    
    w = np.uint(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
    h = np.uint(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))
    logger.debug("width: " + str(w) + " height: " + str(h))
    aggregate_pts = []
    
    for i in range(0, 30):
        _ = cap.read()
        
    logger.debug("get background")
    _, bg = cap.read()
    #bg = cv2.cvtColor(bg, cv2.COLOR_RGB2BGR)
    bg = bg[200:720, 260:1020]
    
    if args.display:
        cv2.imshow("bg", bg)
        cv2.waitKey(0)
        
    logger.debug("TODO: calibrate for laser dot brightness")
    
    def process_frame(frame_orig, args, exit_flag, mem_sem):
        aggregate_pts = []
        logger.debug("subtract background and emphasize regions of interest")
        if args.display:
            frame = frame_orig.copy()
        else:
            frame = frame_orig
        
        frame_diff = cv2.absdiff(frame, bg)
        frame_diff = cv2.cvtColor(frame_diff, cv2.COLOR_BGR2GRAY)
        frame_diff = cv2.merge((frame_diff, frame_diff, frame_diff))
        frame = np.uint8(frame * np.float32(frame_diff) / np.max(frame_diff))
        
        logger.debug("TODO: correct for camera and device orientation")
        #M = cv2.getRotationMatrix2D((1920/2,1080/2),2,1)
        #frame = cv2.warpAffine(frame, M, (1920, 1080))
        
        logger.debug("filter laser-like regions based on colour")
        ll = (50, 10, 50)
        ul = (150, 255, 255)
        
        hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
        hsv[:, :, 0] = (hsv[:, :, 0] + 90) % 180
        nonred = cv2.inRange(hsv, ll, ul)
        frame = cv2.bitwise_and(frame, frame, nonred)
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        
        #frame2 = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #frame2 = cv2.adaptiveThreshold(frame2, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 51, -10)
        
        logger.debug("select laser dot based on contour shape and size")
        logger.debug("TODO: cv2 version 3.2 returns image also")
        contours, _ = cv2.findContours(frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        contours = list(filter(lambda c: (cv2.contourArea(c) > 200) and (cv2.arcLength(c, True) < 500), contours))
        logging.debug("Contours found #", len(contours))
        if len(contours) > 0:
            logger.debug("find contour center")
            largest = max(contours, key=cv2.contourArea)
            m = cv2.moments(largest)
            cx, cy = m['m10'] / m['m00'], m['m01'] / m['m00']
            aggregate_pts.append((cx, cy))
            logger.info("Contour center: {} {} | Color {}".format(cx, cy, 3333)) #hsv[np.uint(cx), np.uint(cy), :]))
            
            if args.display:
                cv2.drawContours(frame_orig, contours, -1, (0, 255, 0), 1)
                cv2.drawContours(frame_orig, [largest], -1, (0, 255, 0), 3)
                cv2.circle(frame_orig, (np.uint(cx), np.uint(cy)), 5, (255, 0, 0), 3)
        
        if args.display:    
                frame_orig = cv2.resize(frame_orig, None, fx=0.5, fy=0.5)
                cv2.imshow("orig", frame_orig)
                cv2.imshow("frame", ~frame)
                k = cv2.waitKey(0) & 0xFF
                if k == ord('q'):
                    exit_flag.set()
                    return None
                elif k == ord('a'):
                    cap.set(cv2.CAP_PROP_POS_FRAMES, cap.get(cv2.CAP_PROP_POS_FRAMES) - 2)
        
        mem_sem.release()
        return aggregate_pts
    
    result_futures = []
    mempool_sem = threading.Semaphore(MAX_FRAMES_IN_MEM)
    exit_flag = threading.Event()
    
    with concurrent.futures.ThreadPoolExecutor(MAX_THREADS) as pool:
        while (not exit_flag.is_set()):
            logger.debug("get frame")
            read, frame_orig = cap.read()
            if not read:
                break
            
            #frame_orig = cv2.cvtColor(frame_orig, cv2.COLOR_RGB2BGR)
            frame_part = frame_orig[200:720, 260:1020]
            
            mempool_sem.acquire()
            result_futures.append(
                pool.submit(
                    process_frame, 
                    frame_part, 
                    args, 
                    exit_flag, 
                    mempool_sem
               )
            )
    
    for future in concurrent.futures.as_completed(result_futures):
        aggregate_pts += future.result()
    
    if cap.isOpened():
        cap.release()
    
    logger.debug("save data points")
    np.savez(args.outfile, pts=aggregate_pts)
    if args.display:
        cv2.destroyAllWindows()

def run(capture, outfile, display=False):
    Args_ = namedtuple("Args_", ["capture", "outfile", "display", "verbose"])
    args = Args_(capture, outfile, display)
    _run(args, logging)

if __name__ == "__main__":
    import argparse
    
    logging.debug("parse arguments")
    parser = argparse.ArgumentParser()
    parser.add_argument("-c", "--capture", required=True, action="store")
    parser.add_argument("-d", "--display", default=False, action="store_true")
    parser.add_argument("-v", "--verbose", default=False, action="store_true")
    parser.add_argument("-o", "--outfile")
    args = parser.parse_args()
    
    logger = logging.getLogger("laser_geom")
    logger.addHandler(logging.StreamHandler())
    if args.verbose:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
    
    _run(args, logger)