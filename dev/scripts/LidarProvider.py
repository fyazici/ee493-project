#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  4 00:41:10 2019

@author: metecan
"""

import numpy as np
import requests
import serial
import logging
import time
from collections import namedtuple

DEVICE_ENUMERATOR_URL = "http://localhost:8001"

class LidarScanController:
    def wait_ack(self):
        while True:
            r = self.device.readline()
            if r.startswith(b"#"):
                continue
            if r == b"ACK\r\n":
                break

    def send_wait(self, cmd):
        self.device.write(cmd)
        self.wait_ack()
    
    def connect(self):
        #requests.get(DEVICE_ENUMERATOR_URL + "/scan")
        devname = requests.post(DEVICE_ENUMERATOR_URL + "/dev", data={"type": "UNO"})
        self.device = serial.Serial(devname.text, baudrate=1000000)
        #self.device.flush()
        time.sleep(3)
        self.send_wait(b"I\n")
        return True

    def disconnect(self):
        self.device.close()

    def scan_start(self):
        data = []
        logging.debug("File created")
        self.send_wait(b'S\n')
        while True:
            r = self.device.readline()
            logging.debug("Got response: %s", str(r))
            if r == b"END\r\n":
                return np.array(data)
            else:
                data.append(np.float32(r.decode("ASCII").split(",")))

    def scan_fake(self):
        self.send_wait(b"Q\n")

    def scan_calibrate(self):
        self.send_wait(b"C\n")

    def scan_cont_meas(self):
        self.send_wait(b"M\n")

def _run(args):
    logging.debug("Connecting to rough map controller")
    ctl = LidarScanController()
    ctl.connect()
    data = ctl.scan_start()
    ctl.disconnect()
    np.savez(args.outfile, sens1=data[:, 0:2], sens2=data[:, 2:4])

def run(outfile):
    Args_ = namedtuple("Args_", ["outfile"])
    args = Args_(outfile)
    _run(args)

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--outfile")
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    _run(args)
    