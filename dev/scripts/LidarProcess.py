#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 21:52:22 2019

@author: metecan
"""

import numpy as np
import itertools
import logging
from collections import defaultdict, namedtuple

def _run(args):
    if args.display:
        import matplotlib.pyplot as plt
        #print("Import lib")
    data = np.load(args.scanfile)
    
    sens1, sens2 = data["sens1"], data["sens2"]
    
    #sens1 = sens1[:, sens1[1] != -1]    # no timeout
    #sens2 = sens2[:, sens2[1] != -1]    # no timeout
    
    sens1[:, 0] = (-sens1[:, 0]) % 360
    sens2[:, 0] = (-sens2[:, 0]) % 360
    
    sens = defaultdict(list)
    
    for (t, r) in itertools.chain(sens1, sens2):
        if r != -1:
            sens[int(t)].append(r + 30) # lidar pos to robot center
    
    if args.display:
        logging.debug(sens)
    
    sens_avg = { t : np.mean(rs) for (t, rs) in sens.items() if len(rs) > 0 }
    
    theta = np.fromiter(sens_avg.keys(), dtype=np.float)
    rad = np.fromiter(sens_avg.values(), dtype=np.float)
    
    if args.display:
        #print(theta)
        #print(rad)
        plt.polar(np.deg2rad(theta), rad, '.')
        plt.show()
    
    np.savez(args.outfile, theta=theta, rad=rad)

def run(scanfile, outfile, display=False):
    Args_ = namedtuple("Args_", ["scanfile", "outfile", "display"])
    args = Args_(scanfile, outfile, display)
    _run(args)

if __name__ == "__main__":
    import argparse
    
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--scanfile")
    parser.add_argument("-o", "--outfile")
    parser.add_argument("-d", "--display", action="store_true")
    parser.add_argument("-v", "--verbose", action="store_true")
    args = parser.parse_args()
    
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    
    _run(args)
