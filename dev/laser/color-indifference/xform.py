#!/usr/bin/env python3

import cv2
import numpy as np
import argparse
import collections
import os.path

# BEGIN CLASS AppConfig
AppConfig = collections.namedtuple('AppConfig', 'files outputsize xformpts method')
# END CLASS AppConfig

# BEGIN CLASS AppConfigCLI
class AppConfigCLI:
  def parse(self):
    parser = argparse.ArgumentParser(description='Perpective transform multiple files')
    parser.add_argument('-f', '--files', dest='files', nargs='+', type=str, required=True)
    parser.add_argument('-s', '--outputsize', dest='outputsize', type=str, required=True)
    parser.add_argument('-x', '--xformpts', dest='xformpts', type=str, required=True)
    parser.add_argument('-m', '--method', dest='method', type=str, default='sinc')
    args = parser.parse_args()
    return AppConfig(files=args.files, outputsize=tuple(map(int, args.outputsize.split('x'))), xformpts=tuple(zip(*[iter(map(int, args.xformpts.split(',')))]*2)), method=args.method)
# END CLASS AppConfigCLI

# BEGIN CLASS InterpMethodMap
InterpMethodMap = {'nearest': cv2.INTER_NEAREST, 'linear': cv2.INTER_LINEAR, 'cubic': cv2.INTER_CUBIC, 'area': cv2.INTER_AREA, 'sinc': cv2.INTER_LANCZOS4}
# END CLASS InterpMethodMap

# BEGIN CLASS AppMain
class AppMain:
  def __init__(self, config):
    self.config = config
    self.width, self.height = 1400, 900 # TODO: make these variable
    self.currently_selecting = False
    self.selection = []
  
  def run(self):
    cv2.namedWindow('image', cv2.WINDOW_NORMAL)
    cv2.resizeWindow('image', self.width, self.height)
    cv2.setMouseCallback('image', self.mouse_callback)
    
    for fname in config.files:
      self.selection = []
      self.currently_selecting = True
      img = cv2.imread(fname)
      # TODO: check if correctly opened image
      while self.currently_selecting:
        frame = img.copy()
        for pt in self.selection:
          self.draw_crosshair(frame, pt, (255,0,0))
        cv2.imshow('image', frame)
        key = cv2.waitKey(33) & 0xFF
        if key == 27:
          break
      if len(self.selection) == 4:
        xform = cv2.getPerspectiveTransform(np.float32(self.selection), np.float32(self.config.xformpts))
        dst = cv2.warpPerspective(img, xform, self.config.outputsize, flags=InterpMethodMap[self.config.method])
        new_name = list(os.path.splitext(os.path.abspath(fname)))
        new_name.insert(-1, '_xformed')
        cv2.imwrite(''.join(new_name), dst)
  
  def draw_crosshair(self, img, pos, color, length=20, thickness=3):
    x, y = pos
    h, w = img.shape[:2]
    k = min(w / self.width, h / self.height)
    d = int(length*k/2)
    pts = [(x-d, y-d), (x+d, y+d), (x-d, y+d), (x+d, y-d)]
    cv2.line(img, pts[0], pts[1], color, int(thickness*k))
    cv2.line(img, pts[2], pts[3], color, int(thickness*k))

  def mouse_callback(self, event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONUP and self.currently_selecting:
      self.selection.append((x, y))
      if len(self.selection) >= 4:
        self.currently_selecting = False
      
# END CLASS AppMain

if __name__ == '__main__':
  config = AppConfigCLI().parse()
  app = AppMain(config)
  app.run()
