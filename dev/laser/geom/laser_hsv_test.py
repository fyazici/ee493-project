#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 00:20:58 2019

@author: fatih
"""

import numpy as np
import cv2
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("videofile")
videopath = parser.parse_args().videofile

def nullFunc(*args, **kwargs):
    pass

cv2.namedWindow("conf")
cv2.createTrackbar("Hmin", "conf", 0, 180, nullFunc)
cv2.createTrackbar("Hmax", "conf", 180, 180, nullFunc)
cv2.createTrackbar("Smin", "conf", 0, 255, nullFunc)
cv2.createTrackbar("Smax", "conf", 255, 255, nullFunc)
cv2.createTrackbar("Vmin", "conf", 0, 255, nullFunc)
cv2.createTrackbar("Vmax", "conf", 255, 255, nullFunc)

def read_limits():
    hmin = cv2.getTrackbarPos("Hmin", "conf")
    hmax = cv2.getTrackbarPos("Hmax", "conf")
    smin = cv2.getTrackbarPos("Smin", "conf")
    smax = cv2.getTrackbarPos("Smax", "conf")
    vmin = cv2.getTrackbarPos("Vmin", "conf")
    vmax = cv2.getTrackbarPos("Vmax", "conf")
    if hmin > hmax:
        hmin = hmax
    if smin > smax:
        smin = smax
    if vmin > vmax:
        vmin = vmax
    return ((hmin, smin, vmin), (hmax, smax, vmax))

cap = cv2.VideoCapture(videopath)
_, frame = cap.read()
frame = cv2.resize(frame, None, fx=0.5, fy=0.5)

while True:
    ll, ul = read_limits()
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv[:, :, 0] = (hsv[:, :, 0] + 90) % 180
    
    thr = cv2.inRange(hsv, ll, ul)
    
    cv2.imshow("frame", frame)
    cv2.imshow("inRange", thr)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('q'):
        break
    elif k == ord('n'):
        read, frame = cap.read()    
        if not read:
            break
        frame = cv2.resize(frame, None, fx=0.5, fy=0.5)

cv2.destroyAllWindows()