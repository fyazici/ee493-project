#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Mar  3 05:28:55 2019

@author: fatih
"""

from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
import argparse
from sklearn import cluster, linear_model

parser = argparse.ArgumentParser()
parser.add_argument("datapath")
datapath = parser.parse_args().datapath


CAM_FOCAL_LENGTH = 1280          # px (was 2571)
LASER_AXIS_POS = [0, 100, 150]   # in mm
LASER_PLANE_TILT = 90 - 34       # in deg

pts = np.load(datapath)["pts"]
pts[:, 0] -= 760/2
pts[:, 1] -= 520/2

qs = np.hstack((pts, -CAM_FOCAL_LENGTH * np.ones((pts.shape[0], 1))))
qs = np.dot(qs, np.diag([1, 1, 1]))

#print(qs)

pl = np.array(LASER_AXIS_POS)
nl = np.array([0, np.tan(np.deg2rad(LASER_PLANE_TILT)), 1])

plnl = np.dot(pl, nl)
qsnl = np.dot(qs, nl)

#print(plnl)

ts = plnl / qsnl
tts = np.vstack((ts, ts, ts)).T

#print(tts)

p3d = np.multiply(qs, tts)

#print(p3d)

xs, ys, zs = p3d.T

figure = plt.figure()
ax3 = plt.axes(projection='3d')
ax3.scatter(xs, ys, zs)
ax3.set_title("3D Raw Point Cloud")
ax3.set_xlabel("x (mm)")
ax3.set_ylabel("y (mm)")
ax3.set_zlabel("z (mm)")
ax3.axis('equal')

#plt.show()

############


def reduce_cluster(p3d, eps):
    db = cluster.DBSCAN(eps=eps, min_samples=10).fit(p3d)
    labels = db.labels_
    core_samples_mask = np.zeros_like(labels, dtype=bool)
    core_samples_mask[db.core_sample_indices_] = True
    
    unique_labels = sorted(list(set(labels)))
    sizes = list(map(lambda x: (labels == x).sum(), unique_labels))
    max_index = np.argmax(sizes)
    max_label = unique_labels[max_index]
    max_points = p3d[core_samples_mask & (labels == max_label)]
    
    return max_points.T

#figure = plt.figure()
#ax3 = plt.axes(projection='3d')
#ax3.scatter(xs, ys, zs)
#ax3.set_title("3D Point Cloud (after outlier removal)")
#ax3.set_xlabel("x (mm)")
#ax3.set_ylabel("y (mm)")
#ax3.set_zlabel("z (mm)")
#ax3.axis('equal')

#plt.show()

############

def circle_fit(p3d):
    # circle fit is more restrictive, points should be densely packed together
    xs, ys, zs = reduce_cluster(p3d, 5)  
    
    figure = plt.figure()
    ax3 = plt.axes(projection='3d')
    ax3.scatter(xs, ys, zs)
    ax3.set_title("3D Point Cloud CIRCLE (after outlier removal)")
    ax3.set_xlabel("x (mm)")
    ax3.set_ylabel("y (mm)")
    ax3.set_zlabel("z (mm)")
    ax3.axis('equal')
    
    x = np.vstack((xs, zs, np.ones_like(zs))).T
    y = xs * xs + zs * zs
    
    a, res, rk, s = np.linalg.lstsq(x, y, rcond=None)
    
    score = 1000000 / res

    print("Circle matching score: ", score);
    
    if score > 1:
        print("Seems like a circle!")
    else:
        return False
    
    xs, ys, zs = reduce_cluster(p3d, 20)
    
    x = np.vstack((xs, zs)).T
    y = xs * xs + zs * zs
    
    ransac = linear_model.RANSACRegressor(loss='squared_loss', residual_threshold=10)
    ransac.fit(x, y)
#    print(ransac.__dict__)
    print(ransac.estimator_.__dict__)
    est = ransac.estimator_
    xc, zc = est.coef_[0]/2, est.coef_[1]/2
    rc = np.sqrt(est.intercept_ + xc*xc + zc*zc)
    inliers = ransac.inlier_mask_
    
    print("center x: ", xc, "\ncenter z: ", zc, "\nradius: ", rc)
    
    figure = plt.figure()
    ax = plt.axes()
    
    c1 = plt.Circle((xc, zc), radius=rc, color='g', fill=False, linestyle='--', linewidth=2)
    
    ax.scatter(xs[~inliers], zs[~inliers])
    ax.scatter(xs[inliers], zs[inliers])
    ax.add_artist(c1)
    ax.set_title("Circular Cross Section Matching")
    ax.set_xlabel("x (mm)")
    ax.set_ylabel("z (mm)")
    ax.legend(["2D projected points", "RANSAC fitted circle"])
    ax.axis('equal')
    return True

def line_fit(p3d):
    # line fit is relaxed, points could be more sparsely distributed
    xs, ys, zs = reduce_cluster(p3d, 20)  
    
    figure = plt.figure()
    ax3 = plt.axes(projection='3d')
    ax3.scatter(xs, ys, zs)
    ax3.set_title("3D Point Cloud LINE (after outlier removal)")
    ax3.set_xlabel("x (mm)")
    ax3.set_ylabel("y (mm)")
    ax3.set_zlabel("z (mm)")
    ax3.axis('equal')
    
    ransac_1 = linear_model.RANSACRegressor(loss='squared_loss', residual_threshold=0.5)
    ransac_1.fit(xs.reshape(-1, 1), zs)
    m_1, n_1 = ransac_1.estimator_.coef_, ransac_1.estimator_.intercept_
    line_1 = ransac_1.inlier_mask_
    
    ransac_2 = linear_model.RANSACRegressor(loss='squared_loss', residual_threshold=0.5)
    ransac_2.fit(xs.reshape(-1, 1)[~line_1], zs[~line_1])
    m_2, n_2 = ransac_2.estimator_.coef_, ransac_2.estimator_.intercept_
    line_2 = ransac_2.inlier_mask_
    
    xs_1, zs_1 = xs[line_1], zs[line_1]
    xs_2, zs_2 = xs[~line_1][line_2], zs[~line_1][line_2]
    
#    b_1 = np.ones_like(xs_1)
#    x_1 = np.vstack((xs_1, b_1)).T
#    a_1, res_1, rk_1, s_1 = np.linalg.lstsq(x_1, zs_1, rcond=None)
#    m_1, n_1 = a_1
#    
#    b_2 = np.ones_like(xs_2)
#    x_2 = np.vstack((xs_2, b_2)).T
#    a_2, res_2, rk_2, s_2 = np.linalg.lstsq(x_2, zs_2, rcond=None)
#    m_2, n_2 = a_2
        
    print(m_1, n_1, m_2, n_2)
    
    angle = 180 - np.rad2deg(np.arctan2(m_2-m_1, 1+m_2*m_1))
    if angle > 180:
        angle = 360 - angle
    print("Angle between edges: ", angle, " degrees")
    
    figure = plt.figure()
    #plt.scatter(xs, zs)
    plt.scatter(xs_1, zs_1, marker='x')
    plt.scatter(xs_2, zs_2, marker='x')
    plt.plot(xs_1, m_1*xs_1+n_1)
    plt.plot(xs_2, m_2*xs_2+n_2)
    plt.title("Linear Cross Section Matching")
    plt.xlabel("x (mm)")
    plt.ylabel("z (mm)")
    plt.legend(["Projected Edge Pts 1", "Projected Edge Pts 2", "Line Fit 1", "Line Fit 2"])
    plt.axis('equal')
    plt.show()

#############

#figure = plt.figure()
#plt.scatter(xs, zs)
#plt.show()

if not circle_fit(p3d):
    print("Not a circle. Lets check square or triangle base prism!")
    line_fit(p3d)

plt.show()
