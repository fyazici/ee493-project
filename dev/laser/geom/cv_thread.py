#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  2 20:16:25 2019

@author: fatih
"""

import cv2
import numpy as np
import argparse
import concurrent.futures
import threading

parser = argparse.ArgumentParser()
parser.add_argument("filename")
args = parser.parse_args()

cap = cv2.VideoCapture(args.filename)

def process_frame(frame, index, sem):
    frame = cv2.GaussianBlur(frame, (11, 11), 2)
    frame_diff = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    frame_diff = cv2.merge((frame_diff, frame_diff, frame_diff))
    
    ll = (80, 10, 50)
    ul = (120, 255, 255)
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    hsv[:, :, 0] = (hsv[:, :, 0] + 90) % 180
    nonred = cv2.inRange(hsv, ll, ul)
    
    frame2 = cv2.bitwise_and(frame, 255*np.ones_like(frame), None, nonred)
    frame2 = cv2.cvtColor(frame2, cv2.COLOR_BGR2GRAY)
    
    frame = np.uint8(frame * np.float32(frame_diff) / np.max(frame_diff))
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    frame = cv2.Canny(frame, 50, 150)
    frame = cv2.resize(frame, None, fx=0.3, fy=0.4)
    _, contours, hierarchy = cv2.findContours(frame, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
    s = 0.0
    for c in contours:
        if cv2.arcLength(c, True) > 3:
            s += cv2.contourArea(c)
    
    sem.release()
    
    return (np.sum(frame) + s, index)

if False:
    frame_index = 0
    while True:
        read, frame = cap.read()
        if not read:
            break
        
        print("submit job", frame_index)
        result = process_frame(frame, frame_index)
        print("got job result", result[1])
        frame_index += 1

else:
    frame_ids = set()    
    result_futures = []
    sem = threading.Semaphore(128)
    
    with concurrent.futures.ThreadPoolExecutor(8) as pool:
        frame_index = 0
        while True:
            read, frame = cap.read()
            if not read:
                break
            
            print("submit job", frame_index)
            
            t = id(frame)
            if t in frame_ids:
                print("!!! FRAME ID MATCH !!!")
                exit(-1)
            
            sem.acquire()
            result_futures.append(pool.submit(process_frame, frame, frame_index, sem))
            frame_index += 1
    
    for future in concurrent.futures.as_completed(result_futures):
        result = future.result()
        print("got job result", result[1])
