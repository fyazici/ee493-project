#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 18:26:46 2019

@author: fatih
"""

import argparse
import subprocess
import time
import logging
from laser_control import LaserController

parser = argparse.ArgumentParser()
parser.add_argument("-o", "--outfile")
parser.add_argument("-t", "--timeout", action="store", default=20000)
args = parser.parse_args()

logging.basicConfig(level=logging.INFO)

CAPTURE_COMMAND = "raspivid -v -n -t {} -o {}".format(args.timeout, args.outfile).split()

logging.info("initializing laser controller connection")

lc = LaserController()
lc.connect()

logging.info("connected to laser controller")

lc.laser_power(0)
lc.laser_position(0)
lc.laser_stop()

logging.info("starting raspivid capture")

p = subprocess.Popen(CAPTURE_COMMAND)

time.sleep(2)

logging.info("enabling laser")
lc.laser_power(255)
lc.laser_start()

logging.info("waiting for recording to finish")
p.wait()

logging.info("stopping laser")
lc.laser_power(0)
lc.laser_stop()
lc.disconnect()


