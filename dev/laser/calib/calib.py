#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Feb 23 23:44:18 2019

@author: fatih
"""

import numpy as np
import cv2
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("cam")
parser.add_argument("--width", required=False)
parser.add_argument("--height", required=False)
args = parser.parse_args()


criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

pattern_size = (4, 11)
objp = np.zeros((pattern_size[0] * pattern_size[1], 3), np.float32)
p_diag = 15
p_inner = p_diag / np.sqrt(2)

for i in range(pattern_size[1]):
    for j in range(pattern_size[0]):
        objp[i * pattern_size[0] + j] = np.array([(2*j + i%2) * p_inner, i * p_inner, 0])

objpoints = []
imgpoints = []
boards_added = 0

try:
    cam = int(args.cam)
except:
    cam = args.cam

cap = cv2.VideoCapture(cam)
if args.width and args.height:
    cap.set(cv2.CAP_PROP_FRAME_WIDTH, int(args.width))
    cap.set(cv2.CAP_PROP_FRAME_HEIGHT, int(args.height))

print(cap.get(cv2.CAP_PROP_FRAME_WIDTH))
print(cap.get(cv2.CAP_PROP_FRAME_HEIGHT))

gray = None

while True:
    read, img = cap.read()
    if not read:
        break
    
    img = cv2.flip(img, 1)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    ret, corners = cv2.findCirclesGrid(gray, pattern_size, flags=cv2.CALIB_CB_ASYMMETRIC_GRID)
    corners2 = corners
    
    if ret == True:
        #corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
        img = cv2.drawChessboardCorners(img, pattern_size, corners2, ret)
    
    cv2.imshow("img", img)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('q'):
        cv2.destroyAllWindows()
        exit()
    elif k == ord('n'):
        break
    elif k == ord('a') or ret == True:
        objpoints.append(objp)
        imgpoints.append(corners2)
        boards_added += 1
        print("Boards added:", boards_added)


idx = np.random.choice(len(objpoints), 40, replace=False)
objpoints = np.array(objpoints)[idx]
imgpoints = np.array(imgpoints)[idx]

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

print("Camera Matrix:", mtx, "\nDistortion Matrix:", dist, "\nRot Vecs:", rvecs, "\nTran Vecs:", tvecs, sep='\n')

cap.set(cv2.CAP_PROP_POS_FRAMES, 0)

while True:
    read, img = cap.read()
    if not read:
        break
    
    h, w = img.shape[:2]
    new_camera_mtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
   
    dst = cv2.undistort(img, mtx, dist, None, new_camera_mtx)
    
    x, y, w, h = roi
    #dst = dst[y:(y+h), x:(x+w)]
    cv2.imshow("orig", img)
    cv2.imshow("undis", dst)
    k = cv2.waitKey(0) & 0xFF
    if k == ord('q'):
        break
    elif k == ord('a'):
        cap.set(cv2.CAP_PROP_POS_FRAMES, cap.get(cv2.CAP_PROP_POS_FRAMES) - 2)
    elif k == ord('d'):
        cap.set(cv2.CAP_PROP_POS_FRAMES, cap.get(cv2.CAP_PROP_POS_FRAMES) + 0)

cv2.destroyAllWindows()