#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Nov  4 01:02:21 2018

@author: fatih
"""

import numpy as np
import cv2

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)

objp = np.zeros((7*9, 3), np.float32)
objp[:,:2] = np.mgrid[0:9, 0:7].T.reshape(-1, 2) * 20 # 20x20mm pattern

objpoints = []
imgpoints = []

fname = "cam1_frame0.jpg"

img = cv2.imread(fname)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

ret, corners = cv2.findChessboardCorners(gray, (9, 7), None)

if ret == True:
    objpoints.append(objp)
    
    corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
    imgpoints.append(corners)
    
    cv2.drawChessboardCorners(img, (9, 7), corners2, ret)
    cv2.imshow('img', img)
    cv2.waitKey(0)

cv2.destroyAllWindows()

ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::1], None, None)

print(dist)

h, w = 480, 640
newcameramtx, roi = cv2.getOptimalNewCameraMatrix(mtx, dist, (w, h), 1, (w, h))
print(mtx)

print(roi)

cap = cv2.VideoCapture(1)
print(cap.isOpened())

while True:
    ret, frame = cap.read()
    if not ret:
        continue
    frame_ud = frame[:]
    frame_ud = cv2.undistort(frame_ud, mtx, dist, None, newcameramtx)
    # x, y, w, h = roi
    #frame_ud = frame_ud[y:y+h, x:x+w]
    cv2.imshow('frame', frame)
    cv2.imshow('frame undistorted', frame_ud)
    if cv2.waitKey(30) == ord('q'):
        break
cap.release()
cv2.destroyAllWindows()
