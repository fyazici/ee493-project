#include <L3G.h>
#include <math.h>
#include <Wire.h>

L3G   gyro;
uint32_t timer;
uint32_t timer_next;
int d;
const int M1P = 9;
const int M1N = 6;
const int M2P = 11;
const int M2N = 10;
const int MAX_THRUST = 200;
const int MVMT_TIME = 100;
const float ERROR_THRESHOLD = 0.5;

float Kp=2;

//integral controller
float   Ki=1;
float   e_s = 0;
float   i = 0;


int ctr1 = 0, ctr2 = 0;
bool prev1, prev2;
int ctr1dir = 0, ctr2dir = 0;

void setup() {
  // put your setup code here, to run once:
  pinMode(M1P, OUTPUT);
  pinMode(M1N, OUTPUT);
  pinMode(M2P, OUTPUT);
  pinMode(M2N, OUTPUT);

  pinMode(2, INPUT_PULLUP);
  pinMode(3, INPUT_PULLUP);
  pinMode(12, OUTPUT);
  pinMode(13, OUTPUT);

  Serial.begin(1000000);
  Serial.setTimeout(1);
  Wire.begin();
  if (!gyro.init())
  {
    Serial.println("Failed to autodetect gyro type!");
    while (1);
  }

  gyro.enableDefault();
  timer = micros();
  timer_next=timer+100000;

  prev1 = digitalRead(2);
  prev2 = digitalRead(3);
}


float gzi=0.0;
float error=0.0;
float angle_target;

void loop() {
  //delay(20);
   // Calculate delta time
  if(Serial.available())
  {
  angle_target =Serial.parseInt();
  Serial.println(angle_target);
  }
  
  auto t = micros();
  //if(t>timer_next)
  {
    double dt = (double)(t - timer) / 1000000;
    timer = t;
    timer_next=timer+50000;

    if (abs(angle_target-gzi) > ERROR_THRESHOLD) 
    {
      gyro.read();
      int gyroZ = gyro.g.z - 25; 
      float gyroZrate = gyroZ / 116.44;
      gzi += gyroZrate* dt;
    }
    
    error=angle_target-gzi;
    e_s += error*dt;
    if(abs(e_s) > 500)
    {
      e_s = (e_s > 0) ? 500 : -500;
    }
    if(abs(error) < ERROR_THRESHOLD) 
    {
      e_s = 0;
    }
    i = e_s * Ki;
    d = Kp*error;
    d = d + i;
    d=constrain(abs(d),0,200)+50;
    
    Serial.print(gzi);
    Serial.print(" ,");
    Serial.print(error);
    Serial.print(" ,");
    Serial.print(i);
    Serial.print(" ,");
    Serial.print(d);
    Serial.print(" ,");
    Serial.print(ctr1);
    Serial.print(" ,");
    Serial.print(ctr2);
    Serial.println();
    
  }
  
  if (abs(error)<20) Kp=8; 
  else if (abs(error)<60) Kp=3;
  else Kp=2;


  auto b1 = digitalRead(2);
  auto b2 = digitalRead(3);
  
  if (!prev1 && b1)
  {
//    digitalWrite(12, HIGH);
    ctr1 += ctr1dir;
  }
  else
  {
//    digitalWrite(12, LOW);
  }

  if (!prev2 && b2)
  {
    digitalWrite(12, HIGH);
    ctr2 += ctr2dir;
  }
  else
  {
    digitalWrite(12, LOW);
  }

  if (ctr1 % 18 == 0)
  {
    digitalWrite(13, HIGH);
  } 
  else
  {
    digitalWrite(13, LOW);
  }

  prev1 = b1;
  prev2 = b2;
  
  
  ctr1dir = 1;
  ctr2dir = -1;
  analogWrite(M1P, 250);
  analogWrite(M1N, 0);
  analogWrite(M2P, 250);
  analogWrite(M2N, 0);
  return;
  
  if(error>0.0 && abs(error)>ERROR_THRESHOLD)
  {
    ctr1dir = 1;
    ctr2dir = -1;
    analogWrite(M1P, d);
    analogWrite(M1N, 0);
    analogWrite(M2P, 0);
    analogWrite(M2N, d);
  }
  else if (error<0.0 && abs(error)>ERROR_THRESHOLD)
  {
    ctr1dir = -1;
    ctr2dir = 1;
    analogWrite(M1P, 0);
    analogWrite(M1N, d);
    analogWrite(M2P,d);
    analogWrite(M2N, 0);
  }
  else 
  {    
    analogWrite(M1P, 0);
    analogWrite(M1N, 0);
    analogWrite(M2P, 0);
    analogWrite(M2N, 0);
   }
}
