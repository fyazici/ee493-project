# -*- coding: utf-8 -*-
"""
Mouse-like camera based navigational tracking
"""

import cv2
import numpy as np

cap = cv2.VideoCapture(0)

cv2.namedWindow("win_main")

exposureFractional = 0
exposureValue = 0

def onExposureModeChange(value, *args, **kwargs):
    global exposureFractional
    exposureFractional = value

def onExposureValueChange(value, *args, **kwargs):
    global exposureValue
    exposureValue = value

# TODO: add other settings

def onApplyCameraSettings(*args, **kwargs):
    global cap, posx, posy
    value = exposureValue - 50
    if exposureFractional != 0:
        value = value / 100
    cap.set(cv2.CAP_PROP_EXPOSURE, value)
    print(cap.get(cv2.CAP_PROP_EXPOSURE))
    posx, posy = 0, 0
    pass

cap.set(cv2.CAP_PROP_GAIN, 1.0)

cv2.createTrackbar("tb_exposure_fractional", "win_main",
                   0, 1, onExposureModeChange)

cv2.createTrackbar("tb_exposure", "win_main", 
                   0, 100, onExposureValueChange)

cv2.createTrackbar("tb_apply", "win_main",
                   0, 10, onApplyCameraSettings)

def listCaptureProperties():
    props = ((k, v) for (k, v) in cv2.__dict__.items() if "CAP_PROP_" in k)
    
    for (k, v) in props:
        value = cap.get(v)
        if value != -1.0:
            print("property ", k, " = ", value)

# get YUY2 image for faster grayscale
#cap.set(cv2.CAP_PROP_CONVERT_RGB, 0)

# auto exposure delay
for i in range(100):
    cap.read()

_, previousFrame = cap.read()
previousFrame = previousFrame[:,:,0]
previousMinLoc = (0, 0)
posx, posy = 0, 0
def processFrame(frame):
    global previousFrame, previousMinLoc, posx, posy
    roi_prev = previousFrame[160:320, 233:466]
    roi_curr = frame[::]
    match = cv2.matchTemplate(roi_curr, roi_prev, cv2.TM_SQDIFF)
    minVal, maxVal, minLoc, maxLoc = cv2.minMaxLoc(match)
    minx, miny = minLoc
    pmx, pmy = previousMinLoc
    
    dx = minx - 233
    dy = miny - 160
    posx += dx
    posy += dy
    print(posx, posy)
    
    if abs(dx*dx+dy*dy) < 10 and abs(posx*posx+posy*posy)>115200:
        posx, posy = 0, 0
    
    print(frame.shape, frame.dtype)
    img = np.zeros(shape=(480,640), dtype=np.uint8)
#    img = frame
    cv2.arrowedLine(img, (320, 240), (320+dx, 240+dy), (255,), 4)
    cv2.circle(img, (posx+320, posy+240), 3, (128,), 3)
    
    previousMinLoc = minLoc
    previousFrame = frame
    return img

try:
    while True:
        _, frame = cap.read()
        gray = frame[:,:,0]
        result = processFrame(gray)
        cv2.imshow("win_main", result)
        if ord('q') == cv2.waitKey(1):
            break
finally:
    cv2.destroyAllWindows()
    cap.release()   
