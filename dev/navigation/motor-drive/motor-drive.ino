#include <L3G.h>
#include <math.h>
#include <Wire.h>

L3G   gyro;
uint32_t timer;

constexpr int M1P = 6;
constexpr int M1N = 9;
constexpr int M2P = 11;
constexpr int M2N = 10;
constexpr int MAX_THRUST = 200;
constexpr int MVMT_TIME = 100;

void setup() {
  // put your setup code here, to run once:
  pinMode(M1P, OUTPUT);
  pinMode(M1N, OUTPUT);
  pinMode(M2P, OUTPUT);
  pinMode(M2N, OUTPUT);

  Serial.begin(1000000);
  Wire.begin();
      if (!gyro.init())
  {
    Serial.println("Failed to autodetect gyro type!");
    while (1);
  }

  gyro.enableDefault();
  timer = micros();
}


double gzi=0.0;

void loop() {
  Serial.print("Enter command:");
  if(Serial.available()) {
      auto c = Serial.read();
      Serial.println(c);
      switch (c) {
        case '1':
          analogWrite(M1P, MAX_THRUST);
          analogWrite(M1N, 0);
          delay(MVMT_TIME);
          break;
        case '2':
          analogWrite(M1P, 0);
          analogWrite(M1N, MAX_THRUST);
          delay(MVMT_TIME);
          break;
        case '3':
          analogWrite(M2P, MAX_THRUST);
          analogWrite(M2N, 0);
          delay(MVMT_TIME);
          break;
        case '4':
          analogWrite(M2P, 0);
          analogWrite(M2N, MAX_THRUST);
          delay(MVMT_TIME);
          break;
        case 'w':
          analogWrite(M1P, 0);
          analogWrite(M1N, MAX_THRUST*1.2);
          analogWrite(M2P, 0);
          analogWrite(M2N, MAX_THRUST);
          delay(MVMT_TIME);
          break;
        case 's':
          analogWrite(M1P, MAX_THRUST);
          analogWrite(M1N, 0);
          analogWrite(M2P, MAX_THRUST);
          analogWrite(M2N, 0);
          delay(MVMT_TIME);
          break;
        case 'd':
          analogWrite(M1P, 0);
          analogWrite(M1N, MAX_THRUST);
          analogWrite(M2P, MAX_THRUST);
          analogWrite(M2N, 0);
          delay(MVMT_TIME);
          break;
        case 'a':
          analogWrite(M1P, MAX_THRUST);
          analogWrite(M1N, 0);
          analogWrite(M2P, 0);
          analogWrite(M2N, MAX_THRUST);
          delay(MVMT_TIME);
          break;
        default:
          delay(100);
          break;
      }
      analogWrite(M1P, 0);
      analogWrite(M1N, 0);
      analogWrite(M2P, 0);
      analogWrite(M2N, 0);
  }

  gyro.read();
  double dt = (double)(micros() - timer) / 1000000; // Calculate delta time
    int gyroZ = -gyro.g.z ; 
  double gyroZrate = gyroZ / 116.44;
  gzi += gyroZrate* dt;
   timer = micros();
   Serial.println(gzi);
}
