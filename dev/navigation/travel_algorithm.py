#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Apr  6 23:27:18 2019

@author: metecan
"""

import numpy as np
import argparse
#from scipy import spatial
from navigation_control import NavigationController
from operator import itemgetter
import logging
import subprocess
import time

length_of_line = np.hypot
#def length_of_line(x,y):
#    distance = math.sqrt(x**2+y**2)
#    return distance

def cart2pol(xy):
    x, y = xy
    dist = np.hypot(x, y)
    angle = np.rad2deg(np.arctan2(y, x))
    return (dist, angle)

def pol2cart(rho, phi):
    x = rho * np.cos(phi)
    y = rho * np.sin(phi)
    return (x, y)
    
parser = argparse.ArgumentParser()
parser.add_argument("-s", "--datafile")
parser.add_argument("-v", "--verbose", action="store_true")
args = parser.parse_args()

if args.verbose:
    logging.basicConfig(level=logging.DEBUG)

NC = NavigationController()
NC.connect()
logging.debug("Mega connected")

data = np.load(args.datafile)
clusters = data["clusters"]

closest_points = []
robot_status = {
        "position": np.float32([0,0]),
        "angle": 0,
        "visited_object": 0,
        "visited_walls": 0,
        "prev_position": [0,0],
        "prev_angle": 0
        }

for xy in clusters:
    if len(xy) > 0 and len(xy) < 20:
        closest_point = { "coordinates": [], "distance": 20000 }
        
        for p in xy:
            distance = np.linalg.norm(p - robot_status["position"])
            if closest_point["distance"] > distance:
                closest_point["distance"] = distance
                closest_point["coordinates"] = p
        
        closest_points.append(closest_point)
        
closest_points = sorted(closest_points, key=itemgetter('distance')) 

for closest_point in closest_points:
    dist, angle = cart2pol(closest_point['coordinates'] - robot_status["position"])
    
    robot_status['prev_angle'] = robot_status['angle']
    logging.debug("rotation to {}".format(angle))
    NC.rotation(angle - robot_status['angle'])
    
    input("block")
    
    logging.debug("movement to {}".format(dist))
    NC.movement(dist - 120 - 100)
    
    input("block")
    
    px, py, ptheta = NC.odometry()
    robot_status["position"] = [ px, py ]
    robot_status["angle"] = ptheta
    logging.debug("At %f %f %f", px, py, ptheta)
    
    # second reach iteration
    _, angle = cart2pol(closest_point['coordinates'] - robot_status["position"])
    NC.rotation(angle - ptheta)
    
    input("block")
    
    ###scan task
    #subprocess.call("python3 laser_geom.py -v -c 0 -l -o fmap/scanpts.npz".split())
    #subprocess.call("python3 laser_proj.py -v fmap/scanpts.npz".split())
    logging.debug("scan task")
    time.sleep(3)
    ###end of scan
    
    logging.debug("Movement to original position {}".format(robot_status['prev_position']))
    #NC.movement(-dist + 120 + 150)
    
    px, py, ptheta = NC.odometry()
    robot_status["position"] = [ px, py ]
    robot_status["angle"] = ptheta
    logging.debug("At %f %f %f", px, py, ptheta)
    
    dist2, angle2 = cart2pol(np.float32([0, 0]) - robot_status["position"])
    
    NC.rotation(angle2 - ptheta)
    
    input("block")
    
    NC.movement(dist2)

    input("block")
    
    px, py, ptheta = NC.odometry()
    robot_status["position"] = [ px, py ]
    robot_status["angle"] = ptheta
    logging.debug("At %f %f %f", px, py, ptheta)
    
    
#    robot_status['prev_position'] = robot_status['position']
#    robot_status['position'] = closest_point['coordinates']
